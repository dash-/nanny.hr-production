@extends('master')

@section('content')
    <div class="content-lower">
        <div>
            <a href="/user/edit-family-info">
                <button class="submit-button-contact">Uredi obitelj</button>
            </a>
            <a href="/user/edit-parents">
                <button class="submit-button-contact">Uredi roditelje</button>
            </a>
            <a href="/user/edit-children-info">
                <button class="submit-button-contact">Uredi djecu</button>
            </a>
        </div>
    </div>
@endsection