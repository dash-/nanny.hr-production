@extends('master')

@section('content')
    <div class="content-lower">
        <h3>{{trans('lang.need_a_nanny')}}</h3>
        <div class="form-views">


            <div id="input_view" style="display: block;">
                {!! Form::model($model) !!}

                {!! Form::label('from', trans('lang.from'), ['class' => "raleway-regular"]) !!}
                {!! Form::text('from', $from_date, ['id' => 'from', 'class'=> 'form-control']) !!}

                {!! Form::label('to', trans('lang.to'), ['class' => "raleway-regular"]) !!}
                {!! Form::text('to', $to_date, ['id' => 'to', 'class'=> 'form-control']) !!}

                <button class="button-regular" style="float:left;margin-top:1%;" type="submit">{{trans('lang.next')}}</button>
                {!! Form::close() !!}
            </div>

            {{--<div id="calendar_view" style="display: none;">--}}



            {{--<div id='calendar' style="width:100%;"></div>--}}
            {{--<button class="button-proceed-reservation">Dalje</button>--}}
            {{--<div class="ovdje"></div>--}}
        </div>
    </div>

@endsection

