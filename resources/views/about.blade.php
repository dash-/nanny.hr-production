@extends('master')

@section('content')
    <div class="content-lower only-text">
        <h3>{{trans('lang.about')}}</h3>
        <p><b>{{trans('lang.about_text_bold')}}</b></p>
        {!! trans('lang.about_text') !!}


        <br>
        <h3>{{trans('lang.our_story')}}</h3>
       {!! trans('lang.our_story_text')!!}
        <br>
        <h3>{{trans('lang.our_services')}}</h3>
        <p><b>{{trans('lang.our_services_bold')}}:</b></p>
        <ul>
            <li>{{trans('lang.our_services_text1')}}</li>
            <li>{{trans('lang.our_services_text2')}}</li>
            <li>{{trans('lang.our_services_text3')}}</li>
            <li>{{trans('lang.our_services_text4')}}</li>
            <li>{{trans('lang.our_services_text5')}}</li>
            <li>{{trans('lang.our_services_text6')}}</li>
        </ul>
        <br>
        <h3>{{trans('lang.our_team')}}</h3>
       {!! trans('lang.our_team_text') !!}
    </div>

@endsection