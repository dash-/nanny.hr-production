@extends('master')

@section('content')
    <div class="content-lower">


    <div class="form-views">
            @if(!$times)
                <p><b>{{trans('lang.time_chosen_error_message')}}.</b></p>
                <a href="/availability/availability"><button class="button-regular">{{ trans('lang.back')}}</button></a>
                @else
            <b>{{trans('lang.nanny_for')}}</b>
            {!! Form::open() !!}

            @foreach($times as $timekey => $time)
                <table class="table borderless">
                    <tbody>

                    <tr>
                        <td>{{trans('lang.for_period')}}: {{\App\Helpers\GeneralHelper::displayLongDate($timekey)}}</td>
                    </tr>

                    @if($children->count())
                        @foreach($children as $key => $child)
                            <tr>
                                <td>{!! Form::checkbox($timekey.'[children][child'.$key.']', $child->name.' '.$child->surname) !!}
                                    {!! Form::label($timekey.'[children][child'.$key.']', $child->name.' '.$child->surname) !!}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>{!! Form::label($timekey.'[other_children]', trans('lang.additional_children').'?')!!}
                                {!! Form::text($timekey.'[other_children]','',['class'=> 'input-field-regular fixed200']) !!}</td>
                        </tr>
                    @else
                        <tr>
                            <td>{!! Form::label($timekey.'[children_number]', trans('lang.num_of_children'))!!}
                                {!! Form::text($timekey.'[children_number]','',['class'=> 'input-field-regular fixed200']) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>{!! Form::label($timekey.' [address_other]', trans('lang.reservation_address'))!!}
                            {!! Form::text($timekey.'[address_other]', $address,['class'=> 'input-field-regular fixed200']) !!}</td>
                    </tr>
                    <tr>
                        <td>{!! Form::label($timekey.'[special_note]', trans('lang.special_reservation_notes').':')!!}
                            {!! Form::text($timekey.'[special_note]','',['class'=> 'input-field-regular fixed200']) !!}</td>
                    </tr>
                    </tbody>
                </table>

            @endforeach
            <button class="button-regular" type="submit">{{trans('lang.next')}}</button>

            {!! Form::close()!!}
            @endif

        </div>
    </div>
@endsection