@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">
            {!! Form::open() !!}
                {!! Form::label('password', 'Nova lozinka') !!}
                {!! Form::password('password', ['class'=> 'input-field-regular']) !!}
            <button type="submit" class="button-regular" style="float:none">Promjeni lozinku</button>
            <span style="visibility: hidden">
                {!! Form::text('id', $id) !!}
            </span>
            {!! Form::close() !!}
        </div>
    </div>
@endsection