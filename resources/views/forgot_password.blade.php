@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.reset_password')}}</h3>

                <div class="form-group">

                {!! Form::open() !!}
                    <label for="email"><span>*</span> Email</label>
                    {!! Form::text('email', '', ['class'=> 'form-control']) !!}
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.submit')}}</button>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>

                <div class="stripe-line"></div>
            </div>
        </div>
    </div>
@endsection