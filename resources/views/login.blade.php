@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.sign_in')}}</h3>
                {!! Form::model($user) !!}
                <div class="form-group">
                    <label for="email"><span>*</span> {{trans('lang.email')}}</label>
                    {!! Form::text('email', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="password"><span>*</span> {{trans('lang.password')}}</label>
                        {!! Form::password('password', ['class' => 'form-control'])!!}
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.login')}}</button>
                </div>
                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="lower-links">
                    <div class="pull-left">
                        <a href="/forgot-password" class="gray-link">
                            {{trans('lang.forgot_password')}}?
                        </a>
                    </div>
                    <div class="pull-right">
                        <a href="/register">
                            {{trans('lang.register')}}!
                        </a>
                    </div>
                </div>
                <div class="stripe-line"></div>
            </div>
        </div>
    </div>
@endsection