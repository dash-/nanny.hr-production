@extends('master')

@section('content')


    <div class="content-lower">
        <div class="how-to-box" style="background-color: #1FC3F3">
            <div class="fn-icons"><img src="/images/noun_1f.png" alt=""/></div>
            <p class="hero-boxes">Prijavi se/registriraj se</p>
            <p class="raleway-regular" style="font-size: 12px;">Popunite osnovne podatke o vašoj obitelji i potrebama. Sigurnost i privatnost vasih osobnih podataka je zajamčena.</p>
        </div>
        <div class="how-to-box" >
            <div class="fn-icons"><img src="/images/noun_2f.png" alt=""/></div>
            <p class="hero-boxes">Odaberi termin čuvanja</p>
            <p class="raleway-regular" style="font-size: 12px">Jednostavno odaberite termin u kalendaru ili nas kontaktirajte ako trebate rezervirati višestruke termine.</p>
        </div>
        <div class="how-to-box" style="background-color: #57BC83">
            <div class="fn-icons"><img src="/images/noun_3f.png" alt=""/></div>
            <p class="hero-boxes">Odaberi čuvalicu</p>
            <p class="raleway-regular" style="font-size: 12px">Na temelju profila čuvalica odaberite željenu čuvalicu, a mi ćemo za vas provjeriti njezinu dostupnost.</p>
        </div>
        <div class="how-to-box" style="background-color: #F07DB0">
            <div class="fn-icons"><img src="/images/noun_4f.png" alt=""/></div>
            <p class="hero-boxes">Plaćanje</p>
            <p class="raleway-regular" style="font-size: 12px">Plaćanje se obavlja isključivo kreditnim i debitnim karticama. Naplata se vrši nakon obavljenog čuvanja.</p>
        </div>
    </div>

    @endsection