@extends('master')

@section('content')

    <div class="content-lower">

        <div class="raleway-regular" style="font-size: 30px; margin:10% 0 0 5%">
            @if(Auth::check() && Auth::user()->role == 1)
                <span>Administracija</span>
            @else
                <span>{{trans('lang.family')}} {{Auth::user()->full_name()}}</span>
            @endif
        </div>
        @if(Auth::check() && Auth::user()->role == 1)
            <div>
                <a href="/admin/korisnici">
                    <button class="submit-button-contact">Korisnici</button>
                </a>
                <a href="/admin/rezervacije">
                    <button class="submit-button-contact">Rezervacije</button>
                </a>
            </div>
            <div>
                <a href="/nannies">
                    <button class="submit-button-contact">Uredi čuvalice</button>
                </a>
                <a href="/nannies/add-nanny">
                    <button class="submit-button-contact">Dodaj čuvalicu</button>
                </a>
            </div>
            <div>
                <a href="/admin/pozadine">
                    <button class="submit-button-contact">Izaberi pozadine</button>
                </a>
                <a href="/admin/dodaj-pozadine">
                    <button class="submit-button-contact">Dodaj pozadine</button>
                </a>
            </div>
            <div>
                <a href="/admin/popust-tokeni">
                    <button class="submit-button-contact">Napravi token za popust</button>
                </a>
                <a href="/admin/nadolazeci-praznici">
                    <button class="submit-button-contact">Uredi nadolazeće praznike</button>
                </a>
            </div>
            <div>
                <a href="/admin/opci-uvjeti">
                    <button class="submit-button-contact">Uredi Opće uvjete</button>
                </a>
                <a href="/admin/change-baby-monitor-availability">
                    <button class="submit-button-contact" style="background-color: {{\App\Helpers\GeneralHelper::monitorAvailability(Auth::user()->baby_monitor_availability)}}">Promjeni dostupnost Baby-monitora (Trenutno: {{(Auth::user()->baby_monitor_availability) ? 'Dostupno' : 'Nedostupno'}})</button>
                </a>
            </div>
            <div>

                <a href="/admin/cjenik">
                    <button class="submit-button-contact">Uredi Cijene</button>
                </a>
            </div>
            <div>
                <a href="/admin/cjenik-tekst">
                    <button class="submit-button-contact">Uredi Cjenik</button>
                </a>
                <a href="/admin/uredi-pakete">
                    <button class="submit-button-contact">Uredi Pakete</button>
                </a>
            </div>
            <div>
                <a href="/admin/novosti-lista">
                    <button class="submit-button-contact">Novosti</button>
                </a>
                <a href="/admin/prevozi">
                    <button class="submit-button-contact">Prijevozi</button>
                </a>
            </div>
        @else
            <div>
                <a href="/user/edit-family-info">
                    <button class="submit-button-contact">{{trans('lang.edit_family')}}</button>
                </a>
                <a href="/user/edit-parents">
                    <button class="submit-button-contact">{{trans('lang.edit_parents')}}</button>
                </a>
                <a href="/user/edit-children-info">
                    <button class="submit-button-contact">{{trans('lang.edit_children')}}</button>
                </a>
            </div>
            <div>
                <a href="/user/rezervacije">
                    <button class="submit-button-contact">{{trans('lang.reservations')}}</button>
                </a>
                <a href="/user/prevozi">
                    <button class="submit-button-contact">{{trans('lang.transport')}}</button>
                </a>
            </div>
            @if(isset($package) && $package && $package->package)
                <div>
                    <a href="/user/aktiviraj-paket/{{$package->id}}">
                        <button class="submit-button-contact">{{trans('lang.activate_package')}}
                            ({{$package->package->package_name}})
                        </button>
                    </a>
                </div>
            @endif
            @if(isset($active_package) && $active_package && $active_package->package)
                <div style="font-weight:bold; margin-top:5%">
                    <p>{{trans('lang.currently_active_package')}}: {{$active_package->package->package_name}}</p>
                    <p>{{trans('lang.uses_left')}}: {{$active_package->no_of_uses}}</p>
                </div>
            @endif

        @endif
    </div>
@endsection