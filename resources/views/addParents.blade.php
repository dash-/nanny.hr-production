@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">
            {!! Form::open() !!}
            <table class="table borderless">
                <tbody>

                <tr>
                    <td>{!! Form::label('parent_type', trans('lang.type'))!!}
                        {!! Form::select('parent_type', array(1 => 'Roditelj', 2 => 'Kontakt za hitne slučajeve'), ['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('name', trans('lang.name'))!!}
                        {!! Form::text('name', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('surname', trans('lang.surname'))!!}
                        {!! Form::text('surname', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('dob', trans('lang.dob'))!!}
                        {!! Form::text('dob', '', ['class'=> 'input-field-regular', 'id' => 'datepicker'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('jmbg', trans('lang.oib'))!!}
                        {!! Form::text('jmbg', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('email', trans('lang.email'))!!}
                        {!! Form::text('email', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('sex', trans('lang.gender')) !!}
                        <span> {{ trans('lang.male') }}</span>
                        {!! Form::radio('sex', 'Muško', true) !!}
                        <span> {{trans('lang.female')}}</span>
                        {!! Form::radio('sex', 'Žensko', false) !!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('phone', trans('lang.phone'))!!}
                        {!! Form::text('phone', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('relation', trans('lang.relatioon'))!!}
                        {!! Form::text('relation', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="button-regular">{{trans('lang.save_changes')}}</button>

            {!! Form::close() !!}
        </div>
    </div>
@endsection