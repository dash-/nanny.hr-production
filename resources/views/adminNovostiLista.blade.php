@extends('master')

@section('content')

    <div class="content-lower">

        <a class="btn btn-primary" href="/admin/novost">
            Dodaj vijest
        </a>
        <table class="table">
            <thead>
            <tr>
                <th>Naziv</th>
                <th>URL</th>
                <th>Kratak opis</th>
                <th>Opcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach($novosti as $novost)
                <tr>
                    <td>
                        {{$novost->title_hr}}
                        <br>
                        {{$novost->title_en}}
                    </td>
                    <td>
                        {{$novost->slug_hr}}
                        <br>
                        {{$novost->slug_en}}
                    </td>
                    <td>
                        {{$novost->summary_hr}}
                        <br>
                        {{$novost->summary_en}}
                    </td>
                    <td>
                        <a href="/admin/novost/{{$novost->id}}">
                            <button>Izmijeni</button>
                        </a>
                        <br>
                        <br>
                        <a href="/admin/novost-obrisi/{{$novost->id}}">
                            <button>Obriši</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $novosti->render() !!}
    </div>
@endsection