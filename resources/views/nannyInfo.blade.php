@extends('master')

@section('content')
    <div class="content-lower">
        @if($locale == 'hr')
        <div style="float:left; margin-left:5%; margin-right:10%">
            <table class="table borderless" style="text-align: center">
                <tr>
                    <td><img src="{{ '/images/nannies/'. $nanny->image }} " alt=""/></td>
                </tr>
                <tr>
                    <td> {{ $nanny->name }} </td>
                </tr>
                <tr>
                    <td> {{ $nanny->surname}}</td>
                </tr>
                <tr>
                    <td>{{ $nanny->dob }}</td>
                </tr>
                <tr>
                    <td>{{ $nanny->city }}</td>
                </tr>
                <tr>
                    <td>{{ $nanny->description }} </td>
                </tr>
                <tr>
                    <td><a href="/availability/available-nannies">
                            <button>{{trans('lang.back')}}</button>
                        </a>
                    </td>
                </tr>
            </table>
        </div>

        <div style="float:left">
            <table class="table">

                <tr>
                    <td>{{trans('lang.experience_babysitting')}}:</td>
                    <td>{{$nanny->years_of_nanny_exp}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.experience_earning_money')}}:</td>
                    <td>{{$nanny->year_of_payed_nanny_exp}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.most_experienced_with_children_of_age')}}:</td>
                    <td>{{$nanny->exp_with_age}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.urgent_demand')}}:</td>
                    <td>{{$nanny->can_accept_urgent}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.maximum_travel_distance')}}:</td>
                    <td>{{$nanny->maximum_distance}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.work_places')}}:</td>
                    <td>{{$nanny->places}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.personal_car')}}:</td>
                    <td>{{$nanny->own_car}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.drivers_license')}}:</td>
                    <td>{{$nanny->own_drivers_licence}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.education_level')}}:</td>
                    <td>{{$nanny->education_level}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.occupation')}}:</td>
                    <td>{{$nanny->education_title}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.foreign_languages')}}:</td>
                    <td>{{$nanny->languages}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.additional_skills')}}:</td>
                    <td>{{$nanny->additional_skills}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.additional_qualifications')}}:</td>
                    <td>{{$nanny->additional_qualifications}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.comfortable_with_pets')}}:</td>
                    <td>{{$nanny->managing_with_pets}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.ill_child_care')}}:</td>
                    <td>{{$nanny->care_sick}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.special_need_care')}}:</td>
                    <td>{{$nanny->care_disabled}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.is_parent')}}:</td>
                    <td>{{$nanny->is_parent}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.parent_children_number')}}:</td>
                    <td>{{$nanny->number_of_children}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.smoker')}}:</td>
                    <td>{{$nanny->smoker}}</td>
                </tr>
                <tr>
                    <td>{{trans('lang.agree_to_supervision')}}?</td>
                    <td>{{$nanny->agree_to_surveillance}}</td>
                </tr>

            </table>
        </div>
        @else
            <div style="float:left; margin-left:5%; margin-right:10%">
                <table class="table borderless" style="text-align: center">
                    <tr>
                        <td><img src="{{ '/images/nannies/'. $nanny->image }} " alt=""/></td>
                    </tr>
                    <tr>
                        <td> {{ $nanny->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ $nanny->surname}}</td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->dob }}</td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->city }}</td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->description_eng }} </td>
                    </tr>
                    <tr>
                        <td><a href="/availability/available-nannies">
                                <button>{{trans('lang.back')}}</button>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="float:left">
                <table class="table">

                    <tr>
                        <td>{{trans('lang.experience_babysitting')}}:</td>
                        <td>{{$nanny->years_of_nanny_exp_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.experience_earning_money')}}:</td>
                        <td>{{$nanny->year_of_payed_nanny_exp_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.most_experienced_with_children_of_age')}}:</td>
                        <td>{{$nanny->exp_with_age_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.urgent_demand')}}:</td>
                        <td>{{$nanny->can_accept_urgent_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.maximum_travel_distance')}}:</td>
                        <td>{{$nanny->maximum_distance_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.work_places')}}:</td>
                        <td>{{$nanny->places_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.personal_car')}}:</td>
                        <td>{{$nanny->own_car_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.drivers_license')}}:</td>
                        <td>{{$nanny->own_drivers_licence_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.education_level')}}:</td>
                        <td>{{$nanny->education_level_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.occupation')}}:</td>
                        <td>{{$nanny->education_title_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.foreign_languages')}}:</td>
                        <td>{{$nanny->languages_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.additional_skills')}}:</td>
                        <td>{{$nanny->additional_skills_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.additional_qualifications')}}:</td>
                        <td>{{$nanny->additional_qualifications_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.comfortable_with_pets')}}:</td>
                        <td>{{$nanny->managing_with_pets_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.ill_child_care')}}:</td>
                        <td>{{$nanny->care_sick_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.special_need_care')}}:</td>
                        <td>{{$nanny->care_disabled_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.is_parent')}}:</td>
                        <td>{{$nanny->is_parent_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.parent_children_number')}}:</td>
                        <td>{{$nanny->number_of_children_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.smoker')}}:</td>
                        <td>{{$nanny->smoker_eng}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.agree_to_supervision')}}?</td>
                        <td>{{$nanny->agree_to_surveillance_eng}}</td>
                    </tr>

                </table>
            </div>
        @endif
    </div>
    @endsection