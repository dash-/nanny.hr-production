@extends('master')

@section('content')
    <div class="content-lower">

        <div class="">
            {!! Form::model($content)!!}

            {!! Form::label('title', 'Naslov')!!}
            {!! Form::text('title', null, ['class'=> 'input-field-regular'])!!}

            {!! Form::label('text', 'Tekst')!!}
            {!! Form::textarea('text', null, ['class'=> 'tinymce'])!!}

            <button>Spremi</button>

            {!! Form::close()!!}
        </div>

        <a href="/admin/cjenik-tekst"><button style="margin-top:3%">Hrvatska verzija</button></a>
    </div>
@endsection