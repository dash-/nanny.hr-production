@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            {!! Form::open() !!}

            {{--<p>Hrvatska verzija</p>--}}
            <table class="table borderless col-sm-6" style="width:45%;float:left;">
                <thead>
                <tr>
                    <th>Hrvatska verzija</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        {{--                        {!! Form::label('name', 'Ime')!!}--}}
                        {!! Form::text('name', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Ime']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--                        {!! Form::label('surname', 'Prezime')!!}--}}
                        {!! Form::text('surname',null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Prezime']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('dob', 'Datum rođenja')!!}--}}
                        {!! Form::text('dob',null, ['class'=> 'input-field-regular-add-nanny', 'id' => 'datepicker', 'placeholder' => 'Datum rođenja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('email', 'Email')!!}--}}
                        {!! Form::text('email',null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Email']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('phone', 'Telefon')!!}--}}
                        {!! Form::text('phone',null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Telefon']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('address', 'Adresa')!!}--}}
                        {!! Form::text('address',null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Adresa']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--                        {!! Form::label('name', 'Ime')!!}--}}
                        {!! Form::text('city', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Grad']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('oib', 'OIB')!!}--}}
                        {!! Form::text('oib',null, ['class'=> 'input-field-regular-add-nanny','placeholder' => 'OIB']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('agree_to_surveillance', 'Pristajem na nadzor uz Baby-monitor?')!!}--}}
                        {!! Form::text('agree_to_surveillance',null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Pristajem na nadzor uz Baby-monitor?']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('description', 'Kratki opis')!!}--}}
                        {!! Form::textarea('description', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Kratki opis']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('years_of_nanny_exp', 'Godine iskustva čuvanja djece')!!}--}}
                        {!! Form::text('years_of_nanny_exp', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Godine iskustva čuvanja djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('year_of_payed_nanny_exp', 'Godine iskustva na plaćenim poslovima čuvanja djece')!!}--}}
                        {!! Form::text('year_of_payed_nanny_exp', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Godine iskustva na plaćenim poslovima čuvanja djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('exp_with_age', 'Najviše iskustva imam s uzrastom')!!}--}}
                        {!! Form::text('exp_with_age', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Najvise iskustva imam sa uzrastom']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('can_accept_urgent', 'Mogu prihvatiti posao hitno')!!}--}}
                        {!! Form::text('can_accept_urgent', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Mogu prihvatiti posao hitno']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('maximum_distance', 'Maksimalna udaljenost od doma do mjesta čuvanja')!!}--}}
                        {!! Form::text('maximum_distance', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Maksimalna udaljenost od doma do mjesta čuvanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('places', 'Mjesta u kojima mogu obavljati čuvanja')!!}--}}
                        {!! Form::text('places', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Mjesta u kojima mogu obavljati čuvanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_car', 'Vlastiti automobil')!!}--}}
                        {!! Form::text('own_car', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Vlastiti automobil']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_drivers_licence', 'Vozačka dozvola')!!}--}}
                        {!! Form::text('own_drivers_licence', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Vozačka dozvola']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_level', 'Stupanj obrazovanja')!!}--}}
                        {!! Form::text('education_level', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Stupanj obrazovanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_title', 'Zvanje')!!}--}}
                        {!! Form::text('education_title', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Zvanje']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('languages', 'Znanje stranih jezika')!!}--}}
                        {!! Form::text('languages', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Znanje stranih jezika']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_skills', 'Dodatne vještine')!!}--}}
                        {!! Form::textarea('additional_skills', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Dodatne vjestine']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_qualifications', 'Dodatne kvalifikacije')!!}--}}
                        {!! Form::textarea('additional_qualifications', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Dodatne kvalifikacije']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('managing_with_pets', 'Snalaženje s kućnim ljubimcima')!!}--}}
                        {!! Form::text('managing_with_pets', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Snalaženje s kućnim ljubimcima']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_sick', 'Briga za bolesnu djecu')!!}--}}
                        {!! Form::text('care_sick', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Briga za bolesnu djecu']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_disabled', 'Briga za djecu s posebnim potrebama')!!}--}}
                        {!! Form::text('care_disabled', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Briga za djecu sa posebnim potrebama']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('is_parent', 'Roditelj sam')!!}--}}
                        {!! Form::text('is_parent', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Roditelj sam']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('number_of_children', 'Broj djece')!!}--}}
                        {!! Form::text('number_of_children', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Broj djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('smoker', 'Jesam li pusač')!!}--}}
                        {!! Form::text('smoker', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Jesam li pušač']) !!}</td>
                </tr>

                </tbody>
            </table>


            {{--<p>Engleska verzija</p>--}}
            <table class="table borderless col-sm-6" style="width:45%; float:right">
                <thead>
                <tr>
                    <th>Engleska verzija</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        {{--{!! Form::label('agree_to_surveillance', 'I agree to surveilance?')!!}--}}
                        {!! Form::text('agree_to_surveillance_eng',null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I agree to surveilance?']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('description', 'Short description')!!}--}}
                        {!! Form::textarea('description_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Short description']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('years_of_nanny_exp', 'Years of experience in babysitting')!!}--}}
                        {!! Form::text('years_of_nanny_exp_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Years of experience in babysitting']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('year_of_payed_nanny_exp', 'Years of experience in earning money as a babysitter')!!}--}}
                        {!! Form::text('year_of_payed_nanny_exp_eng', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'Years of experience in earning money as a babysitter']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('exp_with_age', 'I am most experienced in working with children between the ages of')!!}--}}
                        {!! Form::text('exp_with_age_eng', null,['class'=> 'input-field-regular-add-nanny','placeholder' => 'I am most experienced in working with children between the ages of']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('can_accept_urgent', 'I can respond to urgent demand for a babysitter')!!}--}}
                        {!! Form::text('can_accept_urgent_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I can respond to urgent demand for a babysitter']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('maximum_distance', 'Maximum travel distance to the place of babysitting I accept is')!!}--}}
                        {!! Form::text('maximum_distance_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Maximum travel distance to the place of babysitting I accept is']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('places', 'Places and venues I am comfortable babysitting at ')!!}--}}
                        {!! Form::text('places_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Places and venues I am comfortable babysitting at']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_car', 'Personal car')!!}--}}
                        {!! Form::text('own_car_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Personal car']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_drivers_licence', 'Drivers license')!!}--}}
                        {!! Form::text('own_drivers_licence_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Drivers license']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_level', 'Education level')!!}--}}
                        {!! Form::text('education_level_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Education level']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_title', 'Occupation')!!}--}}
                        {!! Form::text('education_title_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Occupation']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('languages', 'Foreign languages')!!}--}}
                        {!! Form::text('languages_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Foreign languages']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {!! Form::textarea('additional_skills_eng', null ,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Additional skills']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {!! Form::textarea('additional_qualifications_eng', null, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Additional qualifications']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('managing_with_pets', 'Are you comfortable around pets')!!}--}}
                        {!! Form::text('managing_with_pets_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you comfortable around pets']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_sick', 'Are you willing to care for an ill child')!!}--}}
                        {!! Form::text('care_sick_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you willing to care for an ill child']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_disabled', 'Are you willing to care for a child with special needs')!!}--}}
                        {!! Form::text('care_disabled_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you willing to care for a child with special needs']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('is_parent', 'I am a parent')!!}--}}
                        {!! Form::text('is_parent_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I am a parent']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('number_of_children', 'Number of children')!!}--}}
                        {!! Form::text('number_of_children_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Number of children']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('smoker', 'Smoker')!!}--}}
                        {!! Form::text('smoker_eng', null,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Smoker']) !!}</td>
                </tr>

                </tbody>
            </table>
            <button class="button-regular" style="margin-right: 5%" type="submit">Spremi</button>

            {!! Form::close() !!}

            <a href="/user">
                <button class="button-regular">Nazad</button>
            </a>
        </div>
    </div>
@endsection