@extends('master')

@section('content')



    <div class="content-lower only-text">
        <h3>{{trans('lang.i_want_to_be_nanny')}}</h3>
        <p>
            {{trans('lang.i_want_to_be_nanny_text')}}
        </p>


        <div class="inline-form">


            {!! Form::model($model) !!}
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.name')}}</label>
                    {!! Form::text('name', null, [ 'class' => "form-control" ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.surname')}}</label>
                    {!! Form::text('surname', null, [ 'class' => "form-control" ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.email')}}</label>
                    {!! Form::text('email', null, [ 'class' => "form-control" ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.phone')}}</label>
                    {!! Form::text('phone', null, [ 'class' => "form-control" ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.dob')}}</label>

                    {!! Form::text('dob', null, [ 'class' => "form-control", 'id' => 'dob']) !!}
                </div>
            </div>
            <div class="form-group radio-fields-holder">

                <div class="col-md-12">
                    <label for=""><span>*</span>{{trans('lang.children_care_experience')}}?</label>
                    <div class="radio-fields-multiple">
                        <div class="izbor">{{trans('lang.yes')}}
                            {!! Form::radio('experience', 'Da', false) !!}
                        </div>
                        <div class="izbor">{{trans('lang.no')}}
                            {!! Form::radio('experience', 'Ne', true) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group radio-fields-holder">

                <div class="col-md-12">
                    <label for=""><span>*</span> {{trans('lang.educated_care')}}?</label>
                    <div class="radio-fields-multiple">
                        <div class="izbor">{{trans('lang.yes')}}
                            {!! Form::radio('education', 'Da', false) !!}
                        </div>
                        <div class="izbor">{{trans('lang.no')}}
                            {!! Form::radio('education', 'Ne', true) !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.submit')}}</button>

            </div>
            <div class="col-md-3"></div>
            <div class="clearfix"></div>

            {!! Form::close() !!}

        </div>
    </div>
    </div>

@endsection