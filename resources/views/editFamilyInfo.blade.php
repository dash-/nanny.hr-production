@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
            {!! Form::open()!!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td> {!! Form::label('family_name', trans('lang.family_name')) !!}
                        {!! Form::input('text', 'family_name', $family->family_name,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('email', trans('lang.email')) !!}
                        {!! Form::input('text', 'email', $email,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>  {!! Form::label('phone', trans('lang.phone')) !!}
                        {!! Form::input('text', 'phone', $family->phone,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('address', trans('lang.address')) !!}
                        {!! Form::input('text', 'address', $family->address,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('address_number', trans('lang.address_number')) !!}
                        {!! Form::input('text', 'address_number', $family->address_number,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('city', trans('lang.city')) !!}
                        {!! Form::input('text', 'city',$family->city,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('state', trans('lang.state')) !!}
                        {!! Form::input('text', 'state', $family->state,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>  {!! Form::label('zip_code', trans('lang.zip_code')) !!}
                        {!! Form::input('text', 'zip_code', $family->zip_code,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('pets', trans('lang.own_a_pet').'?') !!}

                        @if($family->pets == 1)
                            <span>{{trans('lang.yes')}}</span>
                            {!! Form::radio('pets', 1, true) !!}
                            <span>{{trans('lang.no')}}</span>
                            {!! Form::radio('pets', 0, false) !!}
                        @else
                            <span>{{trans('lang.yes')}}</span>
                            {!! Form::radio('pets', 1, false) !!}
                            <span>{{trans('lang.no')}}</span>
                            {!! Form::radio('pets', 0 , true) !!}
                        @endif</td>
                </tr>
                <tr>
                    <td>
                        {!! Form::label('pets_info', trans('lang.which_pet')) !!}
                        {!! Form::input('text', 'pets_info', $family->pets_info,['class'=> 'input-field-regular']) !!}
                    </td>
                </tr>
                </tbody>
            </table>
            <button type="submit" style="margin-right:5%" class="button-regular">{{trans('lang.save_changes')}}</button>
            {!! Form::close()!!}

            <a href="/user">
                <button style="margin-right:5%" class="button-regular">{{trans('lang.back')}}</button>
            </a>


            <a href="/user/edit-parents">
                <button class="button-regular">{{trans('lang.next_page')}}</button>
            </a>
        </div>
    </div>
@endsection