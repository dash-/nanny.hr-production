@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">
            {!! Form::open(['class' => 'form-style'])!!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td> {!! Form::label('name', trans('lang.name')) !!}
                        {!! Form::input('text', 'name', $child->name, ['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('surname', trans('lang.surname')) !!}
                        {!! Form::input('text', 'surname', $child->surname, ['class'=> 'input-field-regular']) !!}</td>
                </tr>


                <tr>
                    <td> {!! Form::label('dob', trans('lang.dob')) !!}
                        {!! Form::input('text', 'dob', $child->dob ? $child->date_of_birth() : null, ['class'=> 'input-field-regular', 'id' => 'datepicker']) !!}</td>
                </tr>


                <tr>
                    <td>{!! Form::label('sex', trans('lang.gender')) !!}
                        @if($child->sex == 'Male')
                            <span>{{trans('lang.male')}}</span>
                            {!! Form::radio('sex', 'Muško', true) !!}
                            <span>{{trans('lang.female')}}</span>
                            {!! Form::radio('sex', 'Žensko', false) !!}
                        @else
                            <span>{{trans('lang.male')}}</span>
                            {!! Form::radio('sex', 'Muško', false) !!}
                            <span>{{trans('lang.female')}}</span>
                            {!! Form::radio('sex', 'Žensko', true) !!}
                        @endif</td>
                </tr>

                <tr>
                    <td>{!! Form::label('allergies', trans('lang.allergies'))!!}
                        {!! Form::checkbox('allergies', '1', $child->allergies)!!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('allergies_info', trans('lang.allergies_info'))!!}
                        {!! Form::input('text', 'allergies_info', $child->allergies_info, ['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('special_note', trans('lang.special_note').'?')!!}
                        {!! Form::input('text', 'special_note', $child->special_note, ['class'=> 'input-field-regular']) !!}</td>
                </tr>


                </tbody>
            </table>
            <button type="submit" class="button-regular">{{trans('lang.save_changes')}}</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection