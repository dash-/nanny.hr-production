@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
        <div style="width:100%">
        @foreach($nannies as $row)
            <div style="background-color: #1FC3F3 ;margin-left:2%; margin-top:2%; padding:1%" class="col-md-3">
                <table class="table borderless" style="text-align: center">
                    <tr>
                        <td>
                            <img src="{{ '/images/nannies/'.$row->image }} " alt="" style="height:130px"/>
                        </td>
                    </tr>
                    <tr>
                        <td>{{ $row->name }} &nbsp;</td>
                    </tr>
                    <tr>
                        <td>{{ $row->surname }} &nbsp;</td>
                    </tr>
                    </tr>
                    <tr>
                        <td>
                            {{ $row->city }} &nbsp;
                        </td>
                    </tr>
                </table>
                <a href="/nannies/profile/{{$row->id}}">
                <button class="nanny-info-button">Pregled</button>
                </a>
                <a href="/nannies/dostupnost-dadilje/{{$row->id}}">
                    <button class="nanny-info-button">Dostupnost</button>
                </a>
            </div>
        @endforeach
        </div>
        </div>
    </div>
@endsection