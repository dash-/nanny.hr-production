@extends('master')

@section('content')

    <div class="content-lower">
        <div style="text-align: center; padding: 5%">

            <p>{{trans('lang.order_transport_individual')}}</p>
            {!! Form::label('from', trans('lang.departure'), ['class' => "raleway-regular"]) !!}
            {!! Form::text('from', '', ['id' => 'order-transport-from', 'class'=> 'form-control', 'placeholder' => trans('lang.enter_departure')]) !!}

            {!! Form::label('to', trans('lang.destination'), ['class' => "raleway-regular"]) !!}
            {!! Form::text('to', '', ['id' => 'order-transport-to', 'class'=> 'form-control', 'placeholder' => trans('lang.enter_destination')]) !!}

            {!! Form::label('date', trans('lang.date_time'), ['class' => "raleway-regular"]) !!}
            {!! Form::text('to', '', ['id' => 'from', 'class'=> 'form-control transport-datetime']) !!}

            <p id="error-message"></p>

            <button style="float:none; margin-top: 3%" class="button-regular" id="calculateDistance">{{trans('lang.proceed')}}</button>
        </div>
    </div>

@endsection