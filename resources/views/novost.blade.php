@extends('master')

@section('content')
    <div class="content-lower only-text">
        <h1>{{trans('lang.news')}}</h1>


        <div class="col-md-4">
            <img style="width: 100%" src="/images/novosti/{{ $novost->image }}"
                 alt="{{ $title }}"
                 title="{{ $title }}">
        </div>

        <div class="col-md-8">
            <h3>
                {{ $title }}
                <small class="pull-right">
                    {{ date("d.m.Y", strtotime($novost->created_at)) }}
                </small>
            </h3>

            <div>
                {!! $content !!}

            </div>
        </div>
        <div class="clearfix"></div>

    </div>
@endsection