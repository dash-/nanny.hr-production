@extends('master')

@section('content')
    <div class="content-lower">

        <table class="table">
            <thead>
            <tr>
                <th>{{trans('lang.name')}}</th>
                <th>{{trans('lang.surname')}}</th>
                <th>{{trans('lang.dob')}}</th>
                <th>{{trans('lang.oib')}}</th>
                <th>{{trans('lang.email')}}</th>
                <th>{{trans('lang.gender')}}</th>
                <th>{{trans('lang.phone')}}</th>
                <th>{{trans('lang.type')}}</th>
                <th>{{trans('lang.relation')}}</th>
                <th>{{trans('lang.options')}}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($parents as $parent)
                <tr>
                    <td>{{$parent->name}}</td>
                    <td>{{$parent->surname}}</td>
                    <td>{{$parent->dob}}</td>
                    <td>{{$parent->jmbg}}</td>
                    <td>{{$parent->email}}</td>
                    <td>{{$parent->sex}}</td>
                    <td>{{$parent->phone}}</td>
                    <td>{{$parent->type()}}</td>
                    <td>{{$parent->relation}}</td>
                    <td><a href="/user/edit-parent-info/{{$parent->id}}">
                            <button>{{trans('lang.manage')}}</button>
                        </a></td>
                    <td><a href="/user/delete-parent/{{$parent->id}}">
                            <button>{{trans('lang.delete')}}</button>
                        </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/user/edit-family-info"><button style="margin-right:5%" class="button-regular">{{trans('lang.previous_page')}}</button></a>

        <a href="/user/add-parent">
            <button style="margin-right:5%" class="button-regular">{{trans('lang.add_parent')}}</button>
        </a>
        <a href="/user/edit-children-info">
            <button style="margin-right:5%" class="button-regular">{{trans('lang.next_page')}}</button>
        </a>


    </div>
@endsection