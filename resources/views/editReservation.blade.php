@extends('master')

@section('content')

    <div class="content-lower">
        {!! Form::model($model) !!}

        <p><b style="font-size: 15px">Cijena rezervacije: {{$reservation->total_price}}kn</b></p>
            {!! Form::label('total_price', 'Dodatna cijena', ['class' => "raleway-regular"]) !!}
            {!! Form::text('total_price', '',['class'=> 'form-control']) !!}

            {!! Form::label('from', 'Od', ['class' => "raleway-regular"]) !!}
            {!! Form::text('from', \App\Helpers\GeneralHelper::dateTimeConcatenate($reservation->reservation_availability->date, $reservation->reservation_availability->start), ['class'=> 'form-control']) !!}

            {!! Form::label('to', 'Do', ['class' => "raleway-regular"]) !!}
            {!! Form::text('to', \App\Helpers\GeneralHelper::dateTimeConcatenate($reservation->reservation_availability->date, $reservation->reservation_availability->end),['class'=> 'form-control']) !!}

            {!! Form::hidden('id', $id) !!}
        <button type="submit">Nastavi</button>

        {!! Form::close() !!}

        @if($first_one)
        <p style="margin-top: 5%">Rezervacija:</p>
        <table class="table" style="margin-top:5%">
            <thead>
            <tr>
                <th>Od</th>
                <th>Do</th>
                <th>Cijena</th>
                <th>Plaćeno</th>
                <th>Vrsta plaćanja</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$first_one->extended_hours_from}}</td>
                    <td>{{$first_one->extended_hours_to}}</td>
                    <td>{{$first_one->extended_price}}</td>
                    <td>{{$first_one->status()}}</td>
                    <td>{{$first_one->payment_type}}</td>
                </tr>
            </tbody>
        </table>
        @endif

        <p style="margin-top:5%">Izmjene</p>
        <table class="table">
            <thead>
            <tr>
                <th>Produženo od</th>
                <th>Produženo do</th>
                <th>Dodatna doplata</th>
                <th>Plaćeno</th>
                <th>Vrsta plaćanja</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($history as $his)
            <tr>
                <td>{{$his->extended_hours_from}}</td>
                <td>{{$his->extended_hours_to}}</td>
                <td>{{$his->extended_price}}</td>
                <td>{{$his->status()}}</td>
                <td>{{$his->payment_type}}</td>
                @if($his->payment_type == 'Direktno')
                    <td><a href="/admin/reservation-history-payed/{{$his->id}}"><button>Plaćeno</button></a></td>
                @endif
            </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection