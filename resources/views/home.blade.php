@extends('master')
@section('slider')

    <span id="top-link-block" class="">
    <a href="#" class="well well-sm">
        <i class="glyphicon glyphicon-chevron-down"></i>
    </a>
</span>
    @include('partials._slider')
@endsection
@section('content')


    <div class="content home-boxes">
        <div class="col-md-6 home-box">
            <img src="/images/home_steps/Jednostavno_rezerviranje.png" alt="Jednostavno rezerviranje"
                 title="Jednostavno rezerviranje">
            <h3>{{trans('lang.easy_reservation')}}</h3>
            <p>{{trans('lang.easy_reservation_text')}}</p>
        </div>
        <div class="col-md-6 home-box">
            <img src="/images/home_steps/provjerene_cuvalice.png" alt="Provjerene čuvalice"
                 title="Provjerene čuvalice">
            <h3>{{trans('lang.tested_nannies')}}</h3>
            <p>{{trans('lang.tested_nannies_text')}}</p>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-6 home-box">
            <img src="/images/home_steps/raspolozivost_24_7.png" alt="Raspoloživost 0-24/7"
                 title="Raspoloživost 0-24/7">
            <h3>{{trans('lang.non_stop_availability')}}</h3>
            <p>{{trans('lang.non_stop_availability_text')}}</p>
        </div>
        <div class="col-md-6 home-box">
            <img src="/images/home_steps/uvijek_dostupna_zamjena.png" alt="Uvijek dostupna zamjena"
                 title="Uvijek dostupna zamjena">
            <h3>{{trans('lang.search_for_nanny')}}</h3>
            <p>{{trans('lang.search_for_nanny_text')}}</p>
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-6">
            <div class="flexDiv">
                <div class="innerFlexDiv">
                    <img src="/images/home_steps/sigurnost.png" alt="Prijevoz djece u sigurnim vozilima"
                         title="Prijevoz djece u sigurnim vozilima" class="full-width">
                </div>
            </div>
        </div>
        <div class="col-md-6 home-box">
            <h3>{{trans('lang.children_transport')}}</h3>
            <p>{{trans('lang.children_transport_text_part1')}}
            </p>
            <p>
                {{trans('lang.children_transport_text_part2')}}
            </p>
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-6 home-box">
            <h3>{{trans('lang.monitor_care')}}
            </h3>
            <p>{{trans('lang.monitor_care_text')}}</p>
        </div>
        <div class="col-md-6">

            <img src="/images/home_steps/tijek_cuvanja_djece.png" alt="Uvid u tijek čuvanja vašeg djeteta"
                 title="Uvid u tijek čuvanja vašeg djeteta" class="full-width">

        </div>

        <div class="clearfix"></div>
        <hr>
        <h1>{{trans('lang.how_to_make_reservation')}}</h1>

        <div class="col-md-3 small-box">
            <img src="/images/home_steps/prijavi_se.png" alt="Prijavi se" title="Prijavi se">
            <h3>{{trans('lang.sign_up_or_register')}}</h3>
            <p>{{trans('lang.sign_up_or_register_text')}}</p>
        </div>
        <div class="col-md-3 small-box lower-box">
            <img src="/images/home_steps/odaberi_termin.png" alt="Odaberi termin čuvanja"
                 title="Odaberi termin čuvanja">
            <h3>{{trans('lang.choose_care_time')}}</h3>
            <p>{{trans('lang.choose_care_time_text')}}</p>
        </div>
        <div class="col-md-3 small-box">
            <img src="/images/home_steps/odaberi_cuvalicu.png" alt="Odaberi čuvalicu" title="Odaberi čuvalicu">
            <h3>{{trans('lang.choose_nanny')}}</h3>
            <p>{{trans('lang.choose_nanny_text')}}</p>
        </div>
        <div class="col-md-3 small-box lower-box">
            <img src="/images/home_steps/placanje.png" alt="Plaćanje" title="Plaćanje">
            <h3>{{trans('lang.payment')}}</h3>
            <p>{{trans('lang.payment_text')}}</p>
        </div>
    </div>

@endsection
