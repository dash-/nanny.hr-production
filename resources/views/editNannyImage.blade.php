@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
            <span>Promijeni sliku za čuvalicu {{ $nanny->name.' '.$nanny->surname}}</span>

            {!! Form::open(['role' => 'form', 'files'=>'true']) !!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td>
                        <label for="slika">Izaberi sliku:</label>
                        {!! Form::file('pozadina') !!}</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" style="margin-right:5%" class="button-regular">Upload sliku</button>
            {!! Form::close() !!}

            <a href="/nannies/profile/{{$nanny->id}}">
                <button class="button-regular">Nazad</button>
            </a>
        </div>
    </div>
@endsection