@extends('master')

@section('content')
    <div class="content-lower">

<div class="form-views">
    <button class="generate-token" style="margin-bottom:2%">Generiši token</button>

        {!! Form::open(['style' => 'margin-bottom:5%'])!!}

        {!! Form::label('token', 'Token')!!}
        {!! Form::text('token', '', ['class' => 'token-input input-field-regular', 'placeholder' => '']) !!}

        {!! Form::label('discount', 'Popust(%)')!!}
        {!! Form::text('discount', '',['class'=> 'input-field-regular']) !!}

        {!! Form::label('type', 'Vrsta')!!}
        {!! Form::select('type', [0 => 'Jednokratni', 1 => 'Visekratni'],['class'=> 'input-field-regular']) !!}

        {!! Form::label('expiration', 'Datum isteka')!!}
        {!! Form::text('expiration', '', ['class'=> 'input-field-regular','id' => 'datetimepicker']) !!}

        <button type="submit" style="margin-top:2%">Potvrdi token</button>

        {!! Form::close()!!}

        <table class="table">
            <thead>
            <tr>
                <th>Token</th>
                <th>Popust</th>
                <th>Vrsta</th>
                <th>Datum isteka</th>
                <th>Iskorišten</th>
                <th>Opcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tokens as $token)
                <tr>
                    <td>{{$token->token}}</td>
                    <td>{{$token->discount}}</td>
                    <td>{{$token->type == 0 ? 'Jednokratni' : 'Višekratni'}}</td>
                    <td>{{$token->expiration}}</td>
                    <td>{{$token->used == 0 ? 'Ne' : 'Da'}}</td>
                    <td><a href="/admin/izbrisi-token/{{$token->id}}">
                            <button>Izbriši</button>
                        </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <a href="/user">
        <button class="generate-token">Nazad</button>
    </a>
</div>
    </div>
@endsection