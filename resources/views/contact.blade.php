@extends('master')


@section('content')
    <div class="content-lower only-text">
        <h3>{{trans('lang.contact')}}</h3>
        {!! trans('lang.contact_text')!!}
        <br>
        e: <a href="mailto: info@nanny.hr">info@nanny.hr</a> <br>
        w: <a href="http://nanny.hr">www.nanny.hr</a> <br>
        m: +385953761155
        <br>
        <br>
        <div class="inline-form">

            {!! Form::model($model) !!}

            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span> {{trans('lang.name_surname')}}</label>
                    {!! Form::text('name', null,[ 'class' => "form-control"]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span>{{trans('lang.email')}}</label>
                    {!! Form::text('email', null,[ 'class' => "form-control"]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span>{{trans('lang.phone')}}</label>

                    {!! Form::text('phone', null,[ 'class' => "form-control"]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label><span>*</span>{{trans('lang.message')}}</label>

                    {!! Form::textarea('message', null,[ 'class' => "form-control"]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div style="">{!! app('captcha')->display() !!}</div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.submit')}}</button>

            </div>
            <div class="col-md-3"></div>
            <div class="clearfix"></div>

            {!! Form::close() !!}
        </div>

    </div>
    </div>
    </div>

@endsection