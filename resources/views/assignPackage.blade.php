@extends('master')

@section('content')
    <div class="content-lower">
        <div class="form-views">
            {!! Form::open() !!}

            {!! Form::label('user_id', 'Korisnik') !!}
            {!! Form::select('user_id', $users, ['class'=> 'input-field-regular'])!!}
            <br/><br/>
            {!! Form::label('package_id', 'Paket') !!}
            {!! Form::select('package_id', $packages, ['class'=> 'input-field-regular']) !!}
            <br/><br/>

            <button type="submit">Aktiviraj</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection