@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">
            <a href="/user">
                <button style="margin-top:5%" class="button-regular">Nazad na profil</button>
            </a>
            Rezervacija uspješna, provjerite vaš email za informacije o rezervaciji.
        </div>
    </div>
@endsection