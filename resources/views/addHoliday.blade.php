@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
            {!! Form::open()!!}

                {!! Form::label('holiday', 'Praznik')!!}
                {!! Form::text('holiday', '', ['class'=> 'input-field-regular'])!!}

                {!! Form::label('date', 'Datum')!!}
                {!! Form::text('date', '', ['class'=> 'input-field-regular', 'id' => 'datepicker'])!!}

            <button>Dodaj</button>

            {!! Form::close()!!}
        </div>
    </div>
@endsection