@extends('master')

@section('content')
    <div class="content-lower">
        <p>Za kojeg korisnika želite napraviti prevoz?</p>
        <table class="table">
            <thead>
            <tr>
                <th>Obitelj</th>
                <th>Telefon</th>
                <th>Adresa</th>
                <th>Grad</th>
                <th>Opcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->family_name}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->address}} {{$user->address_number}}</td>
                    <td>{{$user->city}}</td>
                    <td><a href="/transport/transport/{{$user->user_id}}"><button>Naruči prevoz</button></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $users->render() !!}
    </div>
@endsection