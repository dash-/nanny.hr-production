@extends('master')

@section('content')
    <div class="content-lower">
        <div class="form-views">
            {{--<div style="width:70%; color:black; margin-left:10%" class="hero-regular">--}}
                <table class="table" style="margin-bottom:5%">
                    <thead>
                    <tr>
                        <th>Za dan</th>
                        <th>Početak</th>
                        <th>Kraj</th>
                        <th>Adresa</th>
                        <th>Djeca</th>
                        <th>Čuvalica</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reservationAvailabilities as $availability)
                        <tr>
                            <td>{{$availability->date}}</td>
                            <td>{{$availability->start}}</td>
                            <td>{{$availability->end}}</td>
                            <td>{{$availability->address}}</td>
                            <td>{{$availability->children}}</td>
                            <td>{{$availability->nanny}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
@endsection