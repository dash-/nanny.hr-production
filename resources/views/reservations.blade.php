@extends('master')

@section('content')

    <div class="content-lower">

        <table class="table">
            <tr>
                <th>Korisnik</th>
                <th>Čuvalica</th>
                <th>Vrsta plaćanja</th>
                <th>Ukupno</th>
                <th>Plaćeno</th>
                <th></th>
                <th>Opcije</th>
                <th></th>
                <th></th>
            </tr>
            <tbody>
            @foreach($reservations as $reservation)
                <tr>
                    <td>
                        <a href="{{ URL::to('/admin/rezervacije-korisnika/'.$reservation->user_id) }}">
                            {{$reservation->user->family->family_name}}
                        </a>
                    </td>
                    <td>
                        @if($reservation->reservation_availability && $reservation->reservation_availability->availability)
                            <a href="{{ URL::to('/nannies/profile/'.$reservation->reservation_availability->availability->nanny_id) }}">{{$reservation->nanny }}
                            </a>
                        @endif
                    </td>
                    <td>{{$reservation->payment_type}}</td>
                    <td>{{$reservation->total_price}}</td>
                    <td>{{$reservation->sum_payed()}}</td>
                    <td>@if($reservation->payment_type == 'Direktno' && $reservation->status == 0 && $reservation->status != 2)
                            <a href="/admin/direct-payment-success/{{$reservation->id}}">
                                <button>Plaćeno</button>
                            </a>
                        @endif
                    </td>
                    <td>
                        <a href="/admin/edit-reservation/{{$reservation->id}}">
                            <button>Izmjeni rezervaciju</button>
                        </a>
                    </td>
                    @if($reservation->status != 2)
                        <td>
                            <a href="/admin/cancel-reservation/{{$reservation->id}}">
                                <button>Otkaži rezervaciju</button>
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $reservations->render() !!}
    </div>
@endsection