@extends('master')

@section('content')

    <div class="content-lower">
        <h3>{{$content->title}}</h3>
        <div>
            {!! $content->text !!}
        </div>
    </div>

@endsection