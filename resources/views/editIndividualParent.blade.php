@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            {!! Form::open() !!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td> {!! Form::label('name', trans('lang.name')) !!}
                        {!! Form::input('text', 'name', $parent->name,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('surname', trans('lang.surname')) !!}
                        {!! Form::input('text', 'surname', $parent->surname,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('dob', trans('lang.dob')) !!}
                        {!! Form::input('text', 'dob', $parent->dob,['class'=> 'input-field-regular', 'id' => 'datepicker']) !!}</td>
                </tr>
                @if(Auth::user()->role == 1)
                <tr>
                    <td> {!! Form::label('jmbg', trans('lang.oib')) !!}
                        {!! Form::input('text', 'jmbg', $parent->jmbg,['class'=> 'input-field-regular']) !!}</td>
                </tr>
                @endif

                <tr>
                    <td>  {!! Form::label('email', trans('lang.email')) !!}
                        {!! Form::input('text', 'email', $parent->email,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('sex', trans('lang.gender')) !!}
                        @if($parent->sex == 'Male')
                            <span>{{trans('lang.male')}}</span>
                            {!! Form::radio('sex', 'Muško', true) !!}
                            <span>{{trans('lang.female')}}</span>
                            {!! Form::radio('sex', 'Žensko', false) !!}
                        @else
                            <span>{{trans('lang.male')}}</span>
                            {!! Form::radio('sex', 'Muško', false) !!}
                            <span>{{trans('lang.female')}}</span>
                            {!! Form::radio('sex', 'Žensko', true) !!}
                        @endif</td>
                </tr>

                <tr>
                    <td>{!! Form::label('phone', trans('lang.phone')) !!}
                        {!! Form::input('text', 'phone', $parent->phone,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('relation', trans('lang.relation').'?') !!}
                        {!! Form::input('text', 'relation', $parent->relation,['class'=> 'input-field-regular']) !!}</td>
                </tr>

                </tbody>
            </table>
            <button type="submit" class="button-regular">{{trans('lang.save_changes')}}</button>
            {!! Form::close()!!}
        </div>
    </div>
@endsection