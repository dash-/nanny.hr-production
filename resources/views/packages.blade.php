@extends('master')

@section('content')
    <div class="content-lower">
        <p style="font-size:20px; font-weight: bold">Paketi:</p>
        <table class="table">
            <thead>
            <tr>
                <th>Paket</th>
                <th>Broj korištenja</th>
                <th>Cijena</th>
                <th>Opcije</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($packages as $package)
                <tr>
                    <td>{{$package->package_name}}</td>
                    <td>{{$package->usage}}</td>
                    <td>{{$package->price}}</td>
                    <td><a href="/admin/obrisi-paket/{{$package->id}}">
                            <button>Obriši paket</button>
                        </a></td>
                    <td><a href="/admin/uredi-paket/{{$package->id}}">
                            <button>Uredi paket</button>
                        </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/admin/dodaj-paket">
            <button>Dodaj paket</button>
        </a>

        <p style="font-size:20px; font-weight:bold; margin-top:5%">Aktivirani paketi:</p>
        <table class="table">
            <thead>
            <tr>
                <th>Korisnik</th>
                <th>Paket</th>
                <th>Preostalo korištenja</th>
                <th>Aktivno</th>
                <th>Opcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach($used_packages as $used)
                <tr>
                    <td>{{$used->user ? $used->user->full_name() : ""}}</td>
                    <td>{{$used->package_d ? $used->package_d->package_name : ""}}</td>
                    <td>{{$used->no_of_uses}}</td>
                    <td>{{$used->active ? 'Da' : 'Ne'}}</td>
                    @if($used->active == 0)
                        <td><a href="/admin/aktiviraj-paket/{{$used->id}}"><button>Aktiviraj</button></a></td>
                    @endif
                    <td><a href="/admin/obrisi-dodijeljeni-paket/{{$used->id}}"><button>Obrisi</button></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/admin/dodijeli-paket">
            <button>Aktiviraj paket</button>
        </a>

    </div>
@endsection