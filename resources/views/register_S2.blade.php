@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.parent')}}</h3>
                {!! Form::model($user) !!}
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="parent_1_name"><span>*</span> {{trans('lang.name')}}</label>
                        {!! Form::text('parent_1_name', null,['class'=> 'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        <label for="parent_1_surname"><span>*</span> {{trans('lang.surname')}}</label>
                        {!! Form::text('parent_1_surname', null,['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="dob"><span>*</span> {{trans('lang.dob')}}</label>
                        {!! Form::text('dob', null,['class'=> 'form-control', 'id' => 'dob']) !!}
                    </div>
                    <div class="col-md-6">
                        <label for="jmbg"><span>*</span> {{trans('lang.oib')}}</label>
                        <span class="tooltip-mark" data-toggle="tooltip" data-placement="right"
                              title="{{trans('lang.oib_title')}}">?</span>
                        {!! Form::text('jmbg', null,['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="parent_1_email"><span>*</span> {{trans('lang.email')}}</label>
                        {!! Form::text('parent_1_email', null,['class'=> 'form-control']) !!}

                    </div>
                    <div class="col-md-6">
                        <label for="parent_1_phone"><span>*</span> {{trans('lang.phone')}}</label>
                        {!! Form::text('parent_1_phone', null,['class'=> 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        <label for="parent_1_sex"><span>*</span> {{trans('lang.gender')}}</label>
                        <div class="radio-fields">
                            <div class="izbor">{{trans('lang.male')}}
                                {!! Form::radio('parent_1_sex', 'Muško', true) !!}
                            </div>
                            <div class="izbor">{{trans('lang.female')}}
                                {!! Form::radio('parent_1_sex', 'Žensko', false) !!}
                            </div>
                        </div>

                    </div>
                        </div>
                <div class="clearfix"></div>
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-submit">{{trans('lang.next')}}</button>
                </div>
                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="stripe-line" style="width: 60%">
                    <div class="step-number">3</div>
                </div>
            </div>
        </div>
    </div>
@endsection
