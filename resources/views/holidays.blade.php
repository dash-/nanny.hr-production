@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
            <table class="table">
                <thead>
                <tr>
                    <th>Praznik</th>
                    <th>Datum</th>
                    <th>Opcije</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($holidays as $holiday)
                    <tr>
                        <td>{{$holiday->holiday}}</td>
                        <td>{{$holiday->date}}</td>
                        <td><a href="/admin/izbrisi-praznik/{{$holiday->id}}"><button>Obriši</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <a href="/admin/dodaj-praznik"><button class="button-regular">Dodaj praznik</button></a>

            {!! $holidays->render() !!}
        </div>
    </div>
@endsection