@extends('master')

@section('content')
    <div class="content-lower">

        <div class="">
            {!! Form::open()!!}

            <table class="table">
                <tr>
                    <th>
                        Broj djece
                    </th>
                    <th>
                        Dan
                    </th>
                    <th>
                        Noć
                    </th>
                    <th>
                        Vikend
                    </th>
                    <th>
                        Trajanje
                    </th>
                    <th>
                        Cijena
                    </th>
                    <th>
                        Agencijska naknada
                    </th>
                </tr>
                @foreach($prices as $price)
                    <tr>
                        <td>
                            {{ $price->children }}
                        </td>
                        <td>
                            {{ $price->day ? "Da" : "Ne" }}
                        </td>
                        <td>
                            {{ $price->night ? "Da" : "Ne" }}
                        </td>
                        <td>
                            {{ $price->weekend ? "Da" : "Ne" }}
                        </td>
                        <td>
                            {{ $price->duration }}
                        </td>
                        <td>
                            {!! Form::text("price[".$price->id."]", $price->price, ['class' => 'form-control', 'required']) !!}
                        </td>
                        <td>
                            {!! Form::text("agency_price[".$price->id."]", $price->agency_price, ['class' => 'form-control', 'required']) !!}
                        </td>
                    </tr>
                @endforeach
            </table>


            <button>Spremi</button>

            {!! Form::close()!!}
        </div>
    </div>
@endsection