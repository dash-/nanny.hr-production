@extends('master')

@section('content')
    <div class="content-lower">

        <table class="table">
            <thead>
            <tr>
                <th>{{trans('lang.name')}}</th>
                <th>{{trans('lang.surname')}}</th>
                <th>{{trans('lang.dob')}}</th>
                <th>{{trans('lang.gender')}}</th>
                <th>{{trans('lang.allergies')}}</th>
                <th>{{trans('lang.allergies_info')}}</th>
                <th>{{trans('lang.special_note')}}</th>
                <th>{{trans('lang.options')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($children as $child)
                <tr>
                    <td>{{$child->name }}</td>
                    <td>{{$child->surname}}</td>
                    <td>{{$child->date_of_birth()}}</td>
                    <td>{{$child->sex}}</td>
                    <td>{{$child->allergies()}}</td>
                    <td>{{$child->allergies_info}}</td>
                    <td>{{$child->special_note}}</td>
                    <td><a href="/user/edit-individual-child/{{$child->id}}">
                            <button>{{trans('lang.manage')}}</button>
                        </a></td>
                    <td><a href="/user/delete-child/{{$child->id}}">
                            <button>{{trans('lang.delete')}}</button>
                        </a></td>
                </tr>
            @endforeach

            </tbody>
        </table>

        <a href="/user/edit-parents"><button style="margin-right:5%" class="button-regular">{{trans('lang.previous_page')}}</button></a>
        <a href="/user/add-children">
            <button style="margin-right:5%" class="button-regular">{{trans('lang.add_child')}}</button>
        </a>
        <a href="/user"><button style="margin-right:5%" class="button-regular">{{trans('lang.finish')}}</button></a>

    </div>
@endsection