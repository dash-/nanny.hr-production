@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            @if(isset($details) && $details)
                <table class="table">
                    @foreach($details as $key => $info)
                        <tr>
                            <td>
                                <p>Za period: {{\App\Helpers\GeneralHelper::displayLongDate($key)}}</p>

                                <div>
                                    <p>Za djecu:</p>
                                    <ul>
                                        @if(isset($info['info']['children']) != 0)
                                            @foreach($info['info']['children'] as $childKey => $child)
                                                <li>{{$child}}</li>
                                            @endforeach
                                        @endif

                                        @if($info['info']['other_children'] != '')
                                            <li> + {{$info['info']['other_children']}}</li>
                                        @endif
                                    </ul>

                                    @if(isset($info['info']['address_other']))
                                        <p>Adresa: {{$info['info']['address_other']}}</p>
                                    @endif
                                    @if(isset($info['info']['special_note']))
                                        <p>Posebne potrebe, zahtjevi i druge
                                            napomene: {{$info['info']['special_note']}}</p>
                                    @endif

                                    <p>Dadilje:</p>
                                    <ul>
                                        @if(isset($info['chosen_nannies']))
                                            @foreach($info['chosen_nannies'] as $nanny)
                                                <li>{{$nanny->name.' '.$nanny->surname}}</li>
                                            @endforeach

                                        @else
                                            <li>Slobodni odabir čuvalice</li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </table>

            @endif
            <p>Desila se greška sa vašom rezervacijom. Ako se greška nastavi dešavati, molim Vas kontaktirajte nas putem
                <a href="/kontakt">kontakt forme </a> ili
                <a href="mailto: info@nanny.hr">emaila</a></p>

        </div>
    </div>


@endsection