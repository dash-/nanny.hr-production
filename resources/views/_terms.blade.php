<div class="terms">
    <?php $content = \App\Models\Content::whereSlug('opci-uvjeti')->first(); ?>

    @if($content)
        {!! $content->text !!}
    @endif
</div>
