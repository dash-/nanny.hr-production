@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            <table class="table">
                <tbody>
                    <tr>
                        <td>{{trans('lang.departure')}}</td>
                        <td>{{$origin}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.destination')}}</td>
                        <td>{{$destination}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.distance')}}</td>
                        <td>{{$distance}}m</td>
                    </tr>
                    <tr>
                        <td>{{trans('lang.date_time_display')}}</td>
                        <td>{{$date}}</td>
                    </tr>
                </tbody>
            </table>
            <div style="width:100%;height: 1px; background-color: black"></div>
            <b style="font-size: 20px">{{trans('lang.total')}}:<b id="total-price">{{$price}}</b> kn</b>

            <p style="margin-top:2%; display: none;">
                <a href="" data-toggle="modal"
                   data-target="#myModal">{!! Form::label('terms', trans('lang.accept_terms'))!!}</a>
                {!! Form::checkbox('terms', 1, true, ['id' => 'terms'])!!}
            </p>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 style="text-transform: uppercase" class="modal-title" id="myModalLabel"
                                style="text-align: center; font-weight: bold">terms_of_use</h4>
                        </div>
                        <div class="modal-body">
                            <?= view('_terms') ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{trans('lang.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>

            {{--<form action="" method="post">--}}
            {!! Form::open(['url' => '/mpayment/payment'])!!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td>{!! Form::label('discount_token', trans('lang.promo_code'))!!}
                        {!! Form::text('discount_token', '',['class'=> 'input-field-regular', 'id' => 'disc-token'])!!}</td>
                </tr>
                @if(Auth::user()->role == 1)
                    <tr>
                        <td>{!! Form::label('custom_price', 'Administratorska cijena:')!!}
                            {!! Form::text('custom_price', '',['class'=> 'input-field-regular', 'id' => 'custom-price'])!!}
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
            <button type="submit" class="button-regular payment-button" id="paypal-button"
                    disabled="disabled">{{trans('lang.pay_with_paypal')}}<img src="/images/paypal.png" alt=""/></button>
            <div>
                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>
            {!! Form::close()!!}

            <div style="margin-top:2%">
                <button class="pay-way-transport button-regular payment-button" id="payway-button"
                        disabled="disabled">{{trans('lang.pay_with_payway')}}<img src="/images/payway.png" alt=""
                                                                                  style="margin-left:5%"/></button>

                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>

            <div style="margin-top:2%">
                <button class="direct-payment-transport button-regular payment-button" id="direct-payment"
                        disabled="disabled">{{trans('lang.direct_payment')}}</button>
            </div>
            {{--</form>--}}

        </div>
    </div>


@endsection