@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            {!! Form::open() !!}

            <table class="table borderless" style="float:left;width:45%">
                <thead>
                <tr>
                    <th>Hrvatska verzija</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>
                        {{--{!! Form::label('name', 'Ime')!!}--}}
                        {!! Form::text('name', $nanny->name,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Ime']) !!}</td>
                </tr>
                <tr>
                    <td>
                        {{--{!! Form::label('surname', 'Prezime')!!}--}}
                        {!! Form::text('surname', $nanny->surname,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Prezime']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('dob', 'Datum rođenja')!!}--}}
                        {!! Form::text('dob', $nanny->dob, ['class'=> 'input-field-regular-add-nanny', 'id' => 'datepicker', 'placeholder' => 'Datum rođenja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('email', 'Email')!!}--}}
                        {!! Form::text('email', $nanny->email, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Email']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('phone', 'Telefon')!!}--}}
                        {!! Form::text('phone', $nanny->phone, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Telefon']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('address', 'Adresa')!!}--}}
                        {!! Form::text('address', $nanny->address, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Adresa']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('address', 'Adresa')!!}--}}
                        {!! Form::text('city', $nanny->city, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Grad']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('oib', 'OIB')!!}--}}
                        {!! Form::text('oib', $nanny->oib, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'OIB']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('agree_to_surveillance', 'Pristajem na nadzor uz Baby-monitor?')!!}--}}
                        {!! Form::text('agree_to_surveillance', $nanny->agree_to_surveillance, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Pristajem na nadzor uz Baby-monitor?']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('description', 'Kratki opis')!!}--}}
                        {!! Form::textarea('description', $nanny->description,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Kratki opis']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('years_of_nanny_exp', 'Godine iskustva čuvanja djece')!!}--}}
                        {!! Form::text('years_of_nanny_exp', $nanny->years_of_nanny_exp,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Godine iskustva čuvanja djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('year_of_payed_nanny_exp', 'Godine iskustva na plaćenim poslovima čuvanja djece')!!}--}}
                        {!! Form::text('year_of_payed_nanny_exp', $nanny->year_of_payed_nanny_exp,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Godine iskustva na plaćenim poslovima čuvanja djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('exp_with_age', 'Najviše iskustva imam s uzrastom')!!}--}}
                        {!! Form::text('exp_with_age', $nanny->exp_with_age,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Najviše iskustva imam s uzrastom']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('can_accept_urgent', 'Mogu prihvatiti posao hitno')!!}--}}
                        {!! Form::text('can_accept_urgent', $nanny->can_accept_urgent,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Mogu prihvatiti posao hitno']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('maximum_distance', 'Maksimalna udaljenost od doma do mjesta čuvanja')!!}--}}
                        {!! Form::text('maximum_distance', $nanny->maximum_distance,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Maksimalna udaljenost od doma do mjesta čuvanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('places', 'Mjesta u kojima mogu obavljati čuvanja')!!}--}}
                        {!! Form::text('places', $nanny->places,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Mjesta u kojima mogu obavljati čuvanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_car', 'Vlastiti automobil')!!}--}}
                        {!! Form::text('own_car', $nanny->own_car,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Vlastiti automobil']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_drivers_licence', 'Vozačka dozvola')!!}--}}
                        {!! Form::text('own_drivers_licence', $nanny->own_drivers_licence,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Vozačka dozvola']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_level', 'Stupanj obrazovanja')!!}--}}
                        {!! Form::text('education_level', $nanny->education_level,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Stupanj obrazovanja']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_title', 'Zvanje')!!}--}}
                        {!! Form::text('education_title', $nanny->education_title,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Zvanje']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('languages', 'Znanje stranih jezika')!!}--}}
                        {!! Form::text('languages', $nanny->languages,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Znanje stranih jezika']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_skills', 'Dodatne vještine')!!}--}}
                        {!! Form::textarea('additional_skills', $nanny->additional_skills,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Dodatne vještine']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_qualifications', 'Dodatne kvalifikacije')!!}--}}
                        {!! Form::textarea('additional_qualifications', $nanny->additional_qualifications,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Dodatne kvalifikacije']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('managing_with_pets', 'Snalaženje s kućnim ljubimcima')!!}--}}
                        {!! Form::text('managing_with_pets', $nanny->managing_with_pets,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Snalaženje s kućnim ljubimcima']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_sick', 'Briga za bolesnu djecu')!!}--}}
                        {!! Form::text('care_sick', $nanny->care_sick,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Briga za bolesnu djecu']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_disabled', 'Briga za djecu s posebnim potrebama')!!}--}}
                        {!! Form::text('care_disabled', $nanny->care_disabled,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Briga za djecu s posebnim potrebama']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('is_parent', 'Roditelj sam')!!}--}}
                        {!! Form::text('is_parent', $nanny->is_parent,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Roditelj sam']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('number_of_children', 'Broj djece')!!}--}}
                        {!! Form::text('number_of_children', $nanny->number_of_children,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Broj djece']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('smoker', 'Jesam li pusač')!!}--}}
                        {!! Form::text('smoker', $nanny->smoker,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Jesam li pusač']) !!}</td>
                </tr>

                </tbody>
            </table>

            <table class="table borderless" style="width:45%; float:right">
                <thead>
                <tr>
                    <th>Engleska verzija</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>
                        {{--{!! Form::label('agree_to_surveillance', 'Pristajem na nadzor uz Baby-monitor?')!!}--}}
                        {!! Form::text('agree_to_surveillance_eng', $nanny->agree_to_surveillance_eng, ['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I agree to surveilance?']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('description', 'Kratki opis')!!}--}}
                        {!! Form::textarea('description_eng', $nanny->description_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Short description']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('years_of_nanny_exp', 'Godine iskustva čuvanja djece')!!}--}}
                        {!! Form::text('years_of_nanny_exp_eng', $nanny->years_of_nanny_exp_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Years of experience in babysitting']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('year_of_payed_nanny_exp', 'Godine iskustva na plaćenim poslovima čuvanja djece')!!}--}}
                        {!! Form::text('year_of_payed_nanny_exp_eng', $nanny->year_of_payed_nanny_exp_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Years of experience in earning money as a babysitter']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('exp_with_age', 'Najviše iskustva imam s uzrastom')!!}--}}
                        {!! Form::text('exp_with_age_eng', $nanny->exp_with_age_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I am most experienced in working with children between the ages of']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('can_accept_urgent', 'Mogu prihvatiti posao hitno')!!}--}}
                        {!! Form::text('can_accept_urgent_eng', $nanny->can_accept_urgent_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I can respond to urgent demand for a babysitter']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('maximum_distance', 'Maksimalna udaljenost od doma do mjesta čuvanja')!!}--}}
                        {!! Form::text('maximum_distance_eng', $nanny->maximum_distance_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Maximum travel distance to the place of babysitting I accept is']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('places', 'Mjesta u kojima mogu obavljati čuvanja')!!}--}}
                        {!! Form::text('places_eng', $nanny->places_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Places and venues I am comfortable babysitting at']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_car', 'Vlastiti automobil')!!}--}}
                        {!! Form::text('own_car_eng', $nanny->own_car_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Personal car']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('own_drivers_licence', 'Vozačka dozvola')!!}--}}
                        {!! Form::text('own_drivers_licence_eng', $nanny->own_drivers_licence_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Drivers license']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_level', 'Stupanj obrazovanja')!!}--}}
                        {!! Form::text('education_level_eng', $nanny->education_level_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Education level']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('education_title', 'Zvanje')!!}--}}
                        {!! Form::text('education_title_eng', $nanny->education_title_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Occupation']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('languages', 'Znanje stranih jezika')!!}--}}
                        {!! Form::text('languages_eng', $nanny->languages_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Foreign languages']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_skills', 'Dodatne vještine')!!}--}}
                        {!! Form::textarea('additional_skills_eng', $nanny->additional_skills_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Additional skills']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('additional_qualifications', 'Dodatne kvalifikacije')!!}--}}
                        {!! Form::textarea('additional_qualifications_eng', $nanny->additional_qualifications_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Additional qualifications']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('managing_with_pets', 'Snalaženje s kućnim ljubimcima')!!}--}}
                        {!! Form::text('managing_with_pets_eng', $nanny->managing_with_pets_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you comfortable around pets']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_sick', 'Briga za bolesnu djecu')!!}--}}
                        {!! Form::text('care_sick_eng', $nanny->care_sick_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you willing to care for an ill child']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('care_disabled', 'Briga za djecu s posebnim potrebama')!!}--}}
                        {!! Form::text('care_disabled_eng', $nanny->care_disabled_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Are you willing to care for a child with special needs']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('is_parent', 'Roditelj sam')!!}--}}
                        {!! Form::text('is_parent_eng', $nanny->is_parent_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'I am a parent']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('number_of_children', 'Broj djece')!!}--}}
                        {!! Form::text('number_of_children_eng', $nanny->number_of_children_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Number of children']) !!}</td>
                </tr>

                <tr>
                    <td>
                        {{--{!! Form::label('smoker', 'Jesam li pusač')!!}--}}
                        {!! Form::text('smoker_eng', $nanny->smoker_eng,['class'=> 'input-field-regular-add-nanny', 'placeholder' => 'Smoker']) !!}</td>
                </tr>

                </tbody>
            </table>

            <button style="margin-right:5%" class="button-regular" type="submit">Spremi</button>

            {!! Form::close() !!}

            <a href="/nannies/profile/{{$nanny->id}}">
                <button class="button-regular">Nazad</button>
            </a>
        </div>
    </div>
@endsection