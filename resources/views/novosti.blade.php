@extends('master')

@section('content')
    <div class="content-lower only-text">
        <h1>{{trans('lang.news')}}</h1>


        @foreach($novosti as $novost)
            <div class="col-md-3">
                <a href="{{ URL::to('novosti/novost/'.($language == "hr" ? $novost->slug_hr : $novost->slug_en)) }}">

                    <img style="width: 100%" src="/images/novosti/{{ $novost->image }}"
                         alt="{{ $language == "hr" ? $novost->title_hr : $novost->title_en }}"
                         title="{{ $language == "hr" ? $novost->title_hr : $novost->title_en }}">
                </a>
            </div>

            <div class="col-md-9">
                <a href="{{ URL::to('novosti/novost/'.($language == "hr" ? $novost->slug_hr : $novost->slug_en)) }}">
                    <h3>
                        <small>
                            @if($language=="hr")
                                {{ $novost->title_hr }}
                            @else
                                {{ $novost->title_en }}
                            @endif
                        </small>
                    </h3>
                </a>

                <div>
                    <a href="{{ URL::to('novosti/novost/'.($language == "hr" ? $novost->slug_hr : $novost->slug_en)) }}">

                        @if($language=="hr")
                            {{ $novost->summary_hr }}
                        @else
                            {{ $novost->summary_en }}
                        @endif
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr>
        @endforeach

    </div>
@endsection