@extends('master')

@section('content')

    <div class="content-lower">

    <div class="form-views">
            @if(Auth::check())
                <b> {{trans('lang.pick_a_nanny')}}. </b>
                <p><b>*{{trans('lang.replacement_nanny')}}.</b></p>
            @else
                <b>{{trans('lang.available_nannies')}}:</b>
            @endif
            @foreach($data as $key => $time)
                <table class="table available-nannies">
                    <thead>
                    <tr>
                        <th>{{trans('lang.for_period')}}: {{\App\Helpers\GeneralHelper::displayLongDate($key)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($time != 0)
                        <tr>
                            <td>
                                @foreach($time['availability'] as $key2 => $nanny)

                                    <div style="background-color: #1FC3F3 ;margin-left:2%; margin-top:2%; padding:1%"
                                         class="col-md-3">
                                        <table class="table borderless"
                                               style="text-align: center; background-color: #1FC3F3">
                                            <tr>
                                                <td>
                                                    @if(Auth::check())
                                                        <img src="{{ '/images/nannies/'.$nanny['image'] }} " alt=""
                                                             style="height:130px; max-width: 200px"/>
                                                    @else
                                                        <img src="/images/sillouethe.png" alt=""
                                                             style="height:130px"/>
                                                    @endif
                                                </td>
                                            </tr>
                                            @if(Auth::check())
                                                <tr>
                                                    <td>{{ $nanny['name'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $nanny['surname'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $nanny['city'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {!! Form::open() !!}
                                                        {!! Form::checkbox('nannies_chosen['.$key.']['.$key2.']', '') !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="/availability/nanny-info/{{$nanny['id']}}">
                                                            <button type="button">{{trans('lang.more')}}</button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td>{{ $nanny['name'] }}</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                @endforeach
                            </td>
                        </tr>

                    @else
                        <tr>
                            <td>{{trans('lang.no_available_nannies')}}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @endforeach
            @if(Auth::check())
                @if($time != 0)
                    <button style="margin-bottom:2%" class="button-regular" type="submit">{{trans('lang.proceed')}}</button>
                    {!! Form::close() !!}
                @else
                    <a href="/availability/availability">
                        <button style="margin-bottom:2%" class="button-regular" type="button">{{trans('lang.proceed')}}</button>
                    </a>
                @endif
            @else
                <div style="vertical-align: bottom">
                    <span>{{trans('lang.more_about_available_nannies')}},  <a href="/#login">{{trans('lang.login')}} {{trans('lang.or')}} {{trans('lang.register')}}</a></span>
                </div>

            @endif
        </div>
    </div>
@endsection