@extends('master')
@section('content')

    <div class="content-lower">

        <div class="form-views">
            <div style="float:left; margin-left:5%; margin-right:10%">
                <table class="table borderless" style="text-align: center">
                    <tr>
                        <td><img src="{{ '/images/nannies/'. $img->image }} " alt=""/></td>
                    </tr>
                    <tr>
                        <td> {{ $nanny->name }} </td>
                    </tr>
                    <tr>
                        <td> {{ $nanny->surname}}</td>
                    </tr>
                    <tr>
                        <td>{{ \App\Helpers\GeneralHelper::dayMonthYearDate($nanny->dob) }}</td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->email }} </td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->phone }} </td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->address }} </td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->city }} </td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->oib }} </td>
                    </tr>
                    <tr>
                        <td>{{ $nanny->description }} </td>
                    </tr>
                    <tr>
                        <td><a href="/nannies/dostupnost-dadilje/{{$nanny->id}}">
                                <button>Dostupnost čuvalice</button>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="/nannies/edit/{{$nanny->id}}">
                                <button>Uredi info čuvalice</button>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="/nannies/promijeni-sliku/{{$nanny->id}}">
                                <button>Promjeni sliku</button>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="/nannies">
                                <button>Nazad</button>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <br>
                            <br>
                            <br>
                            <a href="/nannies/delete/{{$nanny->id}}">
                                <button class="">Obrisi čuvalicu</button>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="float:left; width:50%">
                <table class="table">

                    <tr>
                        <td>Godine iskustva čuvanja djece:</td>
                        <td>{{$nanny->years_of_nanny_exp}}</td>
                    </tr>
                    <tr>
                        <td>Godine iskustva na plaćenim poslovima čuvanja djece:</td>
                        <td>{{$nanny->year_of_payed_nanny_exp}}</td>
                    </tr>
                    <tr>
                        <td>Najviše iskustva imam s uzrastom:</td>
                        <td>{{$nanny->exp_with_age}}</td>
                    </tr>
                    <tr>
                        <td>Mogu prihvatiti posao hitno:</td>
                        <td>{{$nanny->can_accept_urgent}}</td>
                    </tr>
                    <tr>
                        <td>Maksimalna udaljenost od doma do mjesta čuvanja:</td>
                        <td>{{$nanny->maximum_distance}}</td>
                    </tr>
                    <tr>
                        <td>Mjesta u kojima mogu obavljati čuvanja:</td>
                        <td>{{$nanny->places}}</td>
                    </tr>
                    <tr>
                        <td>Vlastiti automobil:</td>
                        <td>{{$nanny->own_car}}</td>
                    </tr>
                    <tr>
                        <td>Vozačka dozvola:</td>
                        <td>{{$nanny->own_drivers_licence}}</td>
                    </tr>
                    <tr>
                        <td>Stupanj obrazovanja:</td>
                        <td>{{$nanny->education_level}}</td>
                    </tr>
                    <tr>
                        <td>Zvanje:</td>
                        <td>{{$nanny->education_title}}</td>
                    </tr>
                    <tr>
                        <td>Znanje stranih jezika:</td>
                        <td>{{$nanny->languages}}</td>
                    </tr>
                    <tr>
                        <td>Dodatne vještine:</td>
                        <td>{{$nanny->additional_skills}}</td>
                    </tr>
                    <tr>
                        <td>Dodatne kvalifikacije:</td>
                        <td>{{$nanny->additional_qualifications}}</td>
                    </tr>
                    <tr>
                        <td>Snalaženje s kućnim ljubimcima:</td>
                        <td>{{$nanny->managing_with_pets}}</td>
                    </tr>
                    <tr>
                        <td>Briga za bolesnu djecu:</td>
                        <td>{{$nanny->care_sick}}</td>
                    </tr>
                    <tr>
                        <td>Briga za djecu s posebnim potrebama:</td>
                        <td>{{$nanny->care_disabled}}</td>
                    </tr>
                    <tr>
                        <td>Roditelj sam:</td>
                        <td>{{$nanny->is_parent}}</td>
                    </tr>
                    <tr>
                        <td>Broj djece:</td>
                        <td>{{$nanny->number_of_children}}</td>
                    </tr>
                    <tr>
                        <td>Jesam li pusač:</td>
                        <td>{{$nanny->smoker}}</td>
                    </tr>
                    <tr>
                        <td>Pristajem na nadzor uz Baby-monitor?</td>
                        <td>{{$nanny->agree_to_surveillance}}</td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

@endsection