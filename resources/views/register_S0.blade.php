@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 style="text-transform: uppercase" class="modal-title" id="myModalLabel"
                        style="text-align: center; font-weight: bold">terms_of_use</h4>
                </div>
                <div class="modal-body">
                    <?= view('_terms') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{trans('lang.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.register')}}</h3>
                {!! Form::model($user) !!}
                <div class="form-group">

                    <label for="family"><span>*</span> {{trans('lang.family_name')}}</label>
                    {!! Form::text('family', null, [ 'class' => "form-control"]) !!}
                </div>
                <div class="form-group">
                    <label for="email"><span>*</span> {{trans('lang.email')}}</label>
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}

                </div>
                <div class="form-group">
                    <label for="password"><span>*</span> {{trans('lang.password')}}</label>
                    {!! Form::password('password', ['class' => 'form-control'])!!}
                </div>
                <div class="form-group">
                    <a href="" data-toggle="modal"
                       data-target="#myModal">{!! Form::label('terms', trans('lang.accept_terms'))!!}</a>
                    {!! Form::checkbox('terms', 1, false)!!}
                </div>

                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.next')}}</button>
                </div>


                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="stripe-line" style="width: 20%">
                    <div class="step-number">1</div>
                </div>
            </div>
        </div>
    </div>
@endsection