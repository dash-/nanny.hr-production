<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <title>Nanny.hr</title>

    <link href='https://fonts.googleapis.com/css?family=Comfortaa&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/js/datetimepicker-master/jquery.datetimepicker.css" >
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel='stylesheet' type='text/css' href='/js/jquery-week-calendar-master/libs/css/smoothness/jquery-ui-1.8.11.custom.css' />
    <link rel='stylesheet' type='text/css' href='/js/jquery-week-calendar-master/jquery.weekcalendar.css' />

    <script type='text/javascript' src='/js/jquery-week-calendar-master/libs/jquery-1.4.4.min.js'></script>
    <script type='text/javascript' src='/js/jquery-week-calendar-master/libs/jquery-ui-1.8.11.custom.min.js'></script>
    <script type='text/javascript' src='/js/jquery-week-calendar-master/jquery.weekcalendar.js'></script>
    <script type="text/javascript" src="/js/calendar.js"></script>
    <script type="text/javascript" src="/js/jquery-week-calendar-master/libs/date.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=hr'></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-73261922-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>
<div class="wrapper" style="height:100%; display: flex">
    <div class="slider1">

        @include('partials._errors')

        <p class="hero-bold" style="margin-top:34%">Registriraj se</p>
        <div>
            {!! Form::open(['url' => '/register']) !!}
            {!! Form::label('family', '*Prezime obitelji', [ 'class' => "raleway-regular"]) !!}
            {!! Form::text('family', '', [ 'class' => "input-field-lg"]) !!}
            <div style="display:flex">
                <div>
                    {!! Form::label('email', '*Email',  [ 'class' => "raleway-regular"]) !!}
                    {!! Form::text('email', '', [ 'class' => "input-field"]) !!}
                </div>
                <div>
                    {!! Form::label('password', '*Zaporka', [ 'class' => "raleway-regular"] ) !!}
                    {!! Form::password('password', [ 'class' => "input-field"]) !!}
                </div>
            </div>
            <button type="submit" class="submit-button">POŠALJI</button>
            {!! Form::close() !!}
        </div>

        <p  style="margin-top:30%" class="hero-bold">Logiraj se</p>
        <div>
            {!! Form::open(['url' => '/login']) !!}
            <div style="display:flex">
                <div>
                    {!! Form::label('email', '*Email', [ 'class' => "raleway-regular"]) !!}
                    {!! Form::text('email', '', ['class' => 'input-field']) !!}
                </div>
                <div>
                    {!! Form::label('password', '*Zaporka', [ 'class' => "raleway-regular"])!!}
                    {!! Form::password('password', ['class' => 'input-field'])!!}
                </div>
            </div>
            <button type="submit" class="submit-button">LOGIRAJ SE</button>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="sidebar">
        <div class="header-icons">
            {{--<p class="login-register">sdsdf</p>--}}
            <img src="/images/ikone-01.png" alt="" class="sidebar-animate"/>
            <a href="/"><img src="/images/ikone-02.png" alt="" class="icon-margin" /></a>
            @if(Auth::check())
                <a href="/logout"><img src="/images/ikone-07.png" alt="" class="icon-margin"/></a>
            @else
                <img src="/images/ikone-03.png" alt="" class="login-register icon-margin"/>
            @endif

        </div>
        <table class="table table-responsive borderless font-white">
            <tbody>
            @if(Auth::check())
                @if(Auth::check())
                    <tr>
                        <td>
                            @if(Auth::user()->role == 1)
                                <a href="/user">Administracija</a>
                            @else
                                <p style="text-decoration: underline">Obitelj:</p>
                                <a href="/user">{{Auth::user()->full_name()}}</a>
                            @endif
                        </td>
                    </tr>
                @endif
                <tr>
                    <td></td>
                </tr>
            @endif
            <tr>
                <td>
                    <a href="/availability/availability">Trebam čuvalicu</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/kako-rezervirati-cuvalicu">Kako rezervirati čuvalicu </a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/cjenik">Cjenik</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/zelim-biti-cuvalica">Želim biti čuvalica</a>
                </td>
            </tr>
            <tr>
                <td>
                    Novosti i savjeti
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/opci-uvjeti">Opći uvjeti</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/onama">O nama</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/kontakt">Kontakt</a>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <a href="/logout">Odjavi se</a>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="footer-icon" id="footer-icon">
            <img src="/images/ikone-04.png" alt=""/>
        </div>
        <div class="footer-info">
            © 2015 Nanny.hr Disclaimer.
        </div>
    </div>
    <div class="container-main-calendar">
        <div class="footer-icon-info">
            <p class="footer-icon-info-text">Organiziramo i prijevoz djece</br>na izvanškolske aktivnosti</br>u sigurnim vozilima
                <br/>s dostupnim autosjedalicama!</p>
            <img src="/images/popup-01.png" alt=""/>
        </div>
        @yield('calendar_content')
    </div>
</div>
</body>
</html>