@extends('master')

@section('content')

    <div class="content-lower">

        <table class="table">
            <thead>
            <tr>
                <th>Korisnik</th>
                <th>Polazak</th>
                <th>Dolazak</th>
                <th>Udaljenost(km)</th>
                <th>Vrijeme</th>
                <th>Cijena(kn)</th>
                <th>Vrsta plaćanja</th>
                <th>Plaćeno</th>
                <th>Opcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transports as $transport)
                <tr>
                    <td>{{$transport->user->family->family_name}}</td>
                    <td>{{$transport->origin}}</td>
                    <td>{{$transport->destination}}</td>
                    <td>{{round($transport->distance / 1000, 2)}}</td>
                    <td>{{$transport->date}}</td>
                    <td>{{$transport->price}}</td>
                    <td>{{$transport->payment_type}}</td>
                    <td>{{$transport->status()}}</td>
                    <td>
                        @if($transport->payment_type == 'Direktno' && $transport->status == 0 && $transport->status != 2)
                            <a href="/admin/transport-direct-payment-success/{{$transport->id}}">
                                <button>Plaćeno</button>
                            </a>
                        @endif
                    </td>
                    <td>
                            <a href="/admin/transport-cancel-reservation/{{$transport->id}}">
                                <button>Otkaži rezervaciju</button>
                            </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $transports->render() !!}
    </div>
@endsection