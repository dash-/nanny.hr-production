@extends('master')

@section('content')
    <div class="content-lower">

            <div class="form-views">
            {!! Form::open()!!}
                <button type="submit">Završi</button>
                <div class="clearfix"></div>

                @foreach($covers as $key => $cover)
                <div style="margin-left:2%; margin-top:2%; padding:1%" class="col-md-5">
                    <img src="{{ '/images/covers/'.$cover->image }}" style="height:300px; width:400px" alt=""/>
                    <div style="text-align: center">
                        Slika {{$key}} {!! Form::checkbox('covers_chosen['.$cover->id.']', 1, $cover->in_use)!!}</div>
                    <label for="">Hrvatski:</label>
                    <textarea name="text_hr[{{$cover->id}}]" id="" cols="30" rows="4"
                              class="form-control tinymce-simple">{{$cover->text_hr}}</textarea>
                    <label for="">Engleski:</label>
                    <textarea name="text_en[{{$cover->id}}]" id="" cols="30" rows="4"
                              class="form-control tinymce-simple">{{$cover->text_en}}</textarea>
                    <a href="/admin/izbrisi-pozadinu/{{$cover->id}}"><button type="button">Izbriši</button></a>
                </div>
            @endforeach
                <div class="clearfix"></div>
            <button type="submit">Završi</button>
                {!! $covers->render() !!}
            {!! Form::close()!!}
        </div>
        </div>
@endsection