@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            <table class="table">
                @foreach($details as $key => $info)
                    @if($key == ' token')
                        <?php continue ?>
                    @endif
                    <tr>
                        <td>
                            <p>Rezervacija za period: {{\App\Helpers\GeneralHelper::displayLongDate($key)}}</p>

                            <div>
                                <p>Za djecu:</p>
                                <ul>
                                    @if(isset($info['info']['children']) != 0)
                                        @foreach($info['info']['children'] as $childKey => $child)
                                            <li>{{$child}}</li>
                                        @endforeach
                                    @endif

                                    @if($info['info']['other_children'] != '')
                                        <li> + {{$info['info']['other_children']}}</li>
                                    @endif
                                </ul>

                                @if(isset($info['info']['address_other']))
                                    <p>Adresa: {{$info['info']['address_other']}}</p>
                                @endif
                                @if(isset($info['info']['special_note']))
                                    <p>Posebne potrebe, zahtjevi i druge
                                        napomene:  {{$info['info']['special_note']}}</p>
                                @endif

                                <p>Čuvalica:</p>
                                <ul>
                                    @if(isset($info['chosen_nannies']))
                                        @foreach($info['chosen_nannies'] as $nanny)
                                            <li>{{$nanny->name.' '.$nanny->surname}}</li>
                                        @endforeach

                                    @else
                                        <li>Slobodni odabir čuvalice</li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </table>
            <b style="font-size: 20px">Cijena rezervacije:<b id="total-price">{{$total}}</b>kn</b>
            <p style="margin-top:5%">Doplata za period: Od {{$extended_hours_from}}h do {{$extended_hours_to}}h</p>
            <b style="font-size: 20px">Za nadoplatiti:<b id="total-price">{{$extended_price}}</b>kn</b>


            <p style="margin-top:2%; display:none">
                <a href="" data-toggle="modal" data-target="#myModal">{!! Form::label('terms', 'Prihvaćam opće uvjete Nanny.hr')!!}</a>
                {!! Form::checkbox('terms', 1, true, ['id' => 'terms'])!!}
            </p>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel" style="text-align: center; font-weight: bold">OPĆI UVJETI</h4>
                        </div>
                        <div class="modal-body">
                            <?= view ('_terms') ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
                        </div>
                    </div>
                </div>
            </div>

            {{--<form action="" method="post">--}}
            {!! Form::open(['url' => '/payment/payment'])!!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td>{!! Form::label('discount_token', 'Posjedujete promo code? Upisite ga ovdje')!!}
                        {!! Form::text('discount_token', '',['class'=> 'input-field-regular', 'id' => 'disc-token'])!!}</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="button-regular payment-button" id="paypal-button" disabled="disabled">Plati sa PayPal<img src="/images/paypal.png" alt=""/></button>
            <div>
                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>
            {!! Form::close()!!}

            <div style="margin-top:2%">
                <button class="pay-way button-regular payment-button" id="payway-button" disabled="disabled">Plati sa PayWay<img src="/images/payway.png" alt="" style="margin-left:5%"/></button>

                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>

            <div style="margin-top:2%">
                <button class="direct-payment button-regular payment-button" id="direct-payment" disabled="disabled">Uplati na račun</button>
            </div>
            {{--</form>--}}

        </div>
    </div>


@endsection