@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.emergency_contact')}}</h3>
                {!! Form::model($user) !!}
                <div class="form-group">
                    <div class="col-md-6">

                        <label for="emergency_contact_name"><span>*</span> {{trans('lang.name')}}</label>
                        {!! Form::text('emergency_contact_name', null,['class'=> 'form-control']) !!}
                    </div>
                    <div class="col-md-6">

                        <label for="emergency_contact_surname"><span>*</span> {{trans('lang.surname')}}</label>
                        {!! Form::text('emergency_contact_surname', null,['class'=> 'form-control']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="emergency_contact_phone"><span>*</span> {{trans('lang.phone')}}</label>
                        {!! Form::text('emergency_contact_phone', null,['class'=> 'form-control']) !!}

                    </div>
                    <div class="col-md-6">
                        <label for="emergency_contact_relation"><span>*</span> {{trans('lang.relation')}}</label>

                        {!! Form::text('emergency_contact_relation', null,['class'=> 'form-control']) !!}

                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.next_step')}}</button>
                </div>
                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="stripe-line" style="width: 80%">
                    <div class="step-number">4</div>
                </div>
            </div>
        </div>
    </div>
@endsection
