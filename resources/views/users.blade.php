@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            <table class="table">
                <thead>
                <tr>
                    <th>Obitelj</th>
                    <th>Email</th>
                    <th>Telefon</th>
                    <th>Adresa</th>
                    <th>Grad</th>
                    <th>Status registracije</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->family_name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->address}}</td>
                        <td>{{$user->city}}</td>
                        <td>{{($user->completed == 4) ? 'Završena' : 'Nezavršena'}}</td>
                        <td><a href="/admin/rezervacije-korisnika/{{$user->id}}"><button>Rezervacije</button></a></td>
                        <td><a href="/admin/profil-korisnika/{{$user->id}}"><button>Profil korisnika</button></a></td>
                        <td><a href="/admin/obrisi-korisnika/{{$user->id}}"><button>Obriši korisnika</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $users->render() !!}
        </div>
@endsection