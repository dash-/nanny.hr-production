@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">

            <p class="hero-regular" style="font-size:20px; color:black">Plaćanje nije
                uspjelo {{session('error') ? ': '.session('error') : ""}}</p>

        <a href="/user"><button class="button-regular">Nazad na profil</button></a>
        </div>
    </div>


@endsection