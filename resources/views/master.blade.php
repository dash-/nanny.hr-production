<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="description"
          content="Nanny j.d.o.o. je specijalizirana agencija za posredovanje pri pružanju usluga povremenog čuvanja djece koja djeluje na prostoru Zagreba i okolice."/>
    <title>{{ isset($page_title) ? $page_title." | " : "" }} Nanny.hr </title>
    <meta property="og:title"
          content="{{ isset($page_title) ? $page_title.' | ' : '' }} Nanny.hr">
    <meta name="twitter:title"
          content="{{ isset($page_title) ? $page_title.' | ' : '' }} Nanny.hr">
    <meta property="og:type" content="website">
    <link href='https://fonts.googleapis.com/css?family=Comfortaa&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link rel="shortcut icon" href="/images/favicon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/js/datetimepicker-master/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css?v=2.01">
    <meta title="keywords"
          content="dadilja, cuvalica, čuvalica, babysitter, čuvanje djece, nanny zagreb, trebam čuvalicu, bejbisiterica, pomoć oko djeteta, prijevoz djeteta, babysitters, bejbisiterica zagreb, caregiver"/>
    @if(isset($page_description) && $page_description)
        <meta name="description" content="{{$page_description}}">
        <meta property="og:description" content="{{ $page_description }}">
        <meta name="twitter:description" content="{{ $page_description }}">
    @else
        <meta name="description"
              content="Želim biti čuvalica: strast čuvanje djece, dodatno zaraditi, odgovorni strpljivi za rad s djecom">
        <meta property="og:description"
              content="Želim biti čuvalica: strast čuvanje djece, dodatno zaraditi, odgovorni strpljivi za rad s djecom">
        <meta name="twitter:description"
              content="Želim biti čuvalica: strast čuvanje djece, dodatno zaraditi, odgovorni strpljivi za rad s djecom">
    @endif
    <script>
        function MM_openBrWindow(theURL, winName, features) { //v2.0
            window.open(theURL, winName, features);
        }

    </script>
</head>
<body>
<div class="wrapper" style="">
    @include('partials._errors')
    <div class="header">
        <div class="content">
            <div class="logo-img">
                @if(isset($navigation_black) && $navigation_black)
                    <a href="/"><img src="/images/NannyLogoBlack.png" alt="Nanny"/></a>
                @else
                    <a href="/"><img src="/images/NannyLogo.png" alt="Nanny"/></a>
                @endif
            </div>
            <div class="navigation  {{ isset($navigation_black) && $navigation_black ? "black" : "" }}">

                <div class="mobile-navigation-holder">
                    <div class="icon-bars">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>

                    <ul class="mobile-navigation" style="display: none;">
                        @if(Auth::check())
                            @if(Auth::user()->role == 1)
                                <li><a href="/availability">{{trans('lang.need_a_nanny')}}</a></li>
                                <li><a href="/transport">{{trans('lang.order_transport')}}</a></li>
                            @else
                                <li><a href="/availability/availability">{{trans('lang.need_a_nanny')}}</a></li>
                                <li><a href="/transport/transport">{{trans('lang.order_transport')}}</a></li>
                            @endif
                        @else
                            <li><a href="/availability/availability">{{trans('lang.need_a_nanny')}}</a></li>
                            <li><a href="/transport/transport">{{trans('lang.order_transport')}}</a></li>
                        @endif
                        <li><a href="/zelim-biti-cuvalica">{{trans('lang.i_want_to_be_nanny')}}</a></li>
                        <li><a href="/cjenik">{{trans('lang.pricing')}}</a></li>
                        <li><a href="/novosti">{{trans('lang.news')}}</a></li>
                        <li><a href="/onama"> {{trans('lang.about')}}</a></li>
                        <li><a href="/kontakt"> {{trans('lang.contact')}}</a></li>
                        @if(Auth::check())
                            <li class="login logout">
                                <a href="/user">{{ strtoupper(Auth::user()->full_name()) }}</a>
                                <a href="/logout" class="logout"><img src="/images/logout.png" alt="Logout"
                                                                      title="Logout"
                                                                      class="icon-margin"/></a>
                            </li>
                        @else
                            <li class="login"><a href="/login"> {{trans('lang.sign_in')}}</a></li>
                        @endif
                    </ul>
                </div>
                <ul class="desktop-navigation">
                    @if(Auth::check())
                        @if(Auth::user()->role == 1)
                            <li><a href="/availability">{{trans('lang.need_a_nanny')}}</a></li>
                            <li><a href="/transport">{{trans('lang.order_transport')}}</a></li>
                            @else
                            <li><a href="/availability/availability">{{trans('lang.need_a_nanny')}}</a></li>
                            <li><a href="/transport/transport">{{trans('lang.order_transport')}}</a></li>
                        @endif
                        @else
                        <li><a href="/availability/availability">{{trans('lang.need_a_nanny')}}</a></li>
                        <li><a href="/transport/transport">{{trans('lang.order_transport')}}</a></li>
                    @endif
                    <li><a href="/zelim-biti-cuvalica">{{trans('lang.i_want_to_be_nanny')}}</a></li>
                    <li><a href="/cjenik">{{trans('lang.pricing')}}</a></li>
                    <li><a href="/novosti">{{trans('lang.news')}}</a></li>
                    <li><a href="/onama"> {{trans('lang.about')}}</a></li>
                    <li><a href="/kontakt"> {{trans('lang.contact')}}</a></li>

                    @if(Auth::check())
                        <li class="login logout">
                            @if(Auth::user()->role == 1)
                                <a href="/user">ADMIN</a>
                            @else
                                <a href="/user">{{ strtoupper(Auth::user()->full_name()) }}</a>
                            @endif
                            <a href="/logout" class=""><img src="/images/logout.png" alt="Logout" title="Logout"
                                                            class="icon-margin"/></a>
                        </li>
                    @else
                        <li class="login"><a href="/login"> {{trans('lang.sign_in')}}</a></li>
                    @endif
                </ul>
            </div>
            <div style="position:absolute; top:0; right:0;display:flex">
                <a style="margin-right:3px;text-decoration: none;" href="{{ \Illuminate\Support\Facades\Request::url() }}?jezik=hr"><div class="lang-icon">
                        <img width="30px" src="/images/croatia_640.png" alt=""/></div></a>
                <a style="text-decoration: none" href="{{ \Illuminate\Support\Facades\Request::url() }}?jezik=en"><div class="lang-icon">
                        <img width="30px" src="/images/usa_640.png" alt=""/></div></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    @yield('content2')
    @yield('slider')
    @yield('content')
    <div class="clearfix"></div>
    <div class="footer   {{ isset($navigation_black) && $navigation_black ? "black" : "" }}">
        <div class="content">
            <div class="col-md-3"><a
                        target="_blank"
                        href="https://www.facebook.com/Nanny-1700921193485926/"><strong>FACEBOOK</strong></a>
                <br>
                <br>

                <a href="https://play.google.com/store/apps/details?id=nanny.hr.nanny" target="_blank">
                    <img src="/images/google_play.png" alt="Google Play" style="width: 50%;">
                </a>
            </div>
            <div class="col-md-6">
                {!! Form::open(['url' => 'home/quick-contact']) !!}
                {!! Form::hidden('footer', 1) !!}
                {!! Form::label('name', '*'. trans('lang.name_surname')) !!} <br>
                {!! Form::text('name', null, ['class' => 'input-transparent', 'required' => 'required']) !!} <br>
                {!! Form::label('email', '*'.trans('lang.email')) !!}<br>
                {!! Form::email('email', null, ['class' => 'input-transparent', 'required' => 'required']) !!}<br>
                {!! Form::label('phone', '*'.trans('lang.phone')) !!}<br>
                {!! Form::text('phone', null, ['class' => 'input-transparent', 'required' => 'required']) !!}<br>
                {!! Form::label('message', '*'.trans('lang.message')) !!}<br>
                {!! Form::textarea('message', null, ['class' => 'input-transparent', 'rows' => 6, 'required' => 'required']) !!}
                <br>
                <div class="text-center">
                    <br>
                    <button class="btn contact-button">{{trans('lang.send_a_question')}}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-md-3 text-right"><a
                        target="_blank" href="https://www.instagram.com/nanny.hr/"><strong>INSTAGRAM</strong></a></div>
            <div class="clearfix"></div>
            <div class="foot">
                <div class="col-md-4 text-left">
                    Design <a href="http://lisicalisica.com">Lisica</a> | Development <a href="http://dash.ba">DASH-</a>
                </div>
                <div class="col-md-4 text-center">*{{trans('lang.all_fields_required')}}</div>


                <div class="col-md-4 text-right">© {{ date('Y', time()) }} Nanny.hr <strong><a href="/opci-uvjeti"> {{trans('lang.terms_of_use')}}</a></strong></div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="col-md-12 text-center"><a
                    target="_blank"
                    href="https://www.facebook.com/Nanny-1700921193485926/"><strong>FACEBOOK</strong></a>
            <br>
            <a href="https://play.google.com/store/apps/details?id=nanny.hr.nanny" target="_blank">
                <img src="/images/google_play.png" alt="Google Play" style="width: 50%;">
            </a>
        </div>
        <div class="col-md-12 text-center"><a target="_blank" href="https://www.instagram.com/nanny.hr/"><strong>INSTAGRAM</strong></a>
        </div>
        <br>
        <div class="col-md-12 text-center">
            © {{ date('Y', time()) }} Nanny.hr <strong><a href="/opci-uvjeti"> {{trans('lang.terms_of_use')}}</a></strong></div>
        <div class="col-md-12 text-center">
            Design <a href="http://lisicalisica.com">Lisica</a> | Development <a href="http://dash.ba">DASH-</a>
        </div>
    </div>
</div>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-73261922-1', 'auto');
    ga('send', 'pageview');

</script>
<script src='https://www.google.com/recaptcha/api.js?hl=hr'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="/js/datetimepicker-master/jquery.js"></script>
<script src="/js/datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="/js/tinymce/tinymce.min.js"></script>
<script src="/js/tinymce/main.js"></script>
<script src="/js/main.js?v=2.01"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&sensor=false&language=hr"></script>
<script src="/js/transport.js"></script>
<script src="/js/orderTransport.js"></script>
</body>
</html>