@extends('master')

@section('content')

    <div class="content-lower">
        <div class="form-views">
            {!! Form::open() !!}

            <table class="table borderless">
                <tbody>
                <tr>
                    <td>{!! Form::label('name', trans('lang.name'))!!}
                        {!! Form::text('name', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('surname', trans('lang.surname'))!!}
                        {!! Form::text('surname', '', ['class'=> 'input-field-regular'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('dob', trans('lang.dob')) !!}
                        {!! Form::text('dob', '', ['class'=> 'input-field-regular', 'id' => 'datetimepicker'])!!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('sex', trans('lang.gender')) !!}
                        <span>{{trans('lang.male')}}</span>
                        {!! Form::radio('sex', 'Muško', true) !!}
                        <span>{{trans('lang.female')}}</span>
                        {!! Form::radio('sex', 'Žensko', false) !!}</td>
                </tr>

                <tr>
                    <td> {!! Form::label('allergies', trans('lang.allergies'))!!}
                        {!! Form::checkbox('allergies', 1, false)!!}</td>
                </tr>

                <tr>
                    <td>{!! Form::label('special_note', trans('lang.special_note'))!!}
                        {!! Form::text('special_note', '', ['class'=> 'input-field-regular']) !!}</td>
                </tr>

                </tbody>
            </table>
            <button type="submit" class="button-regular">{{trans('lang.save_changes')}}</button>

            {!! Form::close() !!}
        </div>
    </div>
@endsection