@extends('master')

@section('content')
    <div class="content-lower">
        <div>
        {!! Form::open() !!}

            {!! Form::label('package_name', 'Paket')!!}
            {!! Form::text('package_name', '', ['class'=> 'input-field-regular'])!!}

            {!! Form::label('usage', 'Broj korištenja')!!}
            {!! Form::text('usage', '', ['class'=> 'input-field-regular'])!!}

            {!! Form::label('price', 'Cijena')!!}
            {!! Form::text('price', '', ['class'=> 'input-field-regular'])!!}
            <button type="submit">Spasi</button>

        {!! Form::close()!!}
        </div>
        <div style="margin-top:5%" >
        </div><a href="/admin/uredi-pakete"><button>Nazad</button></a>
        </div>

    </div>
@endsection