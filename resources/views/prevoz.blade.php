@extends('master')

@section('content')

    <div class="content-lower">

        <table class="table">
            <thead>
            <tr>
                <th>Polazak</th>
                <th>Dolazak</th>
                <th>Udaljenost(km)</th>
                <th>Vrijeme</th>
                <th>Cijena(kn)</th>
                <th>Vrsta plaćanja</th>
                <th>Plaćeno</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($transports as $transport)
                <tr>
                    <td>{{$transport->origin}}</td>
                    <td>{{$transport->destination}}</td>
                    <td>{{round($transport->distance / 1000, 2)}}</td>
                    <td>{{$transport->date}}</td>
                    <td>{{$transport->price}}</td>
                    <td>{{$transport->payment_type}}</td>
                    <td>{{$transport->status()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $transports->render() !!}
    </div>
@endsection