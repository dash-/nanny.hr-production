@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            <span>Dodaj pozadinu</span>
            <p><b>Optimalna velicina pozadine za najbolji izgled bi trebala biti minimalno 1920 x 1080</b></p>

            {!! Form::open(['role' => 'form', 'files'=>'true']) !!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td>
                        <label for="slika">Izaberi sliku:</label>
                        {!! Form::file('pozadina') !!}</td>
                </tr>
                </tbody>
            </table>

            <label for="">Tekst - Hrvatski:</label>
            <textarea name="text_hr" cols="30" rows="4" class="form-control tinymce-simple"></textarea>
            <label for="">Tekst - Engleski:</label>
            <textarea name="text_en" cols="30" rows="4" class="form-control tinymce-simple"></textarea>
            <button type="submit" style="margin-right:5%" class="button-regular">Upload sliku</button>
            {!! Form::close() !!}

            <a href="/user">
                <button class="button-regular">Nazad</button>
            </a>
        </div>
    </div>
@endsection