@extends('master')

@section('content')

    <div class="content-lower">

        <div style="font-size:17px; font-weight: bold">
            <span>{{ trans('lang.direktna_uplata_1') }}</span><br/>
            <br/>
            <span>{{ trans('lang.direktna_uplata_2') }}</span><br/>
            <span>{{ trans('lang.direktna_uplata_3') }}</span><br/>
            <span>{{ trans('lang.direktna_uplata_4') }}</span><br/>
            <span>{{ trans('lang.direktna_uplata_5') }}</span><br/>
            <span>{{ trans('lang.direktna_uplata_6') }}</span><br/>
            <br/>
            <span>{{ trans('lang.direktna_uplata_7') }}</span><br/>
            <span>{{ trans('lang.direktna_uplata_8') }}</span><br/>
        </div>
        <p style="margin-top:5%"><a href="{{ $url  }}">
                <button class="button-regular">{{ trans('lang.confirm_reservation') }}</button>
            </a></p>
    </div>
@endsection