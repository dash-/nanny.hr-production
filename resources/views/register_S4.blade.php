@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.child')}}</h3>
                {!! Form::model($child) !!}
                <div class="form-group">
                    <div class="col-md-6">

                        <label for="name"><span>*</span> {{trans('lang.name')}}</label>
                        {!! Form::text('name', null,['class'=> 'form-control']) !!}
                    </div>
                    <div class="col-md-6">

                        <label for="prezime"><span>*</span> {{trans('lang.surname')}}</label>
                        {!! Form::text('surname', null,['class'=> 'form-control']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="dob"><span>*</span> {{trans('lang.dob')}}</label>
                        {!! Form::text('dob', null, ['class'=> 'form-control','id' => 'dob']) !!}
                    </div>
                    <div class="col-md-6">
                        <label for="sex"><span>*</span> {{trans('lang.gender')}}</label>
                        <div class="clearfix"></div>
                        <div class="radio-fields">
                            <div class="izbor">{{trans('lang.male')}}
                                {!! Form::radio('sex', 'Muško', true) !!}
                            </div>
                            <div class="izbor">{{trans('lang.female')}}
                                {!! Form::radio('sex', 'Žensko', false) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-6">
                        <label for="allergies"><span>*</span> {{trans('lang.allergies')}}?</label>
                        {!! Form::text('allergies_info', null,['placeholder' => 'Određena hrana, piće? ','class'=> 'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        <label for="special_note">{{trans('lang.special_note')}}</label>
                        {!! Form::text('special_note', null,['class'=> 'form-control']) !!}
                    </div>
                </div>


                <div class="clearfix"></div>
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.next_step')}}</button>
                </div>
                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="stripe-line" style="width: 100%">
                    <div class="step-number">5</div>
                </div>
            </div>
        </div>
    </div>
@endsection
