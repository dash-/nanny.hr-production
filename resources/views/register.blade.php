@extends('master')

@section('slider')
    @include('partials._slider', ['dark' => true])
@endsection

@section('content2')
    <div class="content-over">
        <div class="content">
            <div class="overlay-form">
                <h3>{{trans('lang.address')}}</h3>
                {!! Form::model($user) !!}
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="address"><span>*</span> {{trans('lang.address')}}</label>

                        {!! Form::text('address', null,['class'=> 'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        <label for="address_number"><span>*</span> {{trans('lang.address_number')}}</label>
                        {!! Form::text('address_number', null,['class'=> 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        <label for="city"><span>*</span> {{trans('lang.city')}}</label>

                        {!! Form::text('city', null,['class'=> 'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        <label for="state"><span>*</span> {{trans('lang.state')}}</label>
                        {!! Form::text('state', null,['class'=> 'form-control']) !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6">
                        <label for="phone"><span>*</span> {{trans('lang.phone')}}</label>
                        {!! Form::text('phone', null,['class'=> 'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        <label for="zip_code"><span>*</span> {{trans('lang.zip_code')}}</label>
                        {!! Form::text('zip_code', null,['class'=> 'form-control']) !!}
                    </div>

                </div>


                <div class="form-group">
                    <div class="col-md-6">
                        <label for="pets"><span>*</span> {{trans('lang.own_a_pet')}}?</label>
                        <div class="radio-fields">
                            <div class="izbor">{{trans('lang.yes')}}
                                {!! Form::radio('pets', 1, false, ['class' => 'yes-pets']) !!}
                            </div>
                            <div class="izbor">{{trans('lang.no')}}
                                {!! Form::radio('pets', 0, true, ['class' => 'no-pets']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="pets_info"><span>*</span>{{trans('lang.which_pet')}}?</label>

                        {!! Form::text('pets_info', null,['class'=> 'form-control']) !!}
                    </div>
                </div>


                <div class="clearfix"></div>

                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button style="text-transform: uppercase" type="submit" class="btn btn-submit">{{trans('lang.next')}}</button>
                </div>
                <div class="col-md-3"></div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
                <div class="stripe-line" style="width: 40%">
                    <div class="step-number">2</div>
                </div>
            </div>
        </div>
    </div>
@endsection