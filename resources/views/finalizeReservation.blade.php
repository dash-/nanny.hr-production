@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            <table class="table">
                @foreach($details as $key => $info)
                    @if($key == ' token')
                        <?php continue ?>
                    @endif
                    <tr>
                        <td>
                            <p>{{trans('lang.for_period')}}: {{\App\Helpers\GeneralHelper::displayLongDate($key)}}</p>

                            <div>
                                <p>{{trans('lang.for_children')}}:</p>
                                <ul>
                                    @if(isset($info['info']['children']) != 0)
                                        @foreach($info['info']['children'] as $childKey => $child)
                                            <li>{{$child}}</li>
                                        @endforeach
                                    @endif

                                    @if($info['info']['other_children'] != '')
                                        <li> + {{$info['info']['other_children']}}</li>
                                    @endif
                                </ul>

                                @if(isset($info['info']['address_other']))
                                    <p> {{trans('lang.address')}}: {{$info['info']['address_other']}}</p>
                                @endif
                                @if(isset($info['info']['special_note']))
                                    <p>{{trans('lang.special_reservation_notes')}}
                                        : {{$info['info']['special_note']}}</p>
                                @endif

                                <p>{{trans('lang.nanny')}}:</p>
                                <ul>
                                    @if(isset($info['chosen_nannies']))
                                        @foreach($info['chosen_nannies'] as $nanny)
                                            <li>{{$nanny->name.' '.$nanny->surname}}</li>
                                        @endforeach

                                    @else
                                        {{--<li>Slobodni odabir čuvalice</li>--}}
                                    @endif
                                </ul>
                                @if(isset($info['price']))
                                    <b style="font-size: 20px"><p>{{trans('lang.price')}}: {{$info['price']}}kn</p></b>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach

            </table>
            @if($transport != 0)
                <b><p style="font-size: 20px; margin-bottom: 2%">{{trans('lang.transport')}}: {{$transport}}</p></b>
            @endif
            <div style="width:100%;height: 1px; background-color: black"></div>
            <b style="font-size: 20px">{{trans('lang.total')}}:<b id="total-price">{{$total}}</b>kn</b>

            <p style="margin-top:2%; display: none;">
                <a href="" data-toggle="modal"
                   data-target="#myModal">{!! Form::label('terms', trans('lang.accept_terms'))!!}</a>
                {!! Form::checkbox('terms', 1, true, ['id' => 'terms'])!!}
            </p>

            {{--<form action="" method="post">--}}
            {!! Form::open(['url' => '/payment/payment'])!!}
            <table class="table borderless">
                <tbody>
                <tr>
                    <td>{!! Form::label('discount_token', trans('lang.promo_code'))!!}
                        {!! Form::text('discount_token', '',['class'=> 'input-field-regular', 'id' => 'disc-token'])!!}</td>
                </tr>
                @if($monitor_availability == true)
                    <tr>
                        <td>{!! Form::label('camera', trans('lang.baby_monitor').'?') !!}
                            {!! Form::checkbox('camera', 1, false, ['id' => 'camera']) !!}
                            <p> *{{trans('lang.damaged_baby_monitor_info')}}. </p>
                        </td>
                    </tr>
                @endif
                @if(Auth::user()->role == 1)
                    <tr>
                        <td>{!! Form::label('custom_price', 'Administratorska cijena:')!!}
                            {!! Form::text('custom_price', '',['class'=> 'input-field-regular', 'id' => 'custom-price'])!!}
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
            <button type="submit" class="button-regular payment-button" id="paypal-button"
                    disabled="disabled">{{trans('lang.pay_with_paypal')}}<img src="/images/paypal.png" alt=""/></button>
            <div>
                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>
            {!! Form::close()!!}

            <div style="margin-top:2%">
                <button class="pay-way button-regular payment-button" id="payway-button"
                        disabled="disabled">{{trans('lang.pay_with_payway')}}<img src="/images/payway.png" alt=""
                                                                                  style="margin-left:5%"/></button>

                <img src="/images/AMEX.png" alt=""/>
                <img src="/images/diners.png" alt=""/>
                <img src="/images/MASTERCARD.png" alt=""/>
                <img src="/images/maestro.png" alt=""/>
                <img src="/images/VISA_DEBIT.png" alt=""/>
                <img src="/images/VISA.png" alt=""/>
            </div>

            <div style="margin-top:2%">
                <button class="direct-payment button-regular payment-button" id="direct-payment"
                        disabled="disabled">{{trans('lang.direct_payment')}}</button>
            </div>
            {{--</form>--}}

        </div>
    </div>


@endsection