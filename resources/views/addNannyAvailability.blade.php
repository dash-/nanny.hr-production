@extends('master')

@section('content')

    <div class="content-lower">

        <div class="form-views">
            {!! Form::model($availability) !!}

            {!! Form::label('from', 'Od', ['class' => "raleway-regular"]) !!}
            {!! Form::text('from', null, ['id' => 'datetimepicker', 'class'=> 'form-control']) !!}

            {!! Form::label('to', 'Do', ['class' => "raleway-regular"]) !!}
            {!! Form::text('to', null, ['id' => 'datetimepicker2', 'class'=> 'form-control']) !!}

            <button class="button-regular" style="float:left; margin-top:8%; margin-right:5%" type="submit">Spremi
                dostupnost
            </button>

            {!! Form::close() !!}
            <a href="/nannies/dostupnost-dadilje/{{$id}}">
                <button class="button-regular" style="margin-top:5%;">Nazad</button>
            </a>
        </div>
    </div>
@endsection