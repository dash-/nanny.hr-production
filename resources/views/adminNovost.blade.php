@extends('master')

@section('content')
    <div class="content-lower">
        <h3>Vijest - izmjena</h3>
        <div class="">
            {!! Form::model($novost, ['files' => 'true'])!!}

            {!! Form::label('title_hr', 'Naslov - Hrvatski')!!} <br>
            {!! Form::text('title_hr', null, ['class'=> 'form-control'])!!}
            <br>
            {!! Form::label('title_en', 'Naslov - Engleski')!!} <br>
            {!! Form::text('title_en', null, ['class'=> 'form-control'])!!}
            <br>
            {!! Form::label('image', 'Slika')!!}
            {!! Form::file('image', null)!!}
            @if($novost && $novost->image)
                <img src="/images/novosti/{{ $novost->image }}" alt="">
            @endif
            <br>
            {!! Form::label('summary_hr', 'Ukratko - Hrvatski')!!}
            {!! Form::textarea('summary_hr', null, ['class' => 'form-control', 'maxlength'=> 255, 'rows' => 3])!!}
            <br>
            {!! Form::label('summary_en', 'Ukratko - Engleski')!!}
            {!! Form::textarea('summary_en', null, ['class' => 'form-control', 'maxlength'=> 255, 'rows' => 3])!!}
            <br>
            {!! Form::label('content_hr', 'Tekst - Hrvatski')!!}
            {!! Form::textarea('content_hr', null, ['class'=> 'tinymce'])!!}
            <br>
            {!! Form::label('content_en', 'Tekst - Engleski')!!}
            {!! Form::textarea('content_en', null, ['class'=> 'tinymce'])!!}
            <br>

            <button>Spremi</button>

            {!! Form::close()!!}
        </div>
    </div>
@endsection