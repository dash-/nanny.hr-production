@extends('master')

@section('content')
    <div class="content-lower">
        <div style="text-align: center; padding: 5%">

            <p>{{trans('lang.if_transport')}}</p>
            <input type="text" id="transport-from" placeholder="{{trans('lang.departure')}}">
            <input type="text" id="transport-to" placeholder="{{trans('lang.destination')}}"><br>

                <button style="float:none; margin-top: 3%" class="button-regular" id="calculateDistance">{{trans('lang.proceed')}}</button>
                <a href="/payment">
                    <button style="float:none;margin-top: 3%;margin-left:2%" class="button-regular">{{trans('lang.skip')}}</button>
                </a>
        </div>
    </div>
@endsection

