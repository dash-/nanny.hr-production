<div class="inner-container">
    <?php $live_cover = App\Helpers\GeneralHelper::liveCover() ?>
    <div class="{{ isset($dark) && $dark ? "overlay-top-full" :"overlay-top" }}"></div>
    <ol class="carousel-indicators">
        @foreach($live_cover as $key => $cover)
            <li data-target="#carousel-example-generic" data-slide-to="{{ $key }}"
                class="{{ $key == 0 ? "active" : "" }}"></li>
        @endforeach

    </ol>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($live_cover as $key => $cover)
                <div class="item {{($key == 0 ? 'active' : '')}}">
                    {{--style="height:100%;background-image: url('/images/covers/{{ $cover->image }}'); background-size: cover;">--}}

                    @if($cover->text())
                        <div class="carousel-caption">
                            {!! nl2br($cover->text()) !!}
                        </div>
                    @endif
                    <img src="{{'/images/covers/'. $cover->image }} " alt="">

                </div>
            @endforeach
        </div>
    </div>
</div>
