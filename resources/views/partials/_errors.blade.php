@if(isset($errors) && count($errors) >0)
    <div class="alert alert-danger">
        <ul>
            @if(is_array($errors))
                @foreach ($errors as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @elseif(is_string($errors))
                {{ $errors }}
            @else
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endif
            <li>
                <button style ="text-transform: uppercase"type="type" class="btn btn-nanny invert btn-zatvoriti">{{trans('lang.close')}}</button>
            </li>
        </ul>
    </div>
@endif
@if(session('message'))

    <div class="flash-message">
        <div class="alert alert-danger">

            {{session('message')}}

        </div>
    </div>
@endif
