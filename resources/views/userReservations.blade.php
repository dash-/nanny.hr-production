@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
            {{--<div style="width:70%; color:black; margin-left:10%" class="hero-regular">--}}
            <table class="table" style="margin-bottom:5%">
                <thead>
                <tr>
                    <th>{{trans('lang.reservation_made')}}</th>
                    <th>{{trans('lang.total')}}</th>
                    <th>{{trans('lang.payed')}}</th>
                    <th>{{trans('lang.options')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($reservations as $reservation)
                    <tr>
                        <td>{{\App\Helpers\GeneralHelper::displayDate($reservation->created_at)}}</td>
                        <td>{{$reservation->total_price}}</td>
                        <td>{{$reservation->status()}}</td>
                        <td><a href="/user/rezervacija/{{$reservation->id}}"><button>{{trans('lang.details')}}</button></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $reservations->render() !!}
        </div>
    </div>
@endsection