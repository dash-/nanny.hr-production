@extends('master')

@section('content')
    <div class="content-lower">

        <div class="form-views">
    <table class="table">
        <thead>
            <tr>
                <th>Od</th>
                <th>Do</th>
                <th>Dan u sedmici</th>
                <th>Ukupan broj sati</th>
                <th>Dostupna?</th>
                <th>Iskoristeno sati</th>
                <th colspan="3"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($availability as $availabile)
                <tr>
                    <td>{{$availabile->from}}</td>
                    <td>{{$availabile->to}}</td>
                    <td>{{$availabile->day_of_week}}</td>
                    <td>{{$availabile->number_of_hours}}</td>
                    <td>{{($availabile->isAvailable) ? 'Da' : 'Ne'}}</td>
                    <td>{{$availabile->used_hours}}</td>
                    <td><a href="/nannies/obrisi-dostupnost/{{$availabile->id}}"><button>Obriši</button></a></td>
                    <td><a href="/nannies/izmijeni-dostupnost/{{$availabile->id}}"><button>Izmjeni dostupnost</button></a></td>
                    <td><a href="/nannies/izmijeni-informacije-dostupnosti/{{$availabile->id}}">
                            <button>Izmjeni informacije dostupnosti</button>
                        </a></td>
                </tr>
            @endforeach
        </tbody>

    </table>

            {!! $availability->render() !!}

    <a href="/nannies/dodaj-dostupnost-dadilji/{{$id}}"><button class="button-regular" style="margin-right: 5%">Dodaj dostupnost</button></a>
        <a href="/nannies"><button class="button-regular">Nazad</button></a>
        </div>
    </div>
@endsection