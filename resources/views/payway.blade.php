@extends('master')

@section('content')
    <div class="content-lower">
        <form id="payway-authorize-form" name="payway-authorize-form" method="post"
              action="{{env('PAYWAY_ACTION', 'https://pgw.ht.hr/services/payment/api/authorize-form')}}">
            <input type="hidden" name="pgw_shop_id" value="{{$payway->pgw_shop_id}}"/>
            <input type="hidden" name="pgw_order_id" value="{{$payway->pgw_order_id}}"/>
            <input type="hidden" name="pgw_amount" value="{{$payway->pgw_amount}}"/>
            <input type="hidden" name="pgw_authorization_type" value="{{$payway->pgw_authorization_type}}"/>
            <input type="hidden" name="pgw_language" value="{{$payway->pgw_language}}"/>
            <input type="hidden" name="pgw_return_method" value="{{$payway->pgw_return_method}}"/>
            <input type="hidden" name="pgw_success_url" value="{{$payway->pgw_success_url}}"/>
            <input type="hidden" name="pgw_failure_url" value="{{$payway->pgw_failure_url}}"/>
            <input type="hidden" name="pgw_order_info" value="{{$payway->pgw_order_info}}"/>
            <input type="hidden" name="pgw_signature"
                   value="{{$hash}}"/>
        </form>
        <script>
            document.forms["payway-authorize-form"].submit();
        </script>
    </div>
@endsection