
<div style="width:100%;height:170px;text-align: center;
background: #ed7caf;
background: -moz-linear-gradient(45deg, #ed7caf 0%, #53cbf1 69%, #53cbf1 69%, #2ba9e0 100%);
background: -webkit-linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
background: linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed7caf', endColorstr='#2ba9e0',GradientType=1 );">
    <img style="padding:50px" width="200px" src="{{ $message->embed('css/logo transparent-01.png') }}" alt=""/>
</div>

<div style="text-align: left; padding:3%; font-family: sans-serif">
<p>Zahvaljujemo na rezervaciji! O potvrdi Vaše rezervacije obavijestit ćemo vas u

    najkraćem mogućem roku putem e-maila/SMS-a.</p>

<p>Vasi podaci:</p>

<table class="table">
    @foreach($data as $key => $info)
        @if($key == ' token')
            <?php continue ?>
        @endif
        <tr>
            <td>
                <p>Za period: {{$key}}</p>
                <div>
                    <p>Za djecu:</p>
                    <ul>
                        @if(isset($info['info']['children']) != 0)
                            @foreach($info['info']['children'] as $childKey => $child)
                                <li>{{$child}}</li>
                            @endforeach
                        @endif

                        @if(isset($info['info']['other_children']))
                            <li> + {{$info['info']['other_children']}}</li>
                        @endif
                    </ul>

                    @if(isset($info['info']['address_other']))
                        <p>Adresa: {{$info['info']['address_other']}}</p>
                    @endif
                    @if(isset($info['info']['special_note']))
                        <p>Posebne potrebe, zahtjevi i druge
                            napomene:  {{$info['info']['special_note']}}</p>
                    @endif

                    <p>Dadilje:</p>
                    <ul>
                        @if(isset($info['chosen_nannies']))
                            @foreach($info['chosen_nannies'] as $nanny)
                                <li>{{$nanny['name'].' '.$nanny['surname']}}</li>
                            @endforeach
                        @else
                            <li><li>Slobodni odabir čuvalice</li></li>
                        @endif
                    </ul>
                    @if(isset($info['price']))
                        <p>Cijena: {{$info['price']}}</p>
                    @endif
                </div>
            </td>
        </tr>

    @endforeach
</table>
<b style="font-size: 20px">Ukupno: {{$total}}{{$currency}}</b>

</div>

<div style="text-align:center;width:100%;height:170px;-webkit-background-size: cover;background: #F5F5F5;background-size: cover;">
    <div style="padding-top:2%">
        <p>Follow:</p><br/>
        <span style="margin-right:5%"><a style="text-decoration: none; font-weight:bold; color:black" href="https://www.facebook.com/Nanny-1700921193485926/">FACEBOOK</a></span><span><a style="text-decoration: none;font-weight:bold; color:black" href="https://www.instagram.com/nanny.hr/">INSTAGRAM</a></span>
    </div>
</div>