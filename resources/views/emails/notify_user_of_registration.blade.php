<div style="width:100%;height:160px;text-align: center;
background: #ed7caf;
background: -moz-linear-gradient(45deg, #ed7caf 0%, #53cbf1 69%, #53cbf1 69%, #2ba9e0 100%);
background: -webkit-linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
background: linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed7caf', endColorstr='#2ba9e0',GradientType=1 );">
    <img style="padding:50px" width="200px" src="{{ $message->embed('css/logo transparent-01.png') }}" alt=""/>
</div>

<div style="text-align: left; padding:3%;">
<p>Poštovanje,<br/><br/>

    hvala Vam što ste odabrali Nanny.hr za uslugu čuvanja djece. Naša misija je da Vam ponudimo pouzdane i provjerene čuvalice koje će najbolje odgovoriti na Vaše zahtjeve i potrebe.
    <br/><br/>

    Uvijek nas možete kontaktirati na email info@nanny.hr ili +385953761155 za sva pitanja i nedoumice, kao i za termine koji možda nisu dostupni preko web stranice. Mi ćemo se uvijek potruditi da najbolje odgovorimo na Vaš zahtjev.
    <br/><br/>

    Ovdje su osnovne informacije o funkcioniranju naše platforme:<br/>
<ul>
    <li>Registrirani članovi imaju pristup bazi dostupnih čuvalica, koje mogu odabrati i

        rezervirati za termin po želji. Svi posjetitelji naše web stranice se obvezuju

        poštivati Opće uvjete poslovanja koji su prikazani na web stranici, s toga Vas

        molimo da proučite prava i obveze naručitelja, posrednika i pružatelja usluga.

        Općim uvjetima poslovanja ujedno se sklapa ugovor o suradnji i angažmanu za

        svaku napravljenu rezervaciju.</li>
    <li>Napravljenu rezervaciju možete platiti na 3 ponuđena načina, i to izravnom uplatom na bankovni račun Nanny.hr, kreditnim i debitnim karticama kroz sustav Pay Way i putem Pay Pal-a.</li>
    <li>Termini se rezerviraju s intervalima uvećanja od po 30 minuta.</li>
    <li>Kao što je vidljivo u cjeniku, Nanny naplaćuje naknadu za posredovanje ovisno o satima čuvanja. Također, u slučaju nepredvidivih produljenja u duljini čuvanja, Nanny će naknade za svoje usluge prilagoditi tim izmjenama.</li>
    <li>Čuvalice pristaju čuvati samo onu djecu koja su navedena u rezervaciji. Molimo da naknadno prilagodite rezervaciju potrebama čuvanja ako će biti više ili manje djece nego u inicijalnoj rezervaciji.</li>
    <li>Ne morate imati gotovinu kod sebe. Ipak, poželjna je kompenzacija za taxi prijevoz čuvalice ako je čuvanje obavljeno izvan radnih sati gradskog prijevoza. Također,napojnica za čuvalice je dobrodošla ako ste zadovoljni njihovim radom.</li>
</ul> <br/><br/>

Radujemo se uspješnoj suradnji s Vama.<br/>
<br/>



Srdačno,<br/>


Nanny Info Team </p> <br/><br/><br/>


<p>Thank you for choosing Nanny.hr as your childcare provider. It is our pivotal mission to offer you carefully curated and vetted, nannies that will best meet your needs.
    <br/>
    We can be reached via email: info@nanny.hr or by phone: +385953761155 and we stand ready to answer all your questions and help you book a nanny even for the time-slots that may not be available on the website. We will do our best in responding to all your queries.
    <br/>
    Here is some basic information about how our platform works:  <br/>
<ul>
    <li>Registered members have full access to profiles of our nannies, which

        they can choose and book for the time they please. By using our website, you

        indicate to consent to our Terms of use, and that you agree to abide by

        them. Thus we kindly ask you to read the rights and obligations concerning our

        clients, the agency, and the provider of services. Consenting to our Terms of use

        we also sign a contract per booking.</li>
    <li>Payment for the booking can be conducted in three ways: Direct Transfer to Nanny.hr bank account, PayWay and/or PayPal for all major credit cards.</li>
    <li>Bookings are made in increments of 30 minutes.</li>
    <li>As stated on our website, Nanny.hr charges a commission fee according to the duration of nanny’s engagement, and therefore will accordingly

        adjust commission fee due to any changes in duration differing from the initial booking.</li>
    <li>Please note nannies consent to provide care solely for the children specified in the initial booking. Nanny.hr collects the total fee for the booking. Please revise your booking or contact Nanny.hr if there will be a different number of children than initially expected.</li>
    <li>You do not have to make any cash payments, however many nannies do ask families to provide for cab fare home, especially if booking terminates late in the evening.</li>
    <li>Gratuity is always at your discretion</li>
</ul> <br/>

Please do not hesitate to reach out if you have any questions. <br/><br/>
We look forward to working with you. <br/><br/>
Nanny Info Team </p>
</div>

<div style="text-align:center;width:100%;height:170px;-webkit-background-size: cover;background: #F5F5F5;background-size: cover;">
    <div style="padding-top:2%">
        <p>Follow:</p><br/>
        <span style="margin-right:5%"><a style="text-decoration: none; font-weight:bold; color:black" href="https://www.facebook.com/Nanny-1700921193485926/">FACEBOOK</a></span><span><a style="text-decoration: none;font-weight:bold; color:black" href="https://www.instagram.com/nanny.hr/">INSTAGRAM</a></span>
    </div>
</div>
