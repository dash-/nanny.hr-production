<p>Rezervacija od Porodice: {{$family}}</p>

<p>Podaci:</p>

<table class="table">
    @foreach($data as $key => $info)
        @if($key == ' token')
            <?php continue ?>
        @endif
        <tr>
            <td>
                <p>Za period: {{$key}}</p>
                <div>
                    <p>Za djecu:</p>
                    <ul>
                        @if(isset($info['info']['children']) != 0)
                            @foreach($info['info']['children'] as $childKey => $child)
                                <li>{{$child}}</li>
                            @endforeach
                        @endif

                        @if(isset($info['info']['other_children']))
                            <li> + {{$info['info']['other_children']}}</li>
                        @endif
                    </ul>

                    @if(isset($info['info']['address_other']))
                        <p>Adresa: {{$info['info']['address_other']}}</p>
                    @endif
                    @if(isset($info['info']['special_note']))
                        <p>Posebne potrebe, zahtjevi i druge
                            napomene:  {{$info['info']['special_note']}}</p>
                    @endif

                    <p>Dadilje:</p>
                    <ul>
                        @if(isset($info['chosen_nannies']))
                            @foreach($info['chosen_nannies'] as $nanny)
                                <li>{{$nanny['name'].' '.$nanny['surname']}}</li>
                            @endforeach
                            @else
                            <li><li>Slobodni odabir čuvalice</li></li>
                        @endif
                    </ul>
                    @if(isset($info['price']))
                        <p>Cijena: {{$info['price']}}</p>
                    @endif
                </div>
            </td>
        </tr>

    @endforeach
</table>
<b style="font-size: 20px">Ukupno: {{$total}}{{$currency}}</b>

