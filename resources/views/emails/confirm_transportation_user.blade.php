<div style="width:100%;height:160px;text-align: center;
background: #ed7caf;
background: -moz-linear-gradient(45deg, #ed7caf 0%, #53cbf1 69%, #53cbf1 69%, #2ba9e0 100%);
background: -webkit-linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
background: linear-gradient(45deg, #ed7caf 0%,#53cbf1 69%,#53cbf1 69%,#2ba9e0 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed7caf', endColorstr='#2ba9e0',GradientType=1 );">
    <img style="padding:50px" width="200px" src="{{ $message->embed('css/logo transparent-01.png') }}" alt=""/>
</div>
        <table class="table">
            <tbody>
            <tr>
                <td>{{trans('lang.departure')}}</td>
                <td>{{$origin}}</td>
            </tr>
            <tr>
                <td>{{trans('lang.destination')}}</td>
                <td>{{$destination}}</td>
            </tr>
            <tr>
                <td>{{trans('lang.distance')}}</td>
                <td>{{$distance}}m</td>
            </tr>
            <tr>
                <td>{{trans('lang.date_time_display')}}</td>
                <td>{{$date}}</td>
            </tr>
            </tbody>
        </table>
        <div style="width:100%;height: 1px; background-color: black"></div>
        <b style="font-size: 20px">{{trans('lang.total')}}:<b id="total-price">{{$total}}</b> {{$currency}}</b>

<div style="text-align:center;width:100%;height:170px;-webkit-background-size: cover;background: #F5F5F5;background-size: cover;">
    <div style="padding-top:2%">
        <p>Follow:</p><br/>
        <span style="margin-right:5%"><a style="text-decoration: none; font-weight:bold; color:black" href="https://www.facebook.com/Nanny-1700921193485926/">FACEBOOK</a></span><span><a style="text-decoration: none;font-weight:bold; color:black" href="https://www.instagram.com/nanny.hr/">INSTAGRAM</a></span>
    </div>
</div>

