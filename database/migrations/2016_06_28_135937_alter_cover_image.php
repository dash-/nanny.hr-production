<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoverImage extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cover_images', function (Blueprint $table) {
			$table->string("text_en", 1000);
			$table->string("text_hr", 1000);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cover_images', function (Blueprint $table) {
			$table->dropColumn("text_en");
			$table->dropColumn("text_hr");
			//
		});
	}
}
