<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToParentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('parents', function (Blueprint $table) {
			$table->string('dob');
			$table->string('jmbg');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('parents', function (Blueprint $table) {
			$table->dropColumn('dob');
			$table->dropColumn('jmbg');
		});
	}
}
