<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransportationFieldsToReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservation_availabilities', function (Blueprint $table) {
            $table->string('origin_point');
            $table->string('destination_point');
            $table->integer('distance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservation_availabilities', function (Blueprint $table) {
            $table->dropColumn('origin_point');
            $table->dropColumn('destination_point');
            $table->dropColumn('distance');
        });
    }
}
