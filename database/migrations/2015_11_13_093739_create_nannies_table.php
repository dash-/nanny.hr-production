<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNanniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nannies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->dateTime('dob');
            $table->string('description');
            $table->string('years_of_nanny_exp');
            $table->string('year_of_payed_nanny_exp');
            $table->string('exp_with_age');
            $table->string('can_accept_urgent');
            $table->string('maximum_distance');
            $table->string('places');
            $table->string('own_car');
            $table->string('own_drivers_licence');
            $table->string('education_level');
            $table->string('education_title');
            $table->string('languages');
            $table->string('additional_skills');
            $table->string('additional_qualifications');
            $table->string('managing_with_pets');
            $table->string('care_sick');
            $table->string('care_disabled');
            $table->string('is_parent');
            $table->string('number_of_children');
            $table->string('smoker');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nannies');
    }
}
