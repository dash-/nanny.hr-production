<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsAddressAndOibToNanniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nannies', function (Blueprint $table) {
            $table->string('address');
            $table->string('oib');
            $table->string('agree_to_surveillance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nannies', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('oib');
            $table->dropColumn('agree_to_surveillance');
        });
    }
}
