<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransportReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('origin');
            $table->string('destination');
            $table->integer('distance');
            $table->dateTime('date');
            $table->float('price');
            $table->string('payment_type');
            $table->integer('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transport_reservations');
    }
}
