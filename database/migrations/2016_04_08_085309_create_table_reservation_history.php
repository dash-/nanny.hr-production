<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservationHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id');
            $table->string('extended_hours_from');
            $table->string('extended_hours_to');
            $table->float('extended_price');
            $table->string('payment_type');
            $table->integer('status');
            $table->string('edited_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations_history');
    }
}
