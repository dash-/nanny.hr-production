<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_availabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id');
            $table->integer('availability_id');
            $table->string('nanny');
            $table->string('children');
            $table->string('address');
            $table->string('date');
            $table->string('start');
            $table->string('end');
            $table->float('price');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_availabilities');
    }
}
