<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnglishFieldsToNanniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nannies', function (Blueprint $table) {
            $table->string('city');
            $table->string('description_eng');
            $table->string('agree_to_surveillance_eng');
            $table->string('years_of_nanny_exp_eng');
            $table->string('year_of_payed_nanny_exp_eng');
            $table->string('exp_with_age_eng');
            $table->string('can_accept_urgent_eng');
            $table->string('maximum_distance_eng');
            $table->string('places_eng');
            $table->string('own_car_eng');
            $table->string('own_drivers_licence_eng');
            $table->string('education_level_eng');
            $table->string('education_title_eng');
            $table->string('languages_eng');
            $table->string('additional_skills_eng');
            $table->string('additional_qualifications_eng');
            $table->string('managing_with_pets_eng');
            $table->string('care_sick_eng');
            $table->string('care_disabled_eng');
            $table->string('is_parent_eng');
            $table->string('number_of_children_eng');
            $table->string('smoker_eng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nannies', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('description_eng');
            $table->dropColumn('agree_to_surveillance_eng');
            $table->dropColumn('years_of_nanny_exp_eng');
            $table->dropColumn('year_of_payed_nanny_exp_eng');
            $table->dropColumn('exp_with_age_eng');
            $table->dropColumn('can_accept_urgent_eng');
            $table->dropColumn('maximum_distance_eng');
            $table->dropColumn('places_eng');
            $table->dropColumn('own_car_eng');
            $table->dropColumn('own_drivers_licence_eng');
            $table->dropColumn('education_level_eng');
            $table->dropColumn('education_title_eng');
            $table->dropColumn('languages_eng');
            $table->dropColumn('additional_skills_eng');
            $table->dropColumn('additional_qualifications_eng');
            $table->dropColumn('managing_with_pets_eng');
            $table->dropColumn('care_sick_eng');
            $table->dropColumn('care_disabled_eng');
            $table->dropColumn('is_parent_eng');
            $table->dropColumn('number_of_children_eng');
            $table->dropColumn('smoker_eng');
        });
    }
}
