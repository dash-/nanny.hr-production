<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function (Blueprint $table) {
			$table->increments('id');
			$table->string("title_hr")->nullable();
			$table->string("title_en")->nullable();
			$table->string("slug_hr")->nullable();
			$table->string("slug_en")->nullable();
			$table->string("image")->nullable();
			$table->string("summary_hr", 500)->nullable();
			$table->string("summary_en", 500)->nullable();
			$table->text("content_hr")->nullable();
			$table->text("content_en")->nullable();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}
}
