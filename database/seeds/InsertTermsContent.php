<?php

use Illuminate\Database\Seeder;

class InsertTermsContent extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$content = \App\Models\Content::create(['title' => 'Opći uvjeti', 'slug' => 'opci-uvjeti']);
		$content->text = '
    <p style="">
    <h3>I. OPĆE ODREDNICE</h3></p>

    <p style="text-align: left"><b>1.</b> Ovi Opći uvjeti uređuju odnose između Nanny, Naručitelja i Pružatelja usluga
        te pravila

        korištenja internetskih stranica www.nanny.hr (u nastavku: Nanny.hr). Oni ćine sastavni dio

        svakog Ugovora o pružanju usluga.</p>
    <br/>

    <p style="text-align: left"><b>2.</b> Podnošenjem zahtjeva na za sklapanje Ugovora o pružanju usluga na Nanny.hr,
        objavom

        profila čuvalice na Nanny.hr, sklapanjem Ugovora o pružanju usluga na standardiziranim

        obrascima Nanny te prijavom (registracijom) na Nanny.hr svatko neopozivo pristaje na

        primjenu ovih Općih uvjeta te ujedno potvrđuje da ih je pročitao, razumio i suglasio se sa

        time da ga ovi Opći uvjeti obvezuju, prihvaćajući sva pravila i uvjete navedene u njima.</p>
    <br/>

    <p style="text-align: left"><b>3.</b> Nanny.hr je postavljena u svrhu informiranja potencijalnih Naručitelja i
        potencijalnih

        Pružatelja usluga o djelatnosti Nanny i uslugama koje ona nudi, u svrhu dovođenja u vezu

        Naručitelja i Pružatelja

        Nanny.hr je Nanny.</p>
    <br/>

    <p style="text-align: left"><b>4.</b> Za sve što nije uređeno Općim uvjetima primjenjuju se pozitivni zakoni i
        propisi Republike

        Hrvatske.</p>

    <p style="text-align: left"><b>5. </b>Najniža dobna granica za korištenje Nanny.hr je 18 godina.</p>

    <p style="text-align: left"><b>6. </b>Za pojedine odnose između Nanny, Naručitelja i Pružatelja usluga mogu biti
        propisani

        drugačiji opći uvjeti. Ako su isti propisani i objavljeni, oni se primjenjuju prije ovih Općih

        uvjeta, u suprotnom primjenjuju se ovi Opći uvjeti.</p>

    <p style="text-align: left"><b>7. </b>Sastavni dio Općih uvjeta je Cjenik.</p>

    <br/>

    <p style="">
    <h3>II. ZNAČENJE POJMOVA</h3></p>
    <br/>

    <p style="text-align: left"><b>8.</b> Pojedini pojmovi koji se koriste u ovim Općim uvjetima imaju sljedeće
        značenje:

    <p style="text-align: left; margin-left:3%">a) Nanny je Nanny j.d.o.o. za posredovanje pri zapošljavanju, osnivač i
        direktor Omer

        Zvizdić, trgovačko društvo sa sjedištem u Zagrebu, Kneza Mutimira 1, OIB

        49862413433, MB 4426495, upisano u Sudski registar Trgovačkog suda u Zagrebu pod

        brojem Tj-15/21147-4) i obavlja djelatnost posredovanja pri zapošljavanju na temelju

        rješenja Ministarstva rada i mirovinskog sustava klasa: UP/I-102-02/15-03/19, urbroj:

        524-04-02-01/4-15-6 od 24. rujna 2015. g.</p>

    <p style="text-align: left; margin-left:3%">b) Naručitelj je svaka fizička osoba koja putem Nanny.hr ili na drugi
        prikladan način zatraži

        sklapanje Ugovora za pružanje usluga ili dovođenje u vezu s neodređenim Pružateljem

        usluge radi pregovaranja o sklapanju Ugovora za pružanje usluga.</p>

    <p style="text-align: left; margin-left:3%">c) Pružatelj usluge je svaka fizička osoba koja putem profila čuvalice
        na Nanny.hr

        neodređenim Naručiteljima nudi pružiti Redovite i Dodatne usluge u određeno vrijeme, u

        skladu s odredbama iz ovih Općih uvjeta i uz naknadu propisanu Cjenikom.</p>

    <p style="text-align: left; margin-left:3%">d) Usluge su Redovite i Dodatne. Ako nije drugačije dogovoreno, smatra
        se da su

        dogovorene sve Redovite usluge, kako specificirane Općim uvjetima važećim u vrijeme

        sklapanja ugovora.</p>

    <p style="text-align: left; margin-left:3%">e) Redovite usluge su usluge čuvanja djeteta ili djece, u odgovarajućem
        privatnom ili

        javnom prostoru određenom od Naručitelja, uz ili bez nadzora Naručitelja. Ako nije

        drugačije dogovoreno u pojedinom slučaju, Redovite usluge uključuju:

    <p style="text-align: left; margin-left:5%">- briga o sveopćoj dobrobiti i sigurnosti djeteta/djece;</p>

    <p style="text-align: left; margin-left:5%">- pripremanje obroka za dijete/djecu usuglašeno s Naručiteljem,
        hranjenje djece, previjanje, presvlačenje i mijenjanje pelena;</p>

    <p style="text-align: left; margin-left:5%">- aktivan angažman i participacija u igranju s djecom, animacija
        i slično;</p>

    <p style="text-align: left; margin-left:5%">- pomaganje djetetu/djeci pri izvršavanju domaćih zadataka za
        školu - samo ako su obje strane suglasne i tako im odgovara.</p>
    </p>

    <p style="text-align: left; margin-left:3%">f) Dodatne usluge su usluge prijevoza djeteta/djece na i sa mjesta
        dogovorenog s

        Naručiteljem. Ove usluge nadopunjuju Redovite usluge i mogu se ugovoriti samo uz sve

        ili pojedine Redovite usluge.</p>

    <p style="text-align: left; margin-left:3%">g) Cjene usluga i naknada uređene su Cjenikom. Cjenik je sastavni dio
        ovih Općih uvjeta i

        vrijedi jednako kao i ovi Opći uvjeti.</p>

    <br/>

    <p style="">
    <h3>III. USLUGE NANNY</h3></p><br/>

    <p style="text-align: left;"><b>9.</b> Nanny provodi postupke traženja, predselekcije i selekcije Pružatelja usluge, posreduje u sklapanju i/ili kao posrednik sklapa Ugovor o pružanju usluga, pronalazi i predlaže drugog odgovarajućeg Pružatelja usluge za slučaj nedostupnosti ranije odabranog Pružatelja usluge, priprema Ugovor o pružanju usluga te obavlja druge usluge, sve sukladno pravilima iz ovih Općih uvjeta.</p>
    <br/>

    <p style="text-align: left;"><b>10.</b> Nanny djeluje nepristrano u odnosu na Pružatelje usluga i Naručitelje.
        Naručitelj samostalno

        donosi odluku o odabiru pojedinog Pružatelja usluge kod sklapanja Ugovora o pružanju

        usluga.</p><br/>

    <p style="text-align: left;"><b>11.</b> Postupak traženja, predselekcije i selekcije Pružatelja usluga Nanny provodi
        između osoba

        koje su se kao takve javile Nanny (npr. pošiljanjem prijave za čuvalicu na Nanny.hr), kao i

        između osoba koje je Nanny sama potražila. Osobe koje su poslale prijavu nemaju prednost u

        odnosu na druge potencijalne Pružatelje usluga.</p> <br/>

    <p style="text-align: left;"><b>12.</b> U postupku traženja, predselekcije i selekcije Pružatelja usluga Nanny
        primjenjuje različite

        metode koje uzimaju u obzir različite zahtjeve, uvjete, znanja, vještine, iskustvo i sposobnosti

        potrebne za uredno izvršavanje usluga. Samo osobe koje su prošle postupak predselekcije i

        selekcije i koje su ocijenjene kao osobe koje zadovoljavaju sve potrebne uvjete za pružanje

        usluga mogu biti Pružatelji usluga i mogu imati profil čuvalice na Nanny.hr.</p><br/>

    <p style="text-align: left;"><b>13.</b> Uvjete iz prethodne točke Općih uvjeta Pružatelji usluge moraju ispunjavati za cijelo vrijeme dok imaju otvoren profil čuvalice na Nanny.hr. Radi provjere ispunjavanja tih uvjeta, Nanny:

    <p style="text-align: left; margin-left:5%">- prati urednost pružanja usluga po pojedinim Ugovorima o pružanju usluga tako što redovito, bez prethodne najave, kontaktira Pružatelja usluge i/ili Naručitelja, raspitujući se o tijeku izvršavanja usluge</p>
    <p style="text-align: left; margin-left:5%">- omogućava Naručiteljima podnošenje prigovora na izvršene usluge od strane pojedinog  Pružatelja usluga te ispituje i utvrđuje osnovanost prigovora Naručitelja, </p>
    <p style="text-align: left; margin-left:5%">- po potrebi poziva Pružatelje usluge na dodatne razgovore i provjere znanja i vještina</p>
    </p> <br/>

    <p style="text-align: left;"><b>14.</b> Svoje troškove dolaska i sudjelovanja u postupcima iz prethodne točke snose
        sami Pružatelji

        usluge odnosno potencijalni Pružatelji usluge.</p> <br/>

    <p style="text-align: left;"><b>15.</b> NNanny nastoji da dođe do sklapanja Ugovora o pružanju usluga te u tom smislu:

    <p style="text-align: left; margin-left:5%">- Naručiteljima koji žele nadzirati izvršavanje usluga putem uređaja za slikovno i/ili zvučno snimanje omogućava korištenje takvih uređaja (baby monitor)</p>

    <p style="text-align: left; margin-left:5%">- organizira izvršavanje Dodatnih usluga od strane drugih valjano osposobljenih osoba, ako Naručitelj zatraži Dodatne usluge, a odabrani Pružatelj usluge ih ne nudi ili ih ne može izvršiti.</p>

    </p> <br/>

    <p style="text-align: left;"><b>16.</b> Nanny ne odgovara za bilo kakvu štetu koja bi eventualno mogli nastati
        Naručitelju ili bilo

        kojim drugim osobama prilikom izvršavanja ugovorene usluge, bilo Redovite i/ili Dodatne

        usluge.</p><br/>

    <p style="text-align: left;"><b>17.</b> Za svoje usluge Nanny ima pravo na naknadu sukladno Cjeniku. Naknada se
        naplaćuje

        isključivo od Naručitelja, sukladno pravilima iz ovih Općih uvjeta.</p><br/>

    <br/>

    <p style="">
    <h3>IV. SKLAPANJE UGOVORA</h3></p><br/>

    <p style="text-align: left;"><b>18. </b>Ugovor o pružanju usluga (u daljnjem tekstu: Ugovor ili Ugovor o pružanju
        usluga) sklapaju

        Nanny, Naručitelj i Pružatelj usluga. Ugovor se može sklopiti pisanim putem ili putem

        Nanny.hr.</p>

    <p style="text-align: left;"><b>19.</b> Ako se Ugovor sklapa pisanim putem, sklapa se na standardnom obrascu pisanog Ugovora za pruženje usluga, kojeg priprema Nanny. Pisani Ugovor o pružanju usluga sklopljen je kada ga sve strane potpišu. S Pružateljem usluge koji je upisan u imenik dadilja uvijek se sklapa pisani ugovor. </p><br/>

    <p style="text-align: left;"><b>20.</b> Ako se Ugovor sklapa putem Nanny.hr, Ugovor je sklopljen kada Naručitelj
        putem Nanny.hr

        podnese zahtjev za sklapanje Ugovora i kada Nanny elektroničkom poštom obavijesti

        Naručitelja da prihvaća taj zahtjev. Zahtjev se podnosi unosom svih potrebnih podataka u

        standardizirane obrasce za naručivanje usluge (Trebam čuvalicu), koje je pripremila Nanny.

        Podaci u zahtjevu za naručivanje usluge moraju odgovarati podacima na profilu odabranog

        Pružatelja usluge.</p><br/>

    <p style="text-align: left;"><b>21.</b> Iznimno od odredbe iz prethodne točke Općih uvjeta, ako podaci ispunjeni u
        zahtjevu za

        naručivanje usluge iz bilo kojeg razloga ne bi bili dostatni za sklapanje Ugovora (npr. radi

        moguće zabune Naručitelja sa nekom drugom osobom), Ugovor neće biti sklopljen, a Nanny

        je ovlaštena u roku od 48 (slovima: četrdeset osam) sati od obavijesti o prihvaćanju zahtjeva

        zatražiti od Naručitelja dostavu dodatnih podataka i/ili isprava. U slučaju da Naručitelj u

        ostavljenom roku dostavi tražene podatke, Ugovor je sklopljen kada Nanny potvrdi da je

        primila tražene podatke. U slučaju da zbog prirode nedostataka sklapanje Ugovora na ovaj

        način nije moguće, Nanny će o tome obavijestiti Naručitelja i Pružatelja usluge, a po

        mogućnosti i uputiti poziv za sklapanje pisanog Ugovora.</p><br/>

    <p style="text-align: left;"><b>22.</b> U slučaju da se nakon sklapanja Ugovora utvrdi da Pružatelj usluge nije u
        mogućnosti izvršiti

        ugovorene usluge u ugovorenom vremenu, Nanny će u najkraćem mogućem roku pronaći

        drugog Pružatelja usluga, o tome na prikladan i brz način obavijestiti Naručitelja te ga

        pozvati na sklapanje Ugovora s drugim Pružateljem usluga. Ako Naručitelj pristane na

        sklapanje novog Ugovora, taj Ugovor se sklapa na isti način kao i svaki Ugovor, neovisno o

        načinu na koji je prethodni Ugovor bio sklopljen. Sklapanjem novog Ugovora raniji Ugovor

        smatra se raskinut.</p><br/>

    <p style="">
    <h3>V. CIJENE I PLAĆANJE</h3></p> <br/>

    <p style="text-align: left;"><b>23.</b> Naknade za usluge Pružatelja usluge i Nanny propisane su Cjenikom. U naknadu
        je uračunat

        pripadajući iznos poreza na dodanu vrijednost.</p><br/>

    <p style="text-align: left;"><b>24.</b> Nanny može uvijek ponuditi povoljnije naknade ili popuste i to kako za ukupne usluge Nanny i Pružatelja usluga, tako i za usluge samo jednoga od njih ili za dio usluga jednoga od njih. U slučaju da se nude povoljnije naknade ili popusti za neke usluge, takva ponuda biti će istaknuta na Nanny.hr. Ista obvezuje Nanny od trenutka njene objave na Nanny.hr. </p><br/>

    <p style="text-align: left;"><b>25.</b> U svaku istaknutu naknadu uračunat je pripadajući iznos poreza na dodanu vrijednost.</p><br/>
    <p style="text-align: left;"><b>26.</b> Nanny je ovlaštena jednostrano i u svakom trenutku izvršiti izmjene postojećeg Cjenika. Novi Cjenik primjenjuje se od trenutka objave na Nanny.hr. Ako bi Cjenik bio izmjenjen nakon podnošenja zahtjeva za sklapanje Ugovora putem Nanny.hr, a prije obavijesti o prihvatu tog zahtjeva od strane Nanny, na Ugovor se primjenjuje Cjenik koji je bio na snazi u vrijeme podnošenja zahtjeva za sklapanje Ugovora.</p><br/>


    <p style="text-align: left;"><b>27.</b> Ukupna naknada koju se Naručitelj sklapanjem Ugovora o pružanju usluga
        obvezuje platiti

        sastoji se od dva dijela:

    <p style="text-align: left; margin-left:3%">- naknade Pružatelju usluge za pružanje Redovitih i/ili Dodatnih usluga
        i</p>

    <p style="text-align: left; margin-left:3%">- naknade Nanny za usluge Nanny.</p>

    </p><br/>

    <p style="text-align: left;"><b>28.</b> Za slučaj da je Nanny organizirala obavljanje Dodatnih usluga od strane druge osobe različite od Pružatelja usluge, naknada za Dodatne usluge pripada osobi koja je obavila iste, a isplaćuje joj se posredstvom Nanny, jednako kao i naknada za Redovite usluge Pružatelju usluge. </p><br/>


    <p style="text-align: left;"><b>29.</b> Ukoliko Pružatelj usluge ne zbog svoje krivnje bude primoran produžiti
        vrijeme pružanja

        usluge, 10 (deset) minuta prekoračenja ugovorenog vremena ne naplaćuje se. Za produljenje

        duže od 10 (minuta), Nanny ima pravo naplatiti produženo pružanje usluge kao da se radi o

        novom Ugovoru o pružanju usluge. Prilikom naplate za produženo pružanje usluge,

        obračunava se i nova Nanny naknada te naknada za Pružatelja usluge, izuzev ako nije

        drugačije dogovoreno između Naručitelja i Nanny.</p><br/>

    <p style="text-align: left;"><b>30.</b> Plaćanje svih usluga vrši se posredstvom Nanny i to online naplatom
        kreditnim

        (American Express®, MasterCard®, Visa i Diners). Plaćanje omogućuje T-Com PayWay i

        PayPal sustav. Podnošenjem zahtjeva za sklapanje Ugovora putem Nanny.hr. ili

        potpisivanjem pisanog Ugovora, Naručitelj prihvaća Opće uvjete i daje suglasnost za ovaj

        način naplate. U trenutku podnošenja zahtjeva sredstva na kartici se rezerviraju, a na račun

        Nanny prenose se u roku od najviše 24 sata nakon obavljene usluge od strane Pružatelja

        usluge. Po prijenosu cjelokupnog iznosa naknade na račun Nanny, a najviše u roku od 20

        dana od tog prijenosa, Nanny prenosi na račun Pružatelja usluge neto iznos naknade za

        pružene usluge, a na račun Državnog proračuna i dr. pripadajuće iznose poreza, doprinosa i

        prireza, sukladno važećim propisima RH.

    <p>
        <a href="http://www.americanexpress.hr" target="_blank"><img src="/images/AmericanExpress50.jpg" alt=""/></a>
        <a href="http://www.mastercard.com" target="_blank"><img src="/images/MasterCard50.gif" alt=""/></a>
        <a href="http://www.maestrocard.com" target="_blank"><img src="/images/Maestro50.gif" alt=""/></a>
        <a href="http://www.visa.com" target="_blank"><img src="/images/Visa50.gif" alt=""/></a>
        <img src="/images/DC_na_rate_bijelo_NOVO.jpg" alt="" width="20%"/>
    </p>
    </p>

    <p style="text-align: left;"><b>31.</b> T-Com Pay Way primjenjuje najmodernije standarde u

        Secure Socket Layer (SSL) protokol sa 128-bitnom enkripcijom podataka i MD5 algoritam.

        ISO 8583 protokol osigurava da se razmjena podataka između T-Com sustava i

        autorizacijskih centara kartičnih kuća obavlja u privatnoj mreži, koja je od neautoriziranog

        pristupa zaštićena dvostrukim slojem "vatrozida" (firewalla).

    <p>
        <a href="#"><img src="/images/ax_jamstvo.gif" alt="" onClick="MM_openBrWindow(\'http://www2.americanexpress.hr/userinfo/Jamstvo_sigurne_online_kupnje.html\'
,\'prozorhr\',\'width=750,height=590\')"/></a>
        <a href="http://www.pbzcard.hr/media/53821/mcsc_hr.html" target="_blank"><img
                    src="/images/mastercard_securecode.gif" alt=""/></a>
        <a href="http://www.pbzcard.hr/media/53827/vbv_hr.html" target="_blank"><img
                    src="/images/verified_by_visa_reverse.jpg" alt=""/></a>
    </p>
    </p>
    <br/><br/>

    <p style="">
    <h3>VI. PRAVA I OBVEZE PRUŽATELJA USLUGA</h3></p> <br/>

    <p style="text-align: left;"><b>32.</b> Pružatelj usluge podnošenjem zahtjeva za otvaranjem profila čuvalice na Nanny.hr neodređenim Naručiteljima nudi pružiti Redovite i/ili Dodatne usluge u određeno vrijeme, u skladu s odredbama iz ovih Općih uvjeta i uz naknadu propisanu Cjenikom. Zahtjev za otvaranje Profila čuvalice podnosi se unosom svih potrebnih podataka u standardizirane obrasce za profile čuvalica, kojeg priprema Nanny. Otvaranje profila odobrava Nanny i o tome obavještava Pružatelja usluge.

    </p><br/>

    <p style="text-align: left;"><b>33.</b> Podnošenjem zahtjeva za otvaranjem profila na Nanny.hr Pružatelj usluge neopozivo pristaje na primjenu svih odredaba ovih Općih uvjeta.</p><br/>

    <p style="text-align: left;"><b>34.</b> Ako Pružatelj usluge ne nudi pružiti sve Redovite usluge ili ako ne nudi
        pružiti Dodatne

        usluge, a posebice ako nije suglasan sa time da Naručitelji nadziru njegovo pružanje usluga

        putem uređaja za slikovno i/ili zvučno snimanje, Pružatelj usluge dužan je u svom profilu to

        posebno naznačiti. Ako Pružatelj usluge pristaje na nadzor putem uređaja za slikovno i/ili

        zvučno snimanje samo uz izričitu prethodnu najavu takvog nadzora od strane Naručitelja i/ili

        Nanny, takav pristanak je također dužan posebno naznačiti u svom profilu.</p><br/>

    <p style="text-align: left;"><b>35.</b> Pružatelj usluge dužan je usluge izvršavati uz primjenu povećane pažnje,
        posebno vodeći

        računa o sigurnosti i dobrobiti djeteta. Ako je Naručitelj naznačio ili na bilo koji drugi način

        dao upute za pružanje zahtijevanih usluga, Pružatelj usluge dužan je izvršiti usluge sukladno

        primljenim uputama.
    </p><br/>

    <p style="text-align: left;"><b>36.</b> Ako Pružatelj usluge ne odredi ništa vezano uz pružanje usluga uz nadzor
        putem uređaja za

        slikovno i/ili zvučno snimanje, smatra se da je pristao na takvu vrstu nadzora u domu

        Naručitelja, neovisno o tome je li on najavljen ili nenajavljen, bilo njemu bilo Nanny.
    </p><br/>


    <p style="text-align: left;"><b>37.</b> Pristanak na nadzor iz prethodne točke ovih Općih uvjeta ne obuhvaća i
        pristanak za

        prikazivanje i reproduciranje tako sačinjenih snimaka trećim osobama različitim od

        konkretnog Naručitelja i osoba kojima je usluga pružena.</p><br/>

    <p style="text-align: left;"><b>38.</b> Podaci na profilu Pružatelja usluge su obvezujuća ponuda Pružatelja usluge
        za sklapanje

        Ugovora za pružanje usluga.</p><br/>

    <p style="text-align: left;"><b>39.</b> Pružatelj usluge odgovara za istinitost i točnost podataka na njegovu
        profilu te ih je dužan

        redovito ažurirati, kako bi uvijek bili točni i istiniti. U slučaju sklapanja Ugovora na temelju

        neistinih ili nedovoljno ažurnih podataka, Pružatelj usluge odgovara za svu štetu koja

        Naručitelju, Nanny ili drugim Pružateljima usluge od tuda nastane.</p><br/>

    <p style="text-align: left;"><b>40.</b> Nanny ima pravo i dužnost redovito provjeravati raspoloživost Pružatelja
        usluga u vrijeme

        koje Pružatelj usluga naznačuje u svom profilu. Radi provjere te raspoloživosti Nanny

        primjenjuje sva prikladna i raspoloživa sredstva, a posebice mailing liste, svoje internet baze

        te direktan kontakt telefonom i drugim servisima.
    </p><br/>

    <p style="text-align: left;"><b>41.</b> Pružatelju usluge je zabranjeno, bez pisane suglasnosti Nanny, izravnim ili
        neizravnim putem

        kontaktirati ili dogovarati pružanje usluga s Naručiteljima. Povreda ove odredbe smatra se

        najgrubljim oblikom kršenja Općih uvjeta od strane Pružatelja usluge. Za slučaj povrede ove

        odredbe, za svako neovlašteno kontaktiranje, dogovaranje ili pokušaj dogovaranja pružanja

        usluga bez sudjelovanja Nanny, neovisno o tome je li Nanny time nastala konkretna šteta,

        Pružatelj usluge je dužan platiti Nanny iznos od 2.000,00 kuna. Ako je zbog povrede ove

        odredbe Nanny nastala šteta koja je veća od prethodno navedenog iznosa, Nanny ima pravo

        od Naručitelja i Pružatelja usluge kao solidarnih dužnika zahtijevati razliku do potpune

        naknade štete.</p><br/>


    <p style="text-align: left;"><b>42.</b> Pružatelj usluge dužan je izvršiti usluge iz Ugovora o pružanju usluga, u
        vrijeme i opsegu

        kako ugovoreno tim Ugovorom i ovim Općim uvjetima, ako je to suglasno sa podacima

        objavljenim na njegovu profilu u vrijeme slanja zahtjeva za sklapanjem Ugovora putem

        Nanny.hr od Naručitelja.</p><br/>

    <p style="text-align: left;"><b>43.</b> Naručitelj ima pravo davati dodatne upute za pružanje usluga, sukladno
        odredbama ovih

        Općih uvjeta</p>
    <br/>

    <p style="text-align: left;"><b>44.</b> U slučaju da Pružatelj usluge ne može izvršiti ugovorene usluge, Nanny će
        Naručitelju bez

        odgode pronaći drugog prikladnog i dostupnog Pružatelja usluge, sukladno odredbama točke

        21 Općih uvjeta. U slučaju da Pružatelj usluge skrivio nemogućnost izvršavanja ugovorenih

        usluga (npr. neažurnošću podataka na profilu), Nanny ima pravo tražiti naknadu sve štete

        koja joj zbog takvih postupaka Pružatelja usluge nastane.</p><br/>


    <p style="text-align: left;"><b>45.</b> Pružatelju usluge zabranjeno je davanje djeci bilo kakvih medicinskih
        pripravaka ili lijekova

        bez pisanog naloga Naručitelja s kojim se je pisano suglasila Nanny.</p>

    <p style="text-align: left;"><b>46.</b> Pružatelj usluga dužan je za vrijeme izvršavanja usluga suzdržati se od bilo
        kakvog

        diskriminatornog govora, po bilo kojoj osnovi, kao i bilo kakvog ponašanja koje se može

        protumačiti kao nametanje vjerskih, političkih i bilo kakvih drugi uvjerenja.</p><br/>

    <p style="text-align: left;"><b>47.</b> Ako se pružanje usluge obavlja u domu Naručitelja, Pružatelj usluge dužan je
        poštivati

        privatnost doma Naručitelja i privatnost svih osoba koje se u njemu nalaze. Potonje

        prvenstveno uključuje zabranu bilo kakvog snimanja koje nije dogovoreno s Naručiteljem i

        osobama koje se nalaze u domu Naručitelja, objavljivanje tih snimaka trećim osobama te

        dovođenja trećih osoba u dom Naručitelja, osim ako drugačije nije dogovoreno pisanim

        putem između svih strana Ugovora. Objavljivanje snimaka trećim osobama nije dozvoljeno

        ni za snimke koje su sačinjene uz znanje i pristanak Naručitelja i osoba kojima se pruža

        usluga.</p><br/>

    <p style="text-align: left;"><b>48.</b> Pružatelj usluge dužan je poštivati kućni red doma i svakog drugog privatnog
        ili javnog

        prostora u kojoj se pružaju ugovorene usluge, uključujući i zgrade u kojoj se eventualno

        nalaze ti prostori.</p><br/>

    <p style="text-align: left;"><b>49.</b> Pružatelj je dužan imovinu, prostor i druge stvari u vlasti Naručitelja ili
        drugih osoba čuvati s

        povećanom pažnjom, posebno pazeći da im ne ošteti te da svojim postupcima Naručitelju i

        drugim osobama ne stvara troškove (npr. korištenjem telefona Naručitelja), osim ako nije

        drugačije dogovoreno pisanim putem između svih stranaka Ugovora.</p><br/>

    <p style="">
    <h3>VII. PRAVA I OBVEZE NARUČITELJA</h3></p> <br/>

    <p style="text-align: left;"><b>50.</b> Naručitelj nakon prijave (registracije) na Nanny.hr može pregledavati
        profile Pružatelja

        usluga, podnositi zahtjeve za sklapanje Ugovora o pružanju usluga ili zatražiti od Nanny

        odabir prikladnog i dostupnog Pružatelja usluga. Prijava na Nanny.hr smatra se dovršena

        unosom svih podataka o Naručitelju (roditeljima i/ili roditelju), a koji se traže u

        standardiziranim obrascima na Nanny.hr.</p><br/>

    <p style="text-align: left;"><b>51.</b> Naručitelj ima pravo davati dodatne upute za pružanje usluga. Dodatne upute
        daju se u

        pravilu prije izvršavanja Ugovora, kontaktiranjem Nanny na bilo koji prikladan način. Ako

        Naručitelj osobno nadzire pružanje usluga, upute za pružanje usluge može davati izravno

        Pružatelju usluge i/ili drugoj osobi koja obavlja usluge (npr. Dodatne usluge).</p><br/>


    <p style="text-align: left;"><b>52.</b> Naručitelj ima pravo nadzora nad pružanjem usluga. To svoje pravo koristi
        tako što može, po

        svojoj želji:

    <p style="text-align: left; margin-left:3%">- u bilo koje doba i bez ikakve najave neposredno opažati pružanje
        usluga,</p>

    <p style="text-align: left; margin-left:3%">- u bilo koje doba i bez ikakve najave kontaktirati Nanny i raspitivati
        se o pružanju usluga,</p>

    <p style="text-align: left; margin-left:3%">- prije pružanja usluga zatražiti od Nanny korištenje uređaja za
        slikovno i/ili zvučno

        snimanje pružanja usluga</p>

    <p style="text-align: left; margin-left:3%">- koristiti vlastite uređaje za slikovno i/ili zvučno snimanje pružanja
        usluga.</p>

    </p><br/>

    <p style="text-align: left;"><b>53.</b> Naručitelj ima pravo u roku od 24 (slovima: dvadeset četiri) sata od
        dovršetka izvršenja

        usluge, pisanim putem ili putem elektroničke pošte naznačene na Nanny.hr, Nanny dostaviti

        prigovor na pružanje usluge. Prigovor mora biti obrazložen na način da se iz njega jasno

        može utvrditi nedostatak na koji se ukazuje.</p><br/>

    <p style="text-align: left;"><b>54.</b> Ukoliko se povodom pravovremenog prigovora Naručitelja utvrdi da Pružatelj usluge nije pravovremeno i u potpunosti izvršio ugovorene usluge, Nanny će Naručitelja, ako je to još uvijek moguće, pozvati na sklapanje Ugovora o pružanju usluga sa drugim Pružateljem usluge, bez ikakve dodatne naknade za Naručitelja. Ako sklapanje takog Ugovora više nije moguće ili ako Naručitelj ne želi isti sklopiti, Naručitelj je oslobođen obveze plaćanja naknade za prethodno ugovorenu uslugu koja nije pružena. Ako je dio usluga izvršen, razmjerno će se sniziti naknada za prethodno ugovorenu uslugu ili za prvu narednu ugovorenu uslugu.</p><br/>

    <p style="text-align: left;"><b>55.</b> Naručitelj je dužan poduzeti sve radnje neophodne za pravovremeno i potpuno
        obavljanje

        dogovorenih usluga, a osobito je dužan omogućiti Pružateljima usluga pristup prostorijama i

        prostorima u kojima će se izvršavati usluge, omogućiti i osigurati da taj prostor i/ili prostorije

        imaju sve uvjete za siguran boravak, da su prikladne za izvršavanje ugovorenih usluga, a

        zatvoreni prostori pogotovo da imaju osiguranu električnu energiju, grijanje i slično.</p><br/>

    <p style="text-align: left;"><b>56.</b> Naručitelj se obvezuje da će prije početka izvršavanja usluga iz prostora u
        kojem se usluge

        pružaju skloniti sve predmete koji nisu potrebni za pružanje usluge, a mogu predstavljati

        izvor opasnosti za zdravlje i sigurnost djece ili imaju veću vrijednosti ili su osobitog značaja

        za Naručitelja, posebice nakit, dragocijenosti i slično.</p><br/>

    <p style="text-align: left;"><b>57.</b> Naručitelju je zabranjeno bez pisane suglasnosti Nanny, izravnim ili
        neizravnim putem,

        kontaktirati ili dogovarati korištenje usluga s Pružateljima usluga. Povreda ove odredbe

        smatra se najgrubljim oblikom kršenja Općih uvjeta od strane Naručitelja. Za slučaj povrede

        ove odredbe, za svako neovlašteno kontaktiranje, dogovaranje ili pokušaj dogovaranja

        izvršavanja određenih Usluga, neovisno o tome je li Nanny time nastala konkretna šteta,

        Nanny ima pravo naplatiti od Naručitelja deseterostruki najviši iznos naknade za usluge

        Nanny (Agencijske usluge) propisan Cjenikom. Ako je zbog povrede ove odredbe Nanny

        nastala šteta koja je veća od najvišeg iznosa naknade za usluge Nanny propisane Cjenikom,

        Nanny ima pravo od Naručitelja i Pružatelja usluge kao solidarnih dužnika zahtijevati razliku

        do potpune naknade štete.</p><br/>

    <p style="">
    <h3>VIII. PRESTANAK UGOVORA</h3></p> <br/>

    <p style="text-align: left;"><b>58.</b> Ugovor o pružanju usluga prestaje izvršenjem ugovorenih usluga ili raskidom
        sukladno ovim

        Općim uvjetima.</p><br/>

    <p style="text-align: left;"><b>59.</b> Raskid ugovora vrši se u obliku u kojem je sklopljen Ugovor koji se raskida.
        Ako je

        sklopljen pisanim putem, potrebna je pisana obavijest o raskidu, dostavljena preporučenom

        poštom ili osobno drugoj stranci. Ako je sklopljen putem Nanny.hr ili na drugi prikladan

        način, raskid se može izvršiti putem elektronske pošte, preporučene pošte ili predajom pisane

        obavijesti o raskidu osobno drugoj strani.</p><br/>

    <p style="text-align: left;"><b>60.</b> U slučaju povrede bilo koje obveze iz ovih Općih uvjeta od strane
        Naručitelja, Nanny je

        ovlaštena jednostrano raskinuti Ugovor o pružanju usluga sklopljen s Naručiteljem. U slučaju

        takvog raskida Nanny je ovlaštena u ime i za korist Pružatelja usluge naplatiti naknadu za sve

        usluge koje je pružio do dana raskida Ugovora. Naknadu za svoje usluge Nanny je ovlaštena

        naplatiti kao da je cijeli Ugovor uredno izvršen. Nanny je ujedno ovlaštena od Naručitelja

        naplatiti sve troškove prouzročene tim raskidom (trošak rezerviranja sredstava, troškovi

        prijenosa sredstava i sl.), kao i naknadu svake štete, materijalne i nematerijalne, koja joj je

        tim raskidom i/ili povredom odredaba ovih Općih uvjeta prouzročena.</p><br/>

    <p style="text-align: left;"><b>61.</b> Nanny je ovlaštena u roku od najkasnije 48 (slovima: četrdeset osam) sati od
        potvrde

        zahtjeva za sklapanje Ugovora putem Nanny.hr odnosno od potvrde zaprimanja dodatne

        dokumentacije navedene u točki 20., jednostrano raskinuti Ugovor, bez ikakvih troškova za

        Nanny, ako Nanny osnovano pretpostavlja da Pružatelj usluge bez svoje krivnje neće biti u

        mogućnosti pružiti ugovorene usluge.</p><br/>

    <p style="text-align: left;"><b>62.</b> Naručitelj je ovlašten u svako doba, bez obrazloženja, jednostrano raskinuti
        Ugovor ili

        otkazati Narudžbu. U slučaju takvog raskida, Naručitelj ima pravo na povrat novčanih

        sredstava koje je uplatio ili koji su rezervirani radi plaćanja, pod sljedećim uvjetima:

    <p style="text-align: left; margin-left:3%">- u slučaju raskida najkasnije 24 (slovima: dvadeset i četiri) sata
        prije ugovorenog početka

        pružanja usluga, pravo povrata 100% uplaćenog odnosno rezerviranog iznosa;</p>

    <p style="text-align: left; margin-left:3%">- u slučaju raskida manje od 24 (slovima: dvadeset i četiri) sata, a
        prije početka

        ugovorenog pružanja usluge, bit će naplaćena naknada kao da je pružena usluga u

        trajanju od 1 (jednog) sata i naknada za usluge Nanny koja se po Cjeniku naplaćuje za

        Ugovore o pružanju usluga u trajanju od 1(jednog) sata;</p>

    <p style="text-align: left; margin-left:3%">- u slučaju raskida nakon što je Pružatelj usluga već započeo s
        pružanjem usluga,

        Naručitelj nema pravo na povrat uplaćenog odnosno rezerviranog iznosa novčanih

        sredstava.</p>

    </p><br/>

    <p style="text-align: left;"><b>63.</b> Rok za povrat sredstava po odredbama ove glave Općih uvjeta je 10 (slovima:
        deset) radnih

        dana od dana raskida Ugovora. Povrat se vrši na karticu koja se koristila za naručivanje i

        osiguranje naplate naručene usluge. Ukoliko Nanny u roku iz ove točke Općih uvjeta izvrši

        povrat novčanih sredstava, Naručitelj nije ovlašten zahtijevati zatezne kamate.</p><br/>


    <p style="">
    <h3>IX. PRAVA I OBVEZE GLEDE PRIKUPLJANA I ZAŠTITE OSOBNIH PODATAKA</h3></p>
    <br/>

    <p style="text-align: left;"><b>64.</b> Nanny na temelju pristanka Pružatelja usluge te Naručitelja usluge Nanny
        prikuplja njihove

        osobne i druge podatke te o njima vodi evidenciju, obrađuje ih i razmjenjuje. Sve prethodno

        navedeno Nanny čini samo u mjeri u kojoj je to potrebno za uredno izvršenje usluga koje

        pruža te u mjeri u kojoj je to potrebno za uredno izvršenje njenih obveza iz zakona i drugih

        prisilnih propisa. Smatra se da je Pružatelj usluge dao pristanak na obradu osobnih podataka

        stavljanjem profila na Nanny.hr, a Naručitelj kreiranjem Narudžbe na Nanny.hr.</p><br/>

    <p style="text-align: left;"><b>65.</b> Nanny jamči Naručiteljima potpunu diskreciju u odnosu na sve podatke o
        Naručiteljima i/ili

        osobama kojima se usluga pruža, a koje bi Nanny, izravno ili posredstvom Pružatelja usluga,

        saznala ili koji bi joj na bilo koji drugi način bili učinjeni dostupnima. Opisanu diskreciju

        Nanny jamči za vrijeme trajanja Ugovora i neograničeno za cijelo vrijeme nakon prestanka

        Ugovora. Na isti način Nanny jamči diskreciju i za sve podatke o članovima obitelji

        Naručitelja i svih drugih osoba koje koriste prostor u kojem je ugovoreno pružanje usluge,

        bilo da se te osobe tamo samo zateknu tijekom pružanja usluga, bilo da tamo redovito žive ili

        stanuju.</p><br/>

    <p style="text-align: left;"><b>66.</b> Nanny se obvezuje informirati Naručitelje i korisnike Nanny.hr o načinu
        korištenja

        prikupljenih podataka. Nanny redovito daje Naručiteljima i Pružateljima usluga mogućnost

        izbora o upotrebi njihovih podataka, uključujući mogućnost odluke žele li ili ne da se njihovo

        ime ukloni s lista koje se koriste u svrhu marketinške svrhe.</p><br/>

    <p style="text-align: left;"><b>67.</b> Svi se podaci o Naručiteljima, osobama kojima se pružaju usluge, članovima
        obitelji

        Naručitelja i drugim osobama koje koriste prostor u kojem se pružaju usluge, kao i drugim

        osobama koje se zateknu u predmetnom prostoru, Nanny, Pružatelj usluge i svi poslovni

        partneri Nanny koji su u okviru svoje suradnje s Nanny bili dužni ili ovlašteni upoznati se s

        predmetninm podacima, dužni su iste strogo čuvati kao povjerljive. Isti osobe predmetne

        podatke mogu otkrivati trećoj strani samo ako budu zatraženi od strane suda, u

        odgovarajućem sudskom postupku ili ako obveza njihova otkrivanja bude propisana prisilnim

        propisom. U takvom slučaju Nanny, Pružatelj usluge i poslovni partneri dužni su o

        zatraženom ili obvezujućem otkrivanju bez odgode obavijestiti osobu čiji su to podaci te

        prilikom dostave traženih podataka maksimalno štititi interese osoba čiji su to podaci,

        otkrivajući samo ono što je nužno i prijeko potrebno.</p><br/>

    <p style="text-align: left;"><b>68.</b> Nanny ne jamči za zaštitu osobnih podataka ako su isti povrijeđeni radnjom
        osobe za koju

        Nanny ne odgovara, pa makar prenesena sa ili na sadržaj Nanny.hr.</p><br/>

    <p style="text-align: left;"><b>69.</b> Pružatelj usluge i Naručitelj imaju pravo na pristup i pravo na ispravak
        osobnih podataka

        putem elektroničke pošte Nanny. Na zahtjev pojedinog podnositelja Nanny će iz evidencije

        izbrisati podatke koji se na njega odnose, ako ih nije dužan prikupljati po zakonu i drugim

        pozitivnim propisima RH.</p><br/>

    <p style="text-align: left;"><b>70.</b> Uskrata osobnog podatka, davanje netočnih osobnih podataka ili prešućivanje
        osobnih

        podataka može imati za posljedicu raskid ugovora krivnjom tog suugovaratelja te obvezu

        naknade štete koja time bude prouzrokovana drugim suugovarateljima.</p><br/>


    <p style="">
    <h3>X. OSTALA PRAVA I OBVEZE PRUŽATELJA USLUGE, NARUČITELJA,

            KORISNIKA NANNY.HR i DRUGIH KORISNIKA STRANICA NANNY.HR (pravila

        korištenja internetskih Nanny.hr)</h3></p> <br/>

    <p style="text-align: left;"><b>71.</b> Sredstva za pružanje Dodatnih usluga (npr. automobil, gorivo i slično) te
        sve potrebne uvjete

        za sigurno obavljanje Dodatnih usluga dužna je osigurati osoba koja pruža Dodatne usluge.

        Troškovi i naknada za pružanje Dodatnih usluga uređeni su Cjenikom i pripadaju osobi koje

        je pružila Dodatne usluge u konkretnom slučaju.</p><br/>

    <p style="text-align: left;"><b>72.</b> Dodatne usluge mogu se pružati samo od strane osoba koje su odgovarajuće
        osposobljene za

        upravljanje vozilom u kojem je potrebno obaviti Dodatne usluge i samo u vozilima koje su

        odgovarajuće registrirana, osigurana i na drugi način osposobljena za sudjelovanje u

        prometu. Nanny ne odgovara za korištenje neprikladnih vozila prilikom obavljanja Dodatnih

        usluga od strane Pružatelja usluge.</p><br/>

    <p style="text-align: left;"><b>73.</b> Naručitelj i Pružatelj usluge obvezuju se da se neće lažno predstavljati ili
        predstavljati u ime

        drugih fizičkih ili pravnih osoba. U zahtjevu za sklapanje Ugovora, u postupku prijave na

        Nanny.hr, u postupku prijave za čuvalicu, kao i u postupku otvaranja profila za čuvalicu na

        Nanny.hr, Naručitelj i Pružatelj usluge, svaki u odgovarajućem i za njega primjenjivom

        dijelu, dužni su upisati točne i potpune podatke.</p><br/>

    <p style="text-align: left;"><b>74.</b> Naručitelj, Pružatelj usluge, registrirani korisnici Nanny.hr i ostali
        korisnici Nanny.hr dužni

        su informacije na Nanny.hr koristiti jedino i isključivo u svrhu pronalaženja odgovarajućih

        Pružatelja usluga, u svrhu nuđenja pružanja određenih usluga ili u svrhu informiranja o

        uslugama Nanny. Pod zlouporabom informacija na Nanny.hr osobito se podrazumijeva

        prikupljanje, čuvanje i objavljivanje osobnih podataka drugih Naručitelja i Pružatelj usluga,

        kao i prikupljanje, čuvanje i bilo kakvo korištenje osobnih podataka Naručitelja i Pružatelja

        usluga za svrhe različite od pronalaženja odgovarajućeg Pružatelja usluge, nuđena pružanja

        usluga, sklapanja Ugovora o pružanju usluga i informiranja o uslugama Nanny.</p><br/>

    <p style="text-align: left;"><b>75.</b> Svim osobama iz točke 72. ovih Općih uvjeta, bez izričitog odobrenja Nanny,
        zabranjeno je

        kopiranje baze podataka (profila i svih prikazanih podataka Pružatelja usluga) na bilo koji

        medij (uključujući pohranjivanje podataka iz baze Nanny.hr na računala korištenjem

        automatskih aplikacija), bez obzira na namjeru ili svrhu takvog postupanja i bez obzira radi li

        se o postupku samog Naručitelja, Pružatelja usluga, korisnika Nanny.hr i dr. ili osobe koja

        radi po njegovim uputama i nalozima. U slučaju postupanja suprotno ovim odredbama, osoba

        odgovorna za takvo postupanje dužna je naknaditi cjelokupnu štetu koju time prouzroči

        Nanny, Pružateljima usluga, drugim Naručiteljima i svim trećim osobama.</p><br/>

    <p style="text-align: left;"><b>76.</b> Sve osobe iz točke 72. ovih Općih uvjeta obvezuju se da neće bez
        odgovarajućeg odobrenja

        objavljivati, slati, razmjenjivati i na bilo koji drugi način na Nanny.hr činiti dostupnim

        sadržaje koji su zaštićeni tuđim autorskih i srodnim pravima.</p><br/>

    <p style="text-align: left;"><b>77.</b> Sve osobe iz točke 72. ovih Općih uvjeta obvezuju se da neće objavljivati,
        slati, razmjenjivati

        i na bilo koji drugi način na Nanny.hr činiti dostupnim sadržaje koji su protivni važećim

        propisima RH i/ili međunarodnim propisima, sadržaje koji su vulgarni, prijeteći, rasistički ili

        na bilo koji drugi način za Nanny, Pružatelje usluge, Naručitelje ili bilo koje druge osobe

        diskriminatorni, uvredljivi ili štetni. U slučaju povrede ove odredbe, Nanny će postupiti

        sukladno odredbama točke 80. ovih Općih uvjeta. Za takav neprihvatljiv sadržaj odgovara

        isključivo osoba koja ga je objavila ili na drugi način učinila dostupnim na Nanny.hr, kako

        kazneno, tako i prekršajno i materijalno, svim osobama kojima je takvim svojim postupanjem

        nanjela bilo kakvu štetu.</p><br/>

    <p style="text-align: left;"><b>78.</b> Sve osobe iz točke 72. ovih Općih uvjeta obvezuju se da putem Nanny.hr neće
        vršiti

        objavljivanje, slanje i razmjenu neželjenih sadržaja drugim osobama bez njihovog pristanka

        ili traženja.</p><br/>

    <p style="text-align: left;"><b>79.</b> Sve osobe iz točke 72. ovih Općih uvjeta obvezuju se da neće neće svjesno
        objavljivati, slati,

        razmjenjivati ili na bilo koji drugi način činiti dostupnim putem Nanny.hr sadržaje koji

        sadrže viruse ili slične računalne datoteke ili programe načinjene u svrhu uništavanja ili

        ograničavanja rada bilo kojeg računalnog softvera i/ili hardvera i telekomunikacijske opreme.</p><br/>

    <p style="text-align: left;"><b>80.</b> Sve osobe iz točke 72. ovih Općih uvjeta obvezuju se da neće objavljivati
        reklamni ili bilo

        kakav sličan sadržaj na Nanny.hr. te da neće koristiti Nanny.hr za oglašavanje ili izvođenje

        bilo kakvog komercijalnog, vjerskog, političkog ili bilo kojeg drugog oblika promidžbe.</p><br/>

    <p style="text-align: left;"><b>81.</b> Sve osobe iz točke 72. ovih Općih uvjeta dužne su suzdržavati se od svake
        inicijacije,

        poticanja i/ili izražavanja svojih političkih, vjerskih i drugih uvjerenja koje bi upućivale na

        diskriminaciju ili bilo kakav oblik netrpeljivosti prema drugima.</p><br/>

    <p style="text-align: left;"><b>82.</b> Nanny ima pravo ukloniti svaki sadržaj s Nanny.hr kojeg ocijeni nepodobnim
        te svaki sadržaj

        koji je u suprotnosti s ovim Općim uvjetima. Za takvo uklanjanje Nanny ne treba prethodno

        kontaktirati osobu koja je objavila predmetni sadržaj, uključujući i njenog autora, kao niti

        tražiti odobrenje od istih. Ako je autor ili objavitelj takvog sadržaja Naručitelj ili Pružatelj

        usluge, Nanny će s istim odmah raskinuti sve ugovorne odnose i ukinuti sve njihove

        korisničke račune, sve bez ikakve najave i otkaznog roka. Kod takvog raskida ugovora radi

        se o raskidu krivnjom suugovaratelja koji je objavljivao predmetne sadržaje.</p><br/>

    <p style="">
    <h3>XI. AUTORSKA PRAVA</h3></p> <br/>


    <p style="text-align: left;"><b>83.</b> Nanny nije odgovorna za bilo kakav sadržaj koji neovlaštena osoba može
        postaviti na Nanny

        hr. Nanny.hr zadržava autorska prava nad svim sadržajima objavljenim na Nanny.hr, osim

        ako za pojedini sadržaj nije drugačije naznačeno.</p><br/>

    <p style="text-align: left;"><b>84.</b> Zabranjeno je svako korištenje autorskog djela bez pisane suglasnosti
        vlasnika. Povreda te

        zabrane može biti razlog za pokretanje odgovarajućeg sudskog postupka radi neovlaštenog

        korištenja autorskog djela, kao i postupka radi naknade štete, materijalne i nematerijalne.</p><br/>


    <p style="">
    <h3>XII. OSTALO</h3></p> <br/>

    <p style="text-align: left;"><b>85.</b> Nanny ima pravo povremeno slati elektronsku poštu koja je povezana s
        aktivnostima

        svima koji su se registrirali na Nanny.hr. Elektronska pošta može sadržavati reklamne

        poruke, obavijesti o posebnim ponudama, kao i ostale vrste obavijesti. Primatelji takve

        elektronske pošte u bilo koje vrijeme mogu pismeno zahtijevati da Nanny da trajno ili

        privremeno prestane koristiti njegove osobne podatke za slanje elektroničke pošte.</p><br/>

    <p style="text-align: left;"><b>86.</b> Nanny na Nanny.hr objavljuje i linkove na vanjske stranice koje nisu u
        njegovom vlasništvu

        i/ili nisu nastale u sklopu Nanny.hr. Posrednik ne jamči korisnicima tih stranica i/ili portala

        zaštitu podataka, odnosno ne jamči za sadržaj tih stranica. Posrednik neće odgovarati za

        eventualnu štetu nastalom korištenjem savjeta preuzetih sa drugih portala / autora članka /

        pedijatara / psihologa / defektologa / savjetnika.</p><br/>

    <p style="">
    <h3>XIII. ZAVRŠNE ODREDBE</h3></p> <br/>

    <p style="text-align: left;"><b>87.</b> Sporovi koji bi proizašli iz Ugovora i Općih uvjeta, uključujući sporove u
        pogledu

        tumačenja, primjene ili izvršenja ovih Općih uvjeta, Ugovorne strane nastojat će riješiti

        mirnim putem. Ukoliko isto nije moguće, nadležni sud bit će stvarno nadležni sud u Zagrebu.</p><br/>

    <p style="text-align: left;"><b>88.</b> Ovi Opći uvjeti će se objaviti na Nanny.hr. Na taj način objavit će se i sve
        eventualne

        izmjene i dopune Općih uvjeta.</p><br/>

    <p style="text-align: left;"><b>89.</b> Nanny ima pravo u svako doba jednostrano izmijeniti i/ili dopuniti ove Opće
        uvjete, bez

        prethodne obavijesti Naručitelju, Pružatelju usluga, korisniku Nanny.hr ili bilo kojoj trećoj

        osobi. Nanny neće biti odgovorna za moguće posljedice proizašle iz neupoznavanja izmjena

        i/ili dopuna Općih uvjeta od strane osoba na koje se ovi Opći uvjeti primjenjuju Navedene

        izmjene i/ili dopune stupaju na snagu objavom na Nanny.hr, izuzev ako u njima nije nešto

        drugačije određeno.</p><br/>

    <p>Ovi Opći uvjeti primjenjuju se od dana ________________. godine.</p>';
		$content->save();
	}
}
