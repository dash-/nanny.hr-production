<?php

use Illuminate\Database\Seeder;
use \App\Models\Price;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \Illuminate\Support\Facades\DB::table('prices')->truncate();

        $prices = [


            //Dnevne srednje-trajanje cijene
            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 35
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 40
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 45
            ],

            //Nocne srednje-trajanje cijene
            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => false,
                'agency_price' => 110,
                'price' => 50
            ],

            //Dnevne srednje-trajanje vikend cijene
            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 50
            ],

            //Nocne srednje-trajanje vikend cijene

            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 2,
                'weekend' => true,
                'agency_price' => 110,
                'price' => 50
            ],

            //Dnevne dugotrajne cijene
            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 30
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 35
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 40
            ],

            //Nocne Dugotrajne
            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 35
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 40
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => false,
                'agency_price' => 150,
                'price' => 45
            ],

            // Dnevne dugotrajne vikend cijene

            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 35
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 40
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 45
            ],

            //Nocne dugotrajne vikend cijene

            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 35
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 40
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 3,
                'weekend' => true,
                'agency_price' => 150,
                'price' => 45
            ],



            //Dnevne kratkotrajne cijene
            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 35
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 40
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 45
            ],

            //Nocne kratkotrajne cijene
            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => false,
                'agency_price' => 80,
                'price' => 50
            ],

            //Dnevne kratkotrajne vikend cijene
            [
                'children' => 0,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => true,
                'night' => false,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 50
            ],

            //Nocne kratkotrajne vikend cijene

            [
                'children' => 0,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 5
            ],
            [
                'children' => 1,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 40
            ],
            [
                'children' => 2,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 45
            ],
            [
                'children' => 3,
                'day' => false,
                'night' => true,
                'duration' => 1,
                'weekend' => true,
                'agency_price' => 80,
                'price' => 50
            ],

        ];

        foreach($prices as $price) {
            Price::create($price);
        }
    }
}
