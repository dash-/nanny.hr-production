<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Family;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'id' => 1,
            'email' => 'info@nanny.hr',
            'password' => bcrypt('nannyadmin123'),
            'role' => 1,
            'completed' => 3,
            'token' => bcrypt('info@nanny.hr' . '' . bcrypt('nannyadmin123'))];

        $family = [
            'id' => 1,
            'user_id' => 1,
        ];

        User::create($admin);
        Family::create($family);
    }
}
