<?php
return array(
    'client_id' => env('PAYPAL_CLIENT_ID', 'AcLI7XR7NjWrt0Sgsu3bCdCicFqxS_hEN41GEcY1sjRfSBNcwMD39OckkylLhDoQkUKu-2sYmfM_C_65'),
    'secret' => env('PAYPAL_SECRET', 'EEWVhn9HiICFictLtdFN3zzejjqrjcQm8NegABjF88JvqOefRdtc7wEE4zaPl-ncQomo9IeHgUgzGpPb'),


    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => env('PAYPAY_MODE', 'live'),

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);