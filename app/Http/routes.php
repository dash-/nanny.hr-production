<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('home');
//});
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@getIndex']);
Route::get('/jezik/{jezik}', 'HomeController@getJezik');

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

Route::get('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@postRegister']);

Route::get('/register-step1', ['as' => 'registerstep1', 'uses' => 'RegisterController@getRegisterStep1']);
Route::post('/register-step1', ['as' => 'postRegisterStep1', 'uses' => 'RegisterController@postRegisterStep1']);

Route::get('/register-step2', ['as' => 'registerstep2', 'uses' => 'RegisterController@getRegisterStep2']);
Route::post('/register-step2', ['as' => 'postRegisterStep2', 'uses' => 'RegisterController@postRegisterStep2']);

Route::get('/register-step3', ['as' => 'registerstep3', 'uses' => 'RegisterController@getRegisterStep3']);
Route::post('/register-step3', ['as' => 'postRegisterStep3', 'uses' => 'RegisterController@postRegisterStep3']);

Route::get('/register-step4', ['as' => 'registerstep4', 'uses' => 'RegisterController@getRegisterStep4']);
Route::post('/register-step4', ['as' => 'postRegisterStep4', 'uses' => 'RegisterController@postRegisterStep4']);

//Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::get('/forgot-password', ['as' => 'ForgotPassword', 'uses' => 'Auth\AuthController@getForgotPassword']);
Route::post('/forgot-password', ['as' => 'postForgotPassword', 'uses' => 'Auth\AuthController@postForgotPassword']);

Route::get('/reset-password', ['as' => 'ResetPassword', 'uses' => 'Auth\AuthController@getResetPassword']);
Route::post('/reset-password', ['as' => 'postResetPassword', 'uses' => 'Auth\AuthController@postResetPassword']);

//Route::post('/paypal-payment', ['as' => 'postPayment', 'uses' => 'PaypalPaymentController@postPayment']);

Route::controller('user', 'UserController');
Route::controller('nannies', 'NanniesController');
Route::controller('home', 'HomeController');
Route::controller('availability', 'AvailabilityController');
Route::controller('reservation', 'ReservationController');
Route::controller('payment', 'PaypalPaymentController');
Route::controller('admin', 'AdminController');
Route::controller('novosti', 'NovostiController');
Route::controller('transport', 'TransportController');
Route::controller('mpayment', 'MiscPaymentController');

//webhook
Route::get("git-pull", ['uses'=> "ArtisanController@gitpull"]);

Route::get('/onama', ['as' => 'onama', 'uses' => 'HomeController@getOnama']);
Route::get('/kako-rezervirati-cuvalicu', ['as' => 'kako-rezervirati-cuvalicu', 'uses' => 'HomeController@getKakoRezerviratiCuvalicu']);
Route::get('/zelim-biti-cuvalica', ['as' => 'nanny-application', 'uses' => 'HomeController@getZelimBitiCuvalica']);
Route::post('/zelim-biti-cuvalica', ['as' => 'post-nanny-application', 'uses' => 'HomeController@postZelimBitiCuvalica']);
Route::get('/cjenik', ['as' => 'cjenik', 'uses' => 'HomeController@getCjenik']);
Route::get('/kontakt', ['as' => 'kontakt', 'uses' => 'HomeController@getKontakt']);
Route::post('/kontakt', ['as' => 'post-kontakt', 'uses' => 'HomeController@postKontakt']);
Route::get('/opci-uvjeti', ['as' => 'kontakt', 'uses' => 'HomeController@getOpciUvjeti']);
Route::get('/404', ['as' => '404', 'uses' => 'HomeController@getError404']);
Route::get('/browser-incompatible', function () {
    return view('browser_incompatible');
});