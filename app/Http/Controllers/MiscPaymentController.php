<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 10.5.2016
 * Time: 18:30
 */
namespace App\Http\Controllers;


use App\Contracts\PayWayHelper;
use App\Helpers\EmailHelper;
use App\Helpers\GeneralHelper;
use App\Models\Availability;
use App\Models\Reservation;
use App\Models\ReservationAvailability;
use App\Models\ReservationHistory;
use App\Models\Token;
use App\Models\TransportReservations;
use App\Models\UsedPackage;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MiscPaymentController extends LoggedController
{

    private $_api_context;

    public function __construct(Request $request)
    {
        parent::__construct();
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);

        if (Auth::user()->role == 1 && $request->session()->has('ar_user_id')) {
            $user = User::find($request->session()->get('ar_user_id'));
            $this->user = $user;
        }
    }

    public function getIndex(Request $request)
    {
        $price = $request->session()->get('transport');
        $origin = $request->session()->get('transport_origin');
        $destination = $request->session()->get('transport_destination');
        $distance = $request->session()->get('distance');
        $datetime = $request->session()->get('transport_date');

        return view('finalizeTransportReservation', ['date' => $datetime, 'price' => $price, 'origin' => $origin, 'destination' => $destination, 'distance' => $distance, 'navigation_black' => true]);
    }

    public function getInitiateDirectPayment(Request $request)
    {

        $request->session()->put('payment_type', 'Direktno');

        $discount_token = $request->query('token');
        $request->session()->put('discount_token_pw', $discount_token);

        return response(['status' => 'ok', 'url' => '/mpayment/direct-payment']);

    }

    public function getDirectPayment(Request $request)
    {
        $request->session()->put('payment_type', 'Direktno');
        $discount_token = $request->session()->get('discount_token_pw');
        $total_price = $request->session()->get('transport');

        $discount = 0;

        if ($discount_token != '') {
            $token = Token::where('token', $discount_token)->first();
            if ($token != null) {
                if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
                    return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
                } else {
                    $discount = $token->discount;
                    $request->session()->put('discount_token_id', $token->id);
                }
            }
        }

        //if there is a discount token
        if ($discount != 0)
            $total_price = $total_price - ($total_price * $discount / 100);

        $userId = $this->user->id;

        $order = new TransportReservations();
        $order->user_id = $userId;
        $order->origin = $request->session()->get('transport_origin');
        $order->destination = $request->session()->get('transport_destination');
        $order->distance = $request->session()->get('distance');
        $order->date = date('Y-m-d H:i:s', strtotime($request->session()->get('transport_date')));
        $order->price = $total_price;
        $order->save();

        $request->session()->put('pid', $order->id);
        $success_url = '/mpayment/success';

        return view('directPayment', ['navigation_black' => true, 'url' => $success_url]);
    }

    public function getPayWay(Request $request)
    {
        $discount_token = $request->query('token');
        $request->session()->put('discount_token_pw', $discount_token);

        return response(['status' => 'ok', 'url' => '/mpayment/initiate-pay-way']);
    }


    public function getInitiatePayWay(Request $request)
    {
        $request->session()->put('payment_type', 'PayWay');
        $discount_token = $request->session()->get('discount_token_pw');
        $discount = 0;

        if ($discount_token != '') {
            $token = Token::where('token', $discount_token)->first();
            if ($token != null) {
                if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
                    return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
                } else {
                    $discount = $token->discount;
                    $request->session()->put('discount_token_id', $token->id);
                }
            }
        }

        $userId = $this->user->id;
        $total_price = $request->session()->get('transport');
        if ($discount != 0)
            $total_price = $total_price - ($total_price * $discount / 100);

        $order = new TransportReservations();
        $order->user_id = $userId;
        $order->origin = $request->session()->get('transport_origin');
        $order->destination = $request->session()->get('transport_destination');
        $order->distance = $request->session()->get('distance');
        $order->date = date('Y-m-d H:i:s', strtotime($request->session()->get('transport_date')));
        $order->price = $total_price;
        $order->save();

        $info = trans('lang.transport');

        $request->session()->put('pid', $order->id);
        $payway = new PayWayHelper($total_price, $info, 'T');
        $payway->pgw_order_id = $order->id;
        $hash = $payway->generateHash();


        return view('payway', ['payway' => $payway, 'hash' => $hash]);

    }

    public function getPayment(Request $request)
    {
        $details = $request->session()->get('total_info');
        $total = $request->session()->get('total');

        return view('finalizeReservation', ['details' => $details, 'total' => $total, 'navigation_black' => true
        ]);
    }

    public function postPayment(Request $request)
    {
        //////////////////PayPal////////////////
        $request->session()->put('payment_type', 'PayPal');
        $discount_token = $request->discount_token;
        $discount = 0;

        if ($discount_token != '') {
            $token = Token::where('token', $discount_token)->first();
            if ($token != null) {
                if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
                    return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
                } else {
                    $discount = $token->discount;
                    $request->session()->put('discount_token_id', $token->id);
                }
            }
        }

        $userId = $this->user->id;
        $total_price = $request->session()->get('transport') * 0.130546;
        if ($discount != 0)
            $total_price = $total_price - ($total_price * $discount / 100);

        $items = [];
        $itemPrice = $total_price;
        $description = 'Nanny.hr - ' . trans('lang.transport');

        $item = new Item();
        $item->setName('Nanny.hr prevoz')
            ->setDescription($description)
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($itemPrice);

        $items[] = $item;

        $order = new TransportReservations();
        $order->user_id = $userId;
        $order->origin = $request->session()->get('transport_origin');
        $order->destination = $request->session()->get('transport_destination');
        $order->distance = $request->session()->get('distance');
        $order->date = date('Y-m-d H:i:s', strtotime($request->session()->get('transport_date')));
        $order->price = $request->session()->get('transport');
        $order->save();

        $request->session()->put('pid', $order->id);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        // add item to list
        $item_list = new ItemList();
        $item_list->setItems($items);

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total_price);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription(trans('lang.nanny_reservation'));

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url('/mpayment/payment-status'))
            ->setCancelUrl(url('/mpayment/payment'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (Config::get('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        $request->session()->put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {
            // redirect to paypal
            return redirect($redirect_url);
        }

        return redirect('/mpayment/failed')
            ->with('error', 'Unknown error occurred');

    }

    public function getPaymentStatus(Request $request)
    {
        // Get the payment ID before session clear
        $payment_id = $request->session()->get('paypal_payment_id');

        // clear the session payment ID
//        Session::forget('paypal_payment_id');

        if (!$request->query('PayerID') || !$request->query('token')) {
            return redirect('/mpayment/failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));

        //Execute the payment
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));

        //Execute the payment

        try {
            $result = $payment->execute($execution, $this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode(); // Prints the Error Code
            $error = json_decode($ex->getData(), true);
            return redirect('/mpayment/failed')->with('error', $error['message']);
//            var_dump($error['name']);// Prints the detailed error message
//            var_dump($error['message']);
//            die;
        }

//        echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
        if ($result->getState() == 'approved') {
            /*  $order = OrderHistory::where("id","=",$order_id);
              $order->payment_response = "1";

              $order->save();*/
            $request->session()->put('currency', '€');
            return redirect('/mpayment/success');
        }
        return redirect('/mpayment/failed');
    }

    public function getSuccess(Request $request)
    {
        $payment_type = $request->session()->get('payment_type');
        $payed = ($payment_type == 'Direktno') ? 0 : 1;

            $reservation = TransportReservations::find($request->session()->get('pid'));
            $reservation->payment_type = $payment_type;
            $reservation->status = $payed;
            $reservation->save();

            $token = Token::find($request->session()->get('discount_token_id'));
            if (isset($token) && $token != null) {
                if ($token->type == 0) {
                    $token->update(['used' => 1]);
                }
            }

            $total = $request->session()->get('transport');
            $currency = ($request->session()->has('currency') ? $request->session()->get('currency') : 'kn');

            $emailDataUser = ['subject' => trans('lang.transport_email_confirmation'), 'to' => $this->user->email];

            EmailHelper::sendEmail('emails.confirm_transportation_user', ['origin' => $request->session()->get('transport_origin'),'destination' => $request->session()->get('transport_destination'),'distance' => $request->session()->get('distance'),'date' => $request->session()->get('transport_date'), 'total' => $total, 'currency' => $currency], $emailDataUser);

            $family = $this->user->full_name();

            $emailDataNanny = ['subject' => trans('lang.transport_email_confirmation_family') .' '. $this->user->full_name(), 'to' => 'info@nanny.hr'];
            EmailHelper::sendEmail('emails.confirm_transportation_nanny', ['origin' => $request->session()->get('transport_origin'),'destination' => $request->session()->get('transport_destination'),'distance' => $request->session()->get('distance'),'date' => $request->session()->get('transport_date'), 'total' => $total, 'family' => $family, 'currency' => $currency], $emailDataNanny);

            $request->session()->forget('transport');
            $request->session()->forget('transport_origin');
            $request->session()->forget('transport_destination');
            $request->session()->forget('transport_date');
            $request->session()->forget('distance');
            $request->session()->forget('total');

        return view('paymentSuccess', [
            'navigation_black' => true
        ]);
    }

    public function getFailed(Request $request)
    {
            $reservation = TransportReservations::find($request->session()->get('pid'));
            if ($reservation)
                $reservation->delete();

            $request->session()->forget('transport');
            $request->session()->forget('transport_origin');
            $request->session()->forget('transport_destination');
            $request->session()->forget('transport_date');
            $request->session()->forget('distance');
            $request->session()->forget('total');

        return view('paymentFailed', [
            'navigation_black' => true
        ]);
    }
}