<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 13.11.2015
 * Time: 11:23
 */

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Models\Availability;
use App\Models\Family;
use App\Models\Holiday;
use App\Models\Nanny;
use App\Models\Price;
use App\Models\UsedPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AvailabilityController extends Controller
{

	public $user;

	public function __construct()
	{
		$request = Request::capture();
		parent::__construct($request);
		$this->user = Auth::user();
	}


    public function getIndex(Request $request) {
        if(!Auth::check()) {
            return redirect('/');
        }
        else {
            if(Auth::user()->role != 1)
                return redirect('/availability/availability');


//        $request->session()->flush();
		$users = Family::where('id', '!=', 1)->paginate(20);
		return view('adminReservation', ['users' => $users, 'navigation_black' => true]);
	    }
	}

	public function getAvailability(Request $request, $id = null)
	{
		if ($id != null) {
			$request->session()->put('ar_user_id', $id);
		}

		$from_date = $to_date = null;
		$from = $request->session()->get('from_date');
		$to = $request->session()->get('to_date');
		if ($from)
			$from_date = date('d.m.Y H:i', strtotime($from));

		if ($to)
			$to_date = date('d.m.Y H:i', strtotime($to));

		return view('availability', [
			'navigation_black' => true,
			'page_title' => 'Trebam čuvalicu',
			'from_date' => $from_date,
			'to_date' => $to_date,
			'model' => new Request(),
			'page_description' => 'Dadilja, cuvalica, čuvalica, babysitter, čuvanje djece, nanny zagreb, trebam čuvalicu, bejbisiterica, pomoć oko djeteta, babysitters, childcare, caregiver'
		]);
	}

	public function postAvailability(Request $request)
	{
		$this->validate($request, [
			'from' => 'required',
			'to' => 'required',
		], [
			'from.required' => trans('lang.enter_start_time'),
			'to.required' => trans('lang.enter_end_time'),
		]);
		$available_nannies = [];
//        if(!isset($request['data']))
//            return redirect()->back()->with('errors', 'Pogresan format vremena');

//        foreach ($request['data'] as $data) {

//            if ($data == 'undefined')
//                continue;

//            $from_date_string = str_replace('GMT+0100 (Central European Standard Time)', '', $data['start']);
//            $to_date_string = str_replace('GMT+0100 (Central European Standard Time)', '', $data['end']);

		$from_date_format = date('Y-m-d H:i:s', strtotime($request->from));
		$to_date_format = date('Y-m-d H:i:s', strtotime($request->to));

		if ($from_date_format < date('Y-m-d H:i:s') || $to_date_format < date('Y-m-d H:i:s') || $to_date_format < $from_date_format)
			return redirect()->back()->with('errors', trans('lang.time_not_valid'));
		else {
			$available = Availability::where('from', '<=', $from_date_format)->where('to', '>=', $to_date_format)->where('isAvailable', true)->groupBy('nanny_id')->get();

			if ($available->count()) {
				foreach ($available as $avail) {

					$nannies = Nanny::join('nannies_images', 'nannies.id', '=', 'nannies_images.nanny_id')->where('nannies.id', $avail->nanny_id)->first();
if($nannies){
					$available_nannies[$from_date_format . ' - ' . $to_date_format]['nannies'][] = $nannies->toArray();
					$available_nannies[$from_date_format . ' - ' . $to_date_format]['availability'][$avail->id] = $nannies->toArray();
}
//                        $available_nannies[$from_date_format . ' - ' . $to_date_format]['availability'][] = $avail ->toArray();
				}
			} else
				$available_nannies[$from_date_format . ' - ' . $to_date_format] = '0';
		}
//        }
		$request->session()->put('from_date', $from_date_format);
		$request->session()->put('to_date', $to_date_format);
		$request->session()->put('data', $available_nannies);

		if (Auth::check())
//            return response(['status' => 'ok', 'url' => '/reservation']);
			return redirect('/reservation');
		else
//            return response(['status' => 'ok', 'url' => '/availability/available-nannies']);
			return redirect('/availability/available-nannies');
	}

	public function getAvailableNannies(Request $request)
	{
		$data = $request->session()->all();
		return view('availableNannies', ['data' => $data['data'], 'navigation_black' => true
		]);
	}

	public function postAvailableNannies(Request $request)
	{

		$times = $request->session()->get('result');
		$result = [];
		$total_info_prepared = [];
		$total_info = [];
		$total = 0;

		if ($times) {
			foreach ($times as $key => $time) {
				if ($key == ' token')
					continue;

				if (!$request->nannies_chosen)
					return redirect('/availability/available-nannies')->with('errors', trans('lang.select_one_nanny'));

				if (!array_key_exists('nannies', $time))
					continue;

				if ($time['info']['address_other'] == '') {
					return redirect('/reservation')->with('message', trans('lang.missing_address'));
				}

				$day = GeneralHelper::weekendDay(date('l', strtotime(substr($key, 0, 19))));
//
				$hourFormer = date('G', strtotime(substr($key, 0, 19)));
				$hourLatter = date('G', strtotime(substr($key, 23, 41)));

				$checkHalfFormer = date('i', strtotime(substr($key, 0, 19)));
				$checkHalfLatter = date('i', strtotime(substr($key, 23, 41)));

				$totalHours = (strtotime(substr($key, 23, 41)) - strtotime(substr($key, 0, 19))) / 60 / 60;

				if ($hourLatter == 0) {
					$hourLatter = 24;
				}
//            $long = ($hourLatter - $hourFormer < 6) ? 0 : 1;
				$long = GeneralHelper::reservationDuration($totalHours);

				$additional = 0;
				$additional_price = 0;
				$price = 0;

				$num_of_children = (isset($time['info']['children'])) ? GeneralHelper::numberOfChildren(count($time['info']['children']), $time['info']['other_children']) : GeneralHelper::numberOfChildren(0, $time['info']['other_children']);

				if ($num_of_children == -1) {
					return redirect('/reservation')->with('message', trans('lang.missing_children'));
				}

				if ($num_of_children > 3) {
					$dbNum = 3;
					$additional = $num_of_children - 3;
					$additional_price = Price::where('children', 0)->where('weekend', intval($day))->where('duration', intval($long))->first()->toArray();
				} else
					$dbNum = $num_of_children;

				$prices = Price::where('children', $dbNum)->get();
//				dd($prices->toArray());
				if ($hourFormer < $hourLatter) {

					for ($i = $hourFormer; $i <= $hourLatter; $i++) {

						if ($i >= 20 || $i <= 7) {
							$p = $prices->where('night', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();

						} else {
							$p = $prices->where('day', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();
						}

						if ((($i == $hourFormer && $checkHalfFormer == 30) || ($i == $hourLatter && $checkHalfLatter == 30)) && $p)
							$price += $p->price / 2;
						else if ($i == $hourLatter && $checkHalfLatter == 00)
							$price += 0;
						else if ($p)
							$price += $p->price;

						$price += $additional * $additional_price['price'];
					}
				} else {
					for ($i = $hourFormer; $i < 24; $i++) {

						if ($i >= 20 || $i <= 7) {
							$p = $prices->where('night', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();

						} else {
							$p = $prices->where('day', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();
						}

						if (($i == $hourFormer && $checkHalfFormer == 30) || ($i == $hourLatter && $checkHalfLatter == 30))
							$price += $p->price / 2;
						else if ($i == $hourLatter && $checkHalfLatter == 00)
							$price += 0;
						else
							$price += $p->price;

						$price += $additional * $additional_price['price'];
					}

					for ($i = 0; $i < $hourLatter; $i++) {

						if ($i >= 20 || $i <= 7) {
							$p = $prices->where('night', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();

						} else {
							$p = $prices->where('day', 1)->where('weekend', intval($day))->where('duration', intval($long))->first();
						}

						if (($i == $hourFormer && $checkHalfFormer == 30) || ($i == $hourLatter && $checkHalfLatter == 30))
							$price += $p->price / 2;
						else if ($i == $hourLatter && $checkHalfLatter == 00)
							$price += 0;
						else
							$price += $p->price;

						$price += $additional * $additional_price['price'];
					}
				}


				//* Calculating agency reimbursment *//
				$user_package = UsedPackage::where('user_id', $this->user->id)->where('active', true)->where('no_of_uses', '!=', 0)->first();
				$reimbursment_time = $hourFormer >= 20 || $hourFormer <= 7 ? 1 : 0;
				if ($user_package) {
					$agency_reimbursment = 0;
				} else {
					if ($additional) {
						$agency_reimbursment = $additional_price['agency_price'];
					} else {

						$agency_reimbursment = $prices->where('children', intval($num_of_children))->where('duration', intval($long))->where('night', $reimbursment_time)->where('weekend', intval($day))->first()['agency_price'];
					}
				}

				$price += $agency_reimbursment;
				//////////

				//* Checking if its holiday *//
				$holidayCheck = date('Y-m-d H:i:s', strtotime(substr($key, 0, 10)));
				$holiday = Holiday::where('date', $holidayCheck)->first();

				if ($holiday) {
					$price = $price + ($price / 2);
				}
				//////////

				$result[$key]['price'] = $price;
				$total += $price;
			}

			$total_info_prepared = array_merge_recursive($request->session()->get('result'), $result);

			foreach ($total_info_prepared as $key => $prep_info) {
				if (!array_key_exists('nannies', $prep_info))
					continue;

				$total_info[$key] = $prep_info;
			}

			if ($request->nannies_chosen) {
				$chosen_nannies = [];

				foreach ($request->nannies_chosen as $key => $nannies) {

					foreach ($nannies as $key2 => $nanny) {
//						$chosen_nannies[$key]['chosen_nannies'][] = Nanny::join('nannies_images', 'nannies.id', '=', 'nannies_images.nanny_id')->where('nannies.id', $key2)->first();
						$chosen_nannies[$key]['chosen_nannies'][] = Availability::select('availability.id AS av_id', 'nannies.name AS name', 'nannies.surname AS surname', 'nannies.id AS nid')->join('nannies', 'nanny_id', '=', 'nannies.id')->where('availability.id', $key2)->first();
					}
				}

				$total_info = array_merge_recursive($total_info, $chosen_nannies);

			}

			$request->session()->put('total_info', $total_info);
			$request->session()->put('total', $total);
		}
		return redirect('/availability/prevoz');
	}

	public function getPrevoz(Request $request)
	{

		if ($request->session()->has('transport')) {
			$request->session()->forget('transport');
			$request->session()->forget('transport_destination');
			$request->session()->forget('transport_origin');
		}

		return view('transport', ['navigation_black' => true]);
	}

	public function postPrevoz(Request $request)
	{

		if ($request->distance) {
			if ($request->distance < 20000)
				$distance_price = 70;
			else {
				$distance_price = (($request->distance - 20000) / 1000) * 4;
			}
			$request->session()->put('transport', $distance_price);
			$request->session()->put('distance', $request->distance);
			$request->session()->put('transport_destination', $request->destination);
			$request->session()->put('transport_origin', $request->origin);
		}
		return response(['status' => 'ok', 'url' => '/payment']);
	}

	public function getNannyInfo(Request $request, $id)
	{
		if (!Auth::check()) {
			$request->session()->put('redirect_me_after', $request->fullUrl());
			return redirect('login');
		}

		$locale = 'hr';
		if ($request->session()->has('locale')) {
			$locale = $request->session()->get('locale');
		}


		$nanny = Nanny::join('nannies_images', 'nannies.id', '=', 'nannies_images.nanny_id')->where('nannies.id', $id)->first();
		return view('nannyInfo', ['nanny' => $nanny, 'locale' => $locale, 'navigation_black' => true]);
	}

}
