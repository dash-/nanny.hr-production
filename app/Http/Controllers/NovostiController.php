<?php
/**
 * Created by PhpStorm.
 * User: damirseremet
 * Date: 22/03/16
 * Time: 21:49
 */

namespace App\Http\Controllers;


use App\Models\News;
use Illuminate\Http\Request;

class NovostiController extends Controller
{
	public function __construct()
	{
		$request = \Request::capture();
		parent::__construct($request);
	}

	public function getIndex(Request $request)
	{
		$novosti = News::orderBy('created_at', 'desc')->paginate(20);
		$language = 'hr';
		if ($request->session()->has('locale'))
			$language = $request->session()->get('locale');

		return view('novosti', [
			'page_title' => 'Novosti',
			'navigation_black' => true,
			'page_title' => 'Novosti i savjeti',
			'language' => $language,
			'novosti' => $novosti
		]);
	}

	public function getNovost(Request $request, $slug)
	{
        if(!$slug) {
            return redirect('/novosti');
        }
        else {
            $news = News::where("slug_en", $slug)->orWhere("slug_hr", $slug)->first();
            if (!$news)
                abort(404);
            $language = 'hr';
            if ($request->session()->has('locale'))
                $language = $request->session()->get('locale');

            if ($news->slug_hr == $slug && $news->slug_en != $slug && !$request->query('jezik')) {
                $language = 'hr';
            } elseif ($news->slug_hr != $slug && $news->slug_en == $slug && !$request->query('jezik')) {
                $language = 'en';
            }

            $title = $language == "en" ? $news->title_en : $news->title_hr;
            $content = $language == "en" ? $news->content_en : $news->content_hr;
            $summary = $language == "en" ? $news->summary_en : $news->summary_hr;

            return view('novost', [
                'page_title' => 'Novosti',
                'content' => $content,
                'image' => $news->image,
                'title' => $title,
                'navigation_black' => true,
                'page_title' => 'Novosti i savjeti | ' . $title,
                'page_description' => $summary, 'novost' => $news
            ]);
	    }
    }
}