<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 6.1.2016
 * Time: 12:39
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoggedController extends Controller
{

    public $user;

    public function __construct()
    {
        $request = Request::capture();
        parent::__construct($request);
        $this->middleware('auth');
        if (Auth::check())
            $this->user = Auth::user();
        else {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                Redirect::to('/login')->send();
            }
        }
    }
}