<?php

namespace App\Http\Controllers;

use App\Contracts\PayWayHelper;
use App\Helpers\EmailHelper;
use App\Helpers\GeneralHelper;
use App\Models\Availability;
use App\Models\Reservation;
use App\Models\ReservationAvailability;
use App\Models\ReservationHistory;
use App\Models\Token;
use App\Models\UsedPackage;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class PaypalPaymentController extends LoggedController
{
	private $_api_context;

	public function __construct(Request $request)
	{
		parent::__construct();
		$paypal_conf = Config::get('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
		$this->_api_context->setConfig($paypal_conf['settings']);

        if(Auth::user()->role == 1 && $request->session()->has('ar_user_id')){
            $user = User::find($request->session()->get('ar_user_id'));
            $this->user = $user;
        }
	}

	public function getIndex(Request $request)
	{
        $transport = 0;
		$details = $request->session()->get('total_info');
		$total = $request->session()->get('total');

        if($request->session()->has('transport')) {
            $total += $request->session()->get('transport');
            $transport = $request->session()->get('transport');
        }

        $monitor_availability = User::find(1);

		if ($total == 0)
			return view('reservation_not_possible', ['details' => $details, 'total' => $total, 'navigation_black' => true]);


        if($request->session()->has('edited_by')) {
            $extended_hours_from = $request->session()->get('extended_hours_from');
            $extended_hours_to = $request->session()->get('extended_hours_to');
            $extended_price = $request->session()->get('extended_price');
            $total_extended_price = $total + $extended_price;

            return view('finalizeEditedReservation', ['extended_hours_from' => $extended_hours_from,'extended_hours_to' => $extended_hours_to, 'extended_price' => $extended_price,'total_extended_price' => $total_extended_price, 'monitor_availability' => $monitor_availability->baby_monitor_availability,'details' => $details, 'total' => $total, 'navigation_black' => true]);
        }
        else {
            return view('finalizeReservation', ['transport' => $transport, 'monitor_availability' => $monitor_availability->baby_monitor_availability,'details' => $details, 'total' => $total, 'navigation_black' => true]);

        }
	}

    public function getInitiateDirectPayment(Request $request) {

        $request->session()->put('payment_type', 'Direktno');

        $discount_token = $request->query('token');
        $camera = $request->query('camera');
        $custom_price = $request->query('customprice');

        if($custom_price != 0) {
            $request->session()->put('total', $custom_price);
        }
        $request->session()->put('discount_token_pw', $discount_token);
        $request->session()->put('camera', $camera);

        return response(['status' => 'ok', 'url' => '/payment/direct-payment']);

    }

    public function getDirectPayment(Request $request) {

        $request->session()->put('payment_type', 'Direktno');
        $discount_token = $request->session()->get('discount_token_pw');
        $camera = $request->session()->get('camera');
        $products = $request->session()->get('total_info');

        $discount = 0;

        if ($discount_token != '') {
            $token = Token::where('token', $discount_token)->first();
            if ($token != null) {
                if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
                    return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
                } else {
                    $discount = $token->discount;
                    $request->session()->put('discount_token_id', $token->id);
                }
            }
        }


        if($request->session()->has('edited_by')){

            $total_price = $request->session()->get('extended_price');
            if ($discount != 0)
                $total_price = $total_price - ($total_price * $discount / 100);

            $reservation_history = new ReservationHistory();
            $reservation_history->reservation_id = $request->session()->get('reservation_id');
            $reservation_history->extended_hours_from = $request->session()->get('extended_hours_from');
            $reservation_history->extended_hours_to = $request->session()->get('extended_hours_to');
            $reservation_history->extended_price = $total_price;
            $reservation_history->edited_by = $request->session()->get('edited_by');
            $reservation_history->save();

            $request->session()->put('reservation_history_id', $reservation_history->id);

        }
        else {

            $userId = $this->user->id;

            $order = new Reservation();
            $order->user_id = $userId;
            $order->save();


            $total_price = $request->session()->get('total');

            //if there is a discount token
            if ($discount != 0)
                $total_price = $total_price - ($total_price * $discount / 100);


            foreach ($products as $key => $info) {
                if ($key == ' token')
                    continue;

                $children = '';

                $reservation_availability = new ReservationAvailability();
                $reservation_availability->reservation_id = $order->id;
                $reservation_availability->availability_id = $info['chosen_nannies'][0]->av_id;
                $reservation_availability->nanny = $info['chosen_nannies'][0]->name . ' ' . $info['chosen_nannies'][0]->surname;
                $reservation_availability->origin_point = ($request->session()->has('transport')) ? $request->session()->get('transport_origin') : '';
                $reservation_availability->destination_point = ($request->session()->has('transport')) ? $request->session()->get('transport_destination') : '';
                $reservation_availability->distance = ($request->session()->has('transport')) ? $request->session()->get('distance') : 0;

                if (isset($info['info']['children'])) {
                    foreach ($info['info']['children'] as $key1 => $child) {
                        $children .= $child . ',';
                    }
                    $children .= $info['info']['other_children'];
                } else
                    $children = $info['info']['other_children'];

                $reservation_availability->children = $children;
                $reservation_availability->address = $info['info']['address_other'];

                $date = date('l,M.j', strtotime(substr($key, 0, 19)));
                $hourFrom = date('G:i', strtotime(substr($key, 0, 19)));
                $hourTo = date('G:i', strtotime(substr($key, 23, 41)));

                $reservation_availability->date = $date;
                $reservation_availability->start = $hourFrom;
                $reservation_availability->end = $hourTo;
                $reservation_availability->price = $info['price'];
                $reservation_availability->save();

                $reservation_history = new ReservationHistory();
                $reservation_history->reservation_id = $order->id;
                $reservation_history->extended_hours_from = $hourFrom;
                $reservation_history->extended_hours_to = $hourTo;
                $reservation_history->extended_price = $info['price'];
                $reservation_history->save();

                $request->session()->put('reservation_history_id', $reservation_history->id);

            }

            //if the camera is selected
            if ($camera == 1) {
                $total_price += 15;
            }

            //if the transport is chosen
            if($request->session()->has('transport'))
                $total_price += $request->session()->get('transport');

            $order->total_price = $total_price;
            $order->save();

            $request->session()->put('total', $total_price);
            $request->session()->put('pid', $order->id);

        }

        $success_url = '/payment/success';
        return view('directPayment',  ['navigation_black' => true, 'url' => $success_url]);
    }

	public function getPayWay(Request $request)
	{
		$discount_token = $request->query('token');
		$camera = $request->query('camera');
        $custom_price = $request->query('customprice');

        if($custom_price != 0) {
            $request->session()->put('total', $custom_price);
        }

		$request->session()->put('discount_token_pw', $discount_token);
		$request->session()->put('camera', $camera);

		return response(['status' => 'ok', 'url' => '/payment/initiate-pay-way']);
	}


	public function getInitiatePayWay(Request $request)
	{
        $request->session()->put('payment_type', 'PayWay');
        $discount_token = $request->session()->get('discount_token_pw');
        $discount = 0;

        if ($discount_token != '') {
            $token = Token::where('token', $discount_token)->first();
            if ($token != null) {
                if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
                    return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
                } else {
                    $discount = $token->discount;
                    $request->session()->put('discount_token_id', $token->id);
                }
            }
        }


        if($request->session()->has('edited_by')){

            $total_price =  $request->session()->get('extended_price');
            if ($discount != 0)
                $total_price = $total_price - ($total_price * $discount / 100);

            $reservation_history = new ReservationHistory();
            $reservation_history->reservation_id = $request->session()->get('reservation_id');
            $reservation_history->extended_hours_from = $request->session()->get('extended_hours_from');
            $reservation_history->extended_hours_to = $request->session()->get('extended_hours_to');
            $reservation_history->extended_price = $total_price;
            $reservation_history->edited_by = $request->session()->get('edited_by');
            $reservation_history->save();

            $info = 'Doplata rezervacije za Nanny.hr';

            $payway = new PayWayHelper($request->session()->get('extended_price'), $info, 'R');
            $payway->pgw_order_id = $reservation_history->id;
            $hash = $payway->generateHash();

            $request->session()->put('reservation_history_id', $reservation_history->id);
        }
        else {

            $camera = $request->session()->get('camera');
            $products = $request->session()->get('total_info');
            $userId = $this->user->id;

            $order = new Reservation();
            $order->user_id = $userId;
            $order->save();


            $total_price = $request->session()->get('total');
            if ($discount != 0)
                $total_price = $total_price - ($total_price * $discount / 100);

            $items = [];

            foreach ($products as $key => $info) {
                if ($key == ' token')
                    continue;

                $children = '';

                $itemPrice = $info['price'] * 0.130546;
                if ($discount != 0)
                    $itemPrice = ($info['price'] - ($info['price'] * $discount / 100)) * 0.130546;

                $order_item_string = GeneralHelper::displayLongDate($key) . '=' . $itemPrice;

                $item[] = $order_item_string;

                $reservation_availability = new ReservationAvailability();
                $reservation_availability->reservation_id = $order->id;
                $reservation_availability->availability_id = $info['chosen_nannies'][0]->av_id;
                $reservation_availability->nanny = $info['chosen_nannies'][0]->name . ' ' . $info['chosen_nannies'][0]->surname;
                $reservation_availability->origin_point = ($request->session()->has('transport')) ? $request->session()->get('transport_origin') : '';
                $reservation_availability->destination_point = ($request->session()->has('transport')) ? $request->session()->get('transport_destination') : '';
                $reservation_availability->distance = ($request->session()->has('transport')) ? $request->session()->get('distance') : 0;

                if (isset($info['info']['children'])) {
                    foreach ($info['info']['children'] as $key1 => $child) {
                        $children .= $child . ',';
                    }
                    $children .= $info['info']['other_children'];
                } else
                    $children = $info['info']['other_children'];

                $reservation_availability->children = $children;
                $reservation_availability->address = $info['info']['address_other'];

                $date = date('l,M.j', strtotime(substr($key, 0, 19)));
                $hourFrom = date('G:i', strtotime(substr($key, 0, 19)));
                $hourTo = date('G:i', strtotime(substr($key, 23, 41)));

                $reservation_availability->date = $date;
                $reservation_availability->start = $hourFrom;
                $reservation_availability->end = $hourTo;
                $reservation_availability->price = $info['price'];
                $reservation_availability->save();

                $reservation_history = new ReservationHistory();
                $reservation_history->reservation_id = $order->id;
                $reservation_history->extended_hours_from = $hourFrom;
                $reservation_history->extended_hours_to = $hourTo;
                $reservation_history->extended_price = $info['price'];
                $reservation_history->save();

                $request->session()->put('reservation_history_id', $reservation_history->id);

                $items[] = $item;
            }

            if ($camera == 1) {
                $order_item =  GeneralHelper::displayLongDate($key) . '=' . '15';
                $items[] = $order_item;
                $total_price += 15;
            }

            if ($request->session()->has('transport')) {
                $order_item_transport =  GeneralHelper::displayLongDate($key) . '=' . $request->session()->get('transport');
                $items[] = $order_item_transport;
                $total_price += $request->session()->get('transport');
            }

            $order->total_price = $total_price;
            $order->save();


            $info = trans('lang.nanny_reservation');

            $request->session()->put('total', $total_price);
            $request->session()->put('pid', $order->id);
            $payway = new PayWayHelper($total_price, $info, 'R');
            $payway->pgw_order_id = $order->id;
            $hash = $payway->generateHash();

            }

		return view('payway', ['payway' => $payway, 'hash' => $hash]);

	}

	public function getPayment(Request $request)
	{
		$details = $request->session()->get('total_info');
		$total = $request->session()->get('total');

		return view('finalizeReservation', ['details' => $details, 'total' => $total, 'navigation_black' => true
		]);
	}

	public function postPayment(Request $request)
	{
		//////////////////PayPal////////////////
        $request->session()->put('payment_type', 'PayPal');
		$discount_token = $request->discount_token;
		$discount = 0;

		if ($discount_token != '') {
			$token = Token::where('token', $discount_token)->first();
			if ($token != null) {
				if (date('Y-m-d H:i:s') > $token->expiration || $token->used == 1) {
					return redirect()->back()->with('message', trans('lang.token_expired_or_not_valid'));
				} else {
					$discount = $token->discount;
					$request->session()->put('discount_token_id', $token->id);
				}
			}
		}

        if($request->session()->has('edited_by')){

            $total_price =  $request->session()->get('extended_price');
            if ($discount != 0)
                $total_price = $total_price - ($total_price * $discount / 100);

            $reservation_history = new ReservationHistory();
            $reservation_history->reservation_id = $request->session()->get('reservation_id');
            $reservation_history->extended_hours_from = $request->session()->get('extended_hours_from');
            $reservation_history->extended_hours_to = $request->session()->get('extended_hours_to');
            $reservation_history->extended_price = $total_price;
            $reservation_history->edited_by = $request->session()->get('edited_by');
            $reservation_history->save();

            $request->session()->put('reservation_history_id', $reservation_history->id);

            $total_price = $total_price * 0.130546;
            $extended = new Item();
            $extended->setName(trans('lang.additional_payment'))
                ->setDescription(trans('lang.additional_payment'))
                ->setCurrency('EUR')
                ->setQuantity(1)
                ->setPrice($total_price);

            $items[] = $extended;

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            // add item to list
            $item_list = new ItemList();
            $item_list->setItems($items);

            $amount = new Amount();
            $amount->setCurrency('EUR')
                ->setTotal($total_price);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription(trans('lang.additional_payment'));

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(url('/payment/payment-status'))
                ->setCancelUrl(url('/payment/payment'));

            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                if (Config::get('app.debug')) {
                    echo "Exception: " . $ex->getMessage() . PHP_EOL;
                    $err_data = json_decode($ex->getData(), true);
                    exit;
                } else {
                    die('Some error occur, sorry for inconvenient');
                }
            }

            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            // add payment ID to session
            $request->session()->put('paypal_payment_id', $payment->getId());

            if (isset($redirect_url)) {
                // redirect to paypal
                return redirect($redirect_url);
            }

            return redirect('/payment/failed')
                ->with('error', 'Unknown error occurred');
        }

        else {

		$products = $request->session()->get('total_info');

		$userId = $this->user->id;

		$order = new Reservation();
		$order->user_id = $userId;
		$order->save();


		$total_price = $request->session()->get('total');
		if ($discount != 0)
			$total_price = $total_price - ($total_price * $discount / 100);

        if($request->custom_price != '')
            $total_price = $request->custom_price;

		$total_quantity = 0;
		$items = [];

		foreach ($products as $key => $info) {
			if ($key == ' token')
				continue;

			$children = '';

            $itemPrice = $info['price'] * 0.130546;
            $description = 'Nanny.hr - '.trans('lang.reservation_for_period').':' . GeneralHelper::displayLongDate($key);

            if($request->custom_price !='') {
                $itemPrice = $total_price * 0.130546;
                $description = 'Nanny.hr - '.trans('lang.discount_price');
            }

			if ($discount != 0)
				$itemPrice = ($info['price'] - ($info['price'] * $discount / 100)) * 0.130546;


			$item = new Item();
			$item->setName('Nanny.hr rezervacija')
				->setDescription($description)
				->setCurrency('EUR')
				->setQuantity(1)
				->setPrice($itemPrice);

			$reservation_availability = new ReservationAvailability();
			$reservation_availability->reservation_id = $order->id;
			$reservation_availability->availability_id = $info['chosen_nannies'][0]->av_id;
			$reservation_availability->nanny = $info['chosen_nannies'][0]->name . ' ' . $info['chosen_nannies'][0]->surname;
            $reservation_availability->origin_point = ($request->session()->has('transport')) ? $request->session()->get('transport_origin') : '';
            $reservation_availability->destination_point = ($request->session()->has('transport')) ? $request->session()->get('transport_destination') : '';
            $reservation_availability->distance = ($request->session()->has('transport')) ? $request->session()->get('distance') : 0;

			if (isset($info['info']['children'])) {
				foreach ($info['info']['children'] as $key1 => $child) {
					$children .= $child . ',';
				}
				$children .= $info['info']['other_children'];
			} else
				$children = $info['info']['other_children'];

			$reservation_availability->children = $children;
			$reservation_availability->address = $info['info']['address_other'];

			$date = date('l,M.j', strtotime(substr($key, 0, 19)));
			$hourFrom = date('G:i', strtotime(substr($key, 0, 19)));
			$hourTo = date('G:i', strtotime(substr($key, 23, 41)));

			$reservation_availability->date = $date;
			$reservation_availability->start = $hourFrom;
			$reservation_availability->end = $hourTo;
			$reservation_availability->price = $info['price'];
			$reservation_availability->save();


            $reservation_history = new ReservationHistory();
            $reservation_history->reservation_id = $order->id;
            $reservation_history->extended_hours_from = $hourFrom;
            $reservation_history->extended_hours_to = $hourTo;
            $reservation_history->extended_price = $info['price'];
            $reservation_history->save();

            $request->session()->put('reservation_history_id', $reservation_history->id);

			$items[] = $item;
			$total_quantity++;
		}

		if ($request->camera == 1) {
            $camera_price = 15.00 * 0.130546;
            $camera = new Item();
			$camera->setName('Baby monitor')
				->setDescription(trans('lang.monitor_rent'))
				->setCurrency('EUR')
				->setQuantity(1)
				->setPrice($camera_price);

			$items[] = $camera;
            $total_price = $total_price * 0.130546 + $camera_price;
			$total_quantity++;
		}
        else if($request->session()->has('transport')){
            $transport_price = $request->session()->get('transport') * 0.130546;
            $transport = new Item();
            $transport->setName(trans('lang.transport'))
                ->setDescription(trans('lang.transport'))
                ->setCurrency('EUR')
                ->setQuantity(1)
                ->setPrice($transport_price);

            $items[] = $transport;
            $total_price = $total_price * 0.130546 + $transport_price;
            $total_quantity++;
        }
        else {
            $total_price = $total_price * 0.130546;

        }

		$order->total_price = $total_price;
		$order->total_quantity = $total_quantity;
		$order->save();

		$request->session()->put('pid', $order->id);
        $request->session()->put('total', $total_price);

		$payer = new Payer();
		$payer->setPaymentMethod('paypal');

		// add item to list
		$item_list = new ItemList();
		$item_list->setItems($items);

		$amount = new Amount();
		$amount->setCurrency('EUR')
			->setTotal($total_price);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($item_list)
			->setDescription(trans('lang.nanny_reservation'));

		$redirect_urls = new RedirectUrls();
		$redirect_urls->setReturnUrl(url('/payment/payment-status'))
			->setCancelUrl(url('/payment/payment'));

		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));
		try {
			$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			if (Config::get('app.debug')) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				$err_data = json_decode($ex->getData(), true);
				exit;
			} else {
				die('Some error occur, sorry for inconvenient');
			}
		}

		foreach ($payment->getLinks() as $link) {
			if ($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}

		// add payment ID to session
		$request->session()->put('paypal_payment_id', $payment->getId());

		if (isset($redirect_url)) {
			// redirect to paypal
			return redirect($redirect_url);
		}

		return redirect('/payment/failed')
			->with('error', 'Unknown error occurred');

        }
	}

	public function getPaymentStatus(Request $request)
	{
		// Get the payment ID before session clear
		$payment_id = $request->session()->get('paypal_payment_id');

		// clear the session payment ID
//        Session::forget('paypal_payment_id');

		if (!$request->query('PayerID') || !$request->query('token')) {
			return redirect('/payment/failed');
		}

		$payment = Payment::get($payment_id, $this->_api_context);

		// PaymentExecution object includes information necessary
		// to execute a PayPal account payment.
		// The payer_id is added to the request query parameters
		// when the user is redirected from paypal back to your site
		$execution = new PaymentExecution();
		$execution->setPayerId($request->input('PayerID'));

		//Execute the payment
		$execution = new PaymentExecution();
		$execution->setPayerId($request->input('PayerID'));

		//Execute the payment

		try {
			$result = $payment->execute($execution, $this->_api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			echo $ex->getCode(); // Prints the Error Code
			$error = json_decode($ex->getData(), true);
			return redirect('/payment/failed')->with('error', $error['message']);
//            var_dump($error['name']);// Prints the detailed error message
//            var_dump($error['message']);
//            die;
		}

//        echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
		if ($result->getState() == 'approved') {
			/*  $order = OrderHistory::where("id","=",$order_id);
			  $order->payment_response = "1";

			  $order->save();*/
            $request->session()->put('currency', '€');
			return redirect('/payment/success');
		}
		return redirect('/payment/failed');
	}

	public function getSuccess(Request $request)
	{
        $payment_type = $request->session()->get('payment_type');
        $payed = ($payment_type == 'Direktno') ? 0 : 1;


        if($request->session()->has('edited_by')) {

            $new_price = $request->session()->get('total') + $request->session()->get('extended_price');

            $reservation_history = ReservationHistory::find($request->session()->get('reservation_history_id'));
            $reservation_history->status = $payed;
            $reservation_history->payment_type = $payment_type;
            $reservation_history->save();

            $edit_reservation = Reservation::find($request->session()->get('reservation_id'));
            $edit_reservation->total_price = $new_price;
            $edit_reservation->save();

            $edit_reservation_availability = ReservationAvailability::find($edit_reservation->reservation_availability->id);
            $edit_reservation_availability->end = $request->session()->get('extended_hours_to');
            $edit_reservation_availability->price = $new_price;
            $edit_reservation_availability->save();
        }
        else {

            $reservation = Reservation::find($request->session()->get('pid'));
            $reservation->payment_type = $payment_type;
            $reservation->status = $payed;
            $reservation->save();


            $reservation_history = ReservationHistory::find($request->session()->get('reservation_history_id'));
            $reservation_history->status = 1;
            $reservation_history->payment_type = $payment_type;
            $reservation_history->save();

            $products = $request->session()->get('total_info');

            foreach ($products as $key => $product) {

                $hourFormer = date('G', strtotime(substr($key, 0, 19)));
                $hourLatter = date('G', strtotime(substr($key, 23, 41)));

                $dateFrom = date('Y-m-d H:i:s', strtotime(substr($key, 0, 19)));
                $dateTo = date('Y-m-d H:i:s', strtotime(substr($key, 23, 41)));


                $availability = Availability::find($product['chosen_nannies'][0]->av_id);
                $availability->isAvailable = false;
                $availability->used_hours = $hourLatter - $hourFormer;
                $availability->save();

                if (((strtotime($dateFrom) - strtotime($availability->from)) / 60 / 60) >= 1) {

                    $formerTimeAvailability = new Availability();
                    $formerTimeAvailability->nanny_id = $availability->nanny_id;
                    $formerTimeAvailability->day_of_week = GeneralHelper::DaniUSedmici(date('l', strtotime($dateFrom)));
                    $formerTimeAvailability->from = $availability->from;
                    $formerTimeAvailability->to = $dateFrom;
                    $formerTimeAvailability->isAvailable = true;
                    $formerTimeAvailability->number_of_hours = (strtotime($dateFrom) - strtotime($availability->from)) / 60 / 60;
                    $formerTimeAvailability->save();
                }

                if (((strtotime($availability->to) - strtotime($dateTo)) / 60 / 60) >= 2) {

                    $addHour = strtotime($dateTo) + 1 * 60 * 60;

                    $latterTimeAvailability = new Availability();
                    $latterTimeAvailability->nanny_id = $availability->nanny_id;
                    $latterTimeAvailability->day_of_week = GeneralHelper::DaniUSedmici(date('l', strtotime($dateFrom)));
                    $latterTimeAvailability->from = date('Y-m-d H:i:s', $addHour);
                    $latterTimeAvailability->to = $availability->to;
                    $latterTimeAvailability->isAvailable = true;
                    $latterTimeAvailability->number_of_hours = (strtotime($availability->to) - $addHour) / 60 / 60;
                    $latterTimeAvailability->save();
                }
            }

            $token = Token::find($request->session()->get('discount_token_id'));
            if (isset($token) && $token != null) {
                if ($token->type == 0) {
                    $token->update(['used' => 1]);
                }
            }

            $user_package = UsedPackage::where('user_id', $this->user->id)->where('active', true)->where('no_of_uses', '!=', 0)->first();
            if($user_package) {
                $no_of_uses = $user_package->no_of_uses - 1;
                UsedPackage::find($user_package->id)->update(['no_of_uses' => $no_of_uses]);
            }

            $data = $request->session()->get('total_info');
            $total = $request->session()->get('total');

            $currency = ($request->session()->has('currency') ? $request->session()->get('currency') : 'kn');

            $emailDataUser = ['subject' => trans('lang.reservation_confirmation'), 'to' => $this->user->email];
            EmailHelper::sendEmail('emails.confirm_reservation_user', ['data' => $data, 'total' => $total, 'currency' => $currency], $emailDataUser);

            $family = $this->user->full_name();

	        $emailDataNanny = ['subject' => trans('lang.reservation_notification_family') . ' ' . $this->user->full_name(), 'to' => 'info@nanny.hr'];
            EmailHelper::sendEmail('emails.confirm_reservation_nanny', ['data' => $data, 'total' => $total, 'family' => $family, 'currency' => $request->session()->get('currency')], $emailDataNanny);

            $request->session()->forget('total_info');
            $request->session()->forget('data');
            $request->session()->forget('result');
            $request->session()->forget('total');
            $request->session()->forget('currency');

        }
		return view('paymentSuccess', [
			'navigation_black' => true
		]);
	}

	public function getFailed(Request $request)
	{
        if($request->session()->has('edited_by')){
            $reservation_history = ReservationHistory::find($request->session()->get('reservation_history_id'));
            $reservation_history->delete();
        }
        else {
            $availabilities = ReservationAvailability::where('reservation_id', $request->session()->get('pid'))->get();
            foreach ($availabilities as $avail) {
                $avail->delete();
            }
            $reservation = Reservation::find($request->session()->get('pid'));
            if ($reservation)
                $reservation->delete();

            $request->session()->forget('total_info');
            $request->session()->forget('data');
            $request->session()->forget('result');
            $request->session()->forget('total');
            $request->session()->forget('currency');
        }
		return view('paymentFailed', [
			'navigation_black' => true
		]);
	}


}