<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 10.5.2016
 * Time: 17:22
 */

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Models\Family;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransportController extends Controller {

    public function __construct() {
        $request = Request::capture();
        parent::__construct($request);
        $this->user = Auth::user();
    }

    public function getIndex() {
        if(Auth::user()->role != 1)
            return redirect('/transport/transport');

        $users = Family::where('id', '!=', 1)->paginate(20);
        return view('adminReservationTransport', ['users' => $users, 'navigation_black' => true]);
    }

    public function getTransport(Request $request, $id = null) {

        if($id != null)  {
            $request->session()->put('ar_user_id', $id);
        }

        if($request->session()->has('order_transport')) {
            $request->session()->forget('order_transport');
            $request->session()->forget('transport_destination');
            $request->session()->forget('transport_origin');
        }

        return view('order_transportation', ['navigation_black' => true]);
    }

    public function postTransport(Request $request) {

        if($request->date < date('d.m.Y H:i'))
            return response(['status' => 'failed', 'lang' => $request->session()->get('locale')]);

        if($request->distance) {
            if($request->distance < 20000)
                $distance_price = 70;
            else {
                if(($request->distance - 20000) > 1000)
                    $distance_price = 70 + (round(($request->distance - 20000) / 1000) * 4);
                else
                    $distance_price = 70;
            }

            $request->session()->put('transport', $distance_price);
            $request->session()->put('distance', $request->distance);
            $request->session()->put('transport_destination', $request->destination);
            $request->session()->put('transport_origin', $request->origin);
            $request->session()->put('transport_date', GeneralHelper::displayDateTransport($request->date));
        }

        return response(['status' => 'ok', 'url' => '/mpayment']);
    }
}