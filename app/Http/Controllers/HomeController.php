<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 18.12.2015
 * Time: 17:59
 */

namespace App\Http\Controllers;


use App\Helpers\EmailHelper;
use App\Models\Content;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function getIndex()
    {
        return view('home');
    }

    public function getOnama()
    {
        return view('about', ['navigation_black' => true, 'page_title' => 'O nama',

            'page_description' => 'Pri pružanju usluga povremenog čuvanja djece, povezuje roditelje s čuvalicama, povremeno brinula o djetetu, standarde u odabiru čuvalica'
        ]);
    }

    public function getKakoRezerviratiCuvalicu()
    {
        return view('howto', ['navigation_black' => true,
            'page_title' => 'Kako rezervirati čuvalicu',
            'page_description' => 'Vidi čuvalicu, prijavi, registriraj se, odaberite termin, kontaktirajte nas za višestruke termine, odaberite željenu čuvalicu, provjeriti dostupnost čuvalice, plaćanje se obavlja izravnim uplatama, kreditnim i debiting karticama'
        ]);
    }

    public function getCjenik(Request $request)
    {
        $language = 'hr';
        if($request->session()->has('locale'))
            $language = $request->session()->get('locale');

        $content = Content::whereSlug('cjenik')->where('language', $language)->first();
        return view('prices', [
            'content' => $content,
            'navigation_black' => true,
            'page_title' => 'Cjenik',
            'page_description' => 'Naknada za čuvalicu, čuvanje u noćnim satima, čuvanje većeg broja djece, prijevoz djece'
        ]);
    }

    public function getZelimBitiCuvalica()
    {
        $model = new Request();
        return view('nannyApplication', [
            'model' => $model,
            'navigation_black' => true,
            'page_title' => 'Želim biti čuvalica'
        ]);
    }

    public function getKontakt()
    {
        $model = new Request();

        return view('contact', ['model' => $model, 'navigation_black' => true, 'page_title' => 'Kontakt',
            'page_description' => 'Koristili čuvalice, angažiranja čuvalica, Nanny.hr pravi izbor za Vas, Vaše mišljenje'
        ]);
    }

    public function postKontakt(Request $request)
    {

        $rules = [];
        $rules_message = [];
        if (!$request->footer) {
            $rules['g-recaptcha-response'] = 'required|captcha';
            $rules_message['g-recaptcha-response.required'] = 'Potvdite da niste robot';
            $rules_message['g-recaptcha-response.captcha'] = 'Potvdite da niste robot';
        }


        $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required',
            ] + $rules, [
                'name.required' => trans('lang.full_name_required'),
                'email.required' => trans('lang.email_required'),
                'email.email' => trans('lang.email_required'),
                'phone.required' => trans('lang.phone_required'),
                'message.required' => trans('lang.message_required')
            ] + $rules_message);

        $mail = 'info@nanny.hr';

        $emailData = ['subject' => 'Kontakt od ' . $request->name, 'to' => $mail];
        EmailHelper::sendEmail('emails.contact', ['name' => $request->name, 'phone' => $request->phone, 'email' => $request->email, 'message1' => $request->message], $emailData);

        return redirect()->back();
    }

    public function postZelimBitiCuvalica(Request $request)
    {

        $legal_years = date('d.m.Y', strtotime('-18 years, -1 day'));

        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^\+?[^a-zA-Z]{5,}$/',
            'dob' => 'required|before:' . $legal_years
        ], [
            'name.required' => trans('lang.full_name_required'),
            'surname.required' => trans('lang.full_name_required'),
            'email.required' => trans('lang.email_required'),
            'email.email' => trans('lang.email_required'),
            'phone.required' => trans('lang.phone_required'),
            'dob.required' => trans('lang.dob_required'),
            'dob.before' => trans('lang.nanny_of_age'),
        ]);

        $mail = 'info@nanny.hr';

        $emailData = ['subject' => 'Aplikacija za cuvalicu od ' . $request->name . " " . $request->surname, 'to' => $mail];
        EmailHelper::sendEmail('emails.nanny_application', ['name' => $request->name, 'surname' => $request->surname, 'phone' => $request->phone, 'email' => $request->email, 'experience' => $request->experience, 'education' => $request->education, 'dob' => $request->dob, 'ip' => $request->ip()], $emailData);

        return redirect()->back();
    }

    public function postQuickContact(Request $request)
    {
//        var_dump($request['message']);die;

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^\+?[^a-zA-Z]{5,}$/',
        ]);

        $mail = 'info@nanny.hr';
        $emailData = ['subject' => 'Brzi kontakt od ' . $request['name'], 'to' => $mail];
        EmailHelper::sendEmail('emails.quick_contact', ['name' => $request['name'], 'email' => $request['email'], 'phone' => $request['phone'], 'message1' => $request['message'], 'ip' => $request->ip()], $emailData);

        return redirect()->back();
    }

    public function getOpciUvjeti()
    {
        return view('termsOfUse', ['navigation_black' => true]);
    }

    public function getError404()
    {
        return view('error', ['navigation_black' => true]);
    }
}