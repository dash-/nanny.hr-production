<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 11.1.2016
 * Time: 17:09
 */

namespace App\Http\Controllers;

use App\Models\Child;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ReservationController extends LoggedController
{

	public function __construct(Request $request)
	{
		parent::__construct();
        if($this->user->role != 1){
            if ($this->user->completed == 0)
                Redirect::to('/register-step1')->with('message', trans('lang.finish_registration'))->send();
            else if ($this->user->completed == 1)
                Redirect::to('/register-step2')->with('message', trans('lang.finish_registration'))->send();
            else if ($this->user->completed == 2)
                Redirect::to('/register-step3')->with('message', trans('lang.finish_registration'))->send();
            else if ($this->user->completed == 3)
                Redirect::to('/register-step4')->with('message', trans('lang.finish_registration'))->send();
        }
        else if(Auth::user()->role == 1 && $request->session()->has('ar_user_id')) {
            $user = User::find($request->session()->get('ar_user_id'));
            $this->user = $user;
        }

//		if ($this->user->role == 1)
//			Redirect::to('/')->send();
	}

	public function getIndex(Request $request)
	{
		$children = Child::where('family_id', $this->user->family->id)->get();
        $address = $this->user->address();
		$times = $request->session()->get('data');
		return view('reservationDetails', ['address' => $address,'children' => $children, 'times' => $times, 'navigation_black' => true
		]);
	}

	public function postIndex(Request $request)
	{
		$newArray = [];
		foreach ($request->all() as $key => $value) {
			if ($key == '_token')
				continue;

			$newArray[str_replace('_', ' ', $key)]['info'] = $value;
		}

		$result = array_merge_recursive($request->session()->get('data'), $newArray);

		$request->session()->put('result', $result);
		return redirect('/availability/available-nannies');
	}

//    public function getFinalizeReservation(Request $request) {
//        $details = $request->session()->get('total_info');
//        $total = $request->session()->get('total');
//        return view('finalizeReservation', ['details' => $details, 'total' => $total]);
//    }

}