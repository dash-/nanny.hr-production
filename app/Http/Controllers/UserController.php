<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 29.10.2015
 * Time: 13:11
 */
namespace App\Http\Controllers;


use App\Models\Availability;
use App\Models\Child;
use App\Models\Family;
use App\Models\FamilyParent;
use App\Models\Reservation;
use App\Models\ReservationAvailability;
use App\Models\TransportReservations;
use App\Models\UsedPackage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Debug\Debug;

class UserController extends LoggedController
{

	public function __construct(Request $request)
	{
		parent::__construct($request);
		if ($this->user->role != 1) {
			if ($this->user->completed == 0)
				Redirect::to('/register-step1')->send();
			else if ($this->user->completed == 1)
				Redirect::to('/register-step2')->send();
			else if ($this->user->completed == 2)
				Redirect::to('/register-step3')->send();
			else if ($this->user->completed == 3)
				Redirect::to('/register-step4')->send();
		} else if (Auth::user()->role == 1 && $request->session()->has('impersonate_user_id')) {
			$user = User::find($request->session()->get('impersonate_user_id'));
			$this->user = $user;
		}

	}

	public function getIndex()
	{
		$package = UsedPackage::where('user_id', $this->user->id)->where('no_of_uses', '!=', 0)->where('active', false)->first();
		$active_package = UsedPackage::where('user_id', $this->user->id)->where('no_of_uses', '!=', 0)->where('active', true)->first();

		return view('userPanel', ['package' => $package, 'active_package' => $active_package, 'navigation_black' => true]);
	}

	public function getRezervacije()
	{
		$reservations = Reservation::where('user_id', $this->user->id)->orderBy('created_at', 'desc')->paginate(10);
		return view('rezervacije', [
			'reservations' => $reservations,
			'navigation_black' => true
		]);
	}

	public function getRezervacija($id)
	{
		$reservationAvailabilities = ReservationAvailability::where('reservation_id', $id)->get();
		return view('rezervacija', ['reservationAvailabilities' => $reservationAvailabilities,
			'navigation_black' => true]);
	}

	public function getEditFamilyInfo()
	{
		$family = Family::where('user_id', $this->user->id)->first();
		return view('editFamilyInfo', [
			'family' => $family,
			'email' => $this->user->email,
			'navigation_black' => true
		]);
	}

	public function postEditFamilyInfo(Request $request)
	{
		$this->validate($request, [
			'family_name' => 'required',
			'email' => 'required|unique:users,email,' . $this->user->id,
			'phone' => 'required',
			'address' => 'required',
			'address_number' => 'required',
			'state' => 'required',
			'city' => 'required',
			'zip_code' => 'required',
			'pets' => 'required'
		]);

		$family = Family::where('user_id', $this->user->id)->first();

		Family::find($family->id)->update($request->except('email'));
//        $familyUpdate->family_name = $request['family_name'];
//        $familyUpdate->phone = $request['phone'];
//        $familyUpdate->address = $request['address'];
//        $familyUpdate->state = $request['state'];
//        $familyUpdate->city = $request['city'];
//        $familyUpdate->zip_code = $request['zip_code'];
//        $familyUpdate->pets = $request['pets'];
//        $familyUpdate->update();

		User::where('id', $this->user->id)->update(['email' => $request['email']]);

		return redirect()->back();
	}

	public function getEditParents()
	{

		$family = Family::where('user_id', $this->user->id)->first();
		$parents = FamilyParent::where('family_id', $family->id)->get();
		return view('editParents', [
			'parents' => $parents,
			'navigation_black' => true

		]);
	}

	public function getEditParentInfo($id)
	{

		$parent = FamilyParent::where('id', $id)->first();
		return view('editIndividualParent', [
			'parent' => $parent,
			'navigation_black' => true
		]);
	}

	public function postEditParentInfo(Request $request, $parent_id)
	{
		FamilyParent::find($parent_id)->update($request->input());
		return redirect('/user/edit-parents');
	}

	public function getAddParent()
	{
		return view('addParents', ['navigation_black' => true]);
	}

	public function postAddParent(Request $request)
	{
//        var_dump($request->all());die;

//        $countTotal = count(FamilyParent::where('family_id', $this->user->id)->get()->toArray());
//        $countParents = count(FamilyParent::where('family_id', $this->user->id)->where('parent_type', 1)->get()->toArray());
//        $countEmergency = count(FamilyParent::where('family_id', $this->user->id)->where('parent_type', 2)->get()->toArray());
//
//        if($countTotal == 3)
//            return redirect()->back()->with('message', 'Samo 2 roditelja i jedan kontakt za hitne slucajeve mogu biti aktivni.');
//        else{

		$family = Family::where('user_id', $this->user->id)->first();
		FamilyParent::create($request->input() + ['family_id' => $family->id]);
		return redirect('/user/edit-parents');
//        }
	}

	public function getDeleteParent($id)
	{

		$parent = FamilyParent::where('id', $id)->first();
		$family = Family::where('user_id', $this->user->id)->first();

		if ($parent->family_id == $family->id) {

			$countParents = FamilyParent::where('family_id', $family->id)->where('parent_type', 1)->get();
			if ($countParents->count() == 1)
				return redirect()->back()->with('errors', trans('lang.minimum_one_contact'));

			FamilyParent::find($id)->delete();
			return redirect('/user/edit-parents');
		}
	}

	public function getEditChildrenInfo()
	{
		$family = Family::where('user_id', $this->user->id)->first();
		$children = Child::where('family_id', $family->id)->get();

		return view('editChildrenInfo', [
			'children' => $children,
			'navigation_black' => true

		]);
	}

	public function getEditIndividualChild($id)
	{
		$family = Family::where('user_id', $this->user->id)->first();
		$child = Child::where('id', $id)->where('family_id', $family->id)->first();

		return view('editIndividualChild', array(
			'child' => $child,
			'navigation_black' => true
		));
	}

	public function postEditIndividualChild(Request $request, $id)
	{

		$legal_years = date('d.m.Y', strtotime('-1 day'));

		$this->validate($request, [
			'name' => 'required',
			'surname' => 'required',
			'sex' => 'required',
			'dob' => 'required|before:' . $legal_years,
//            'children[child1][allergies]' => 'required',
//            'children[child1][special_note]'   => 'required',
		], [
			'name.required' => trans('lang.child_name_required'),
			'surname.required' => trans('lang.child_surname_required'),
			'sex.required' => trans('lang.child_gender_required'),
			'dob.required' => trans('lang.child_dob_required'),
			'dob.before' => trans('lang.child_dob_valid')
		]);

		$child = Child::find($id);
		$child->dob = date('Y-m-d H:i:s', strtotime($request->dob));
		$child->name = $request->name;
		$child->surname = $request->surname;
		$child->sex = $request->sex;
		$child->allergies = $request->allergies;
		$child->allergies_info = $request->allergies_info;
		$child->special_note = $request->special_note;
		$child->save();

		return redirect('/user/edit-children-info/' . $child->family_id);
	}

	public function getAddChildren()
	{
		return view('addChildren', ['navigation_black' => true]);
	}

	public function postAddChildren(Request $request)
	{
		$family = Family::where('user_id', $this->user->id)->first();
		Child::create($request->input() + ['family_id' => $family->id]);
		return redirect('/user/edit-children-info');
	}

	public function getDeleteChild($id)
	{
		Child::find($id)->delete();
		return redirect()->back();
	}

	public function getAktivirajPaket($id)
	{
		UsedPackage::where('id', $id)->update(['active' => true]);
		return redirect()->back();
	}

    public function getPrevozi() {
        $transports = TransportReservations::where('user_id', $this->user->id)->orderBy('created_at', 'desc')->paginate(20);
        return view('prevoz', ['navigation_black' => true, 'transports' => $transports]);
    }
}