<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Artisan;

class ArtisanController extends Controller
{
    public function gitpull()
    {
        echo Artisan::call("git:pull");
        echo "<br>";
        echo "Tnx.";
    }
}