<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request) {

        if($request->query('jezik')){
            Session::put("locale",$request->query('jezik'));
            App::setLocale(Session::get("locale"));
        }

        if(Session::has("locale")){
            App::setLocale(Session::get("locale"));
        }

        $agent = new Agent();
        $browser = $agent->browser();

        if($browser == 'IE' && $agent->version($browser) < 11) {
            Redirect::to('/browser-incompatible')->send();
        }
    }
}
