<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 13.11.2015
 * Time: 11:23
 */

namespace App\Http\Controllers;

use App\Helpers\EmailHelper;
use App\Models\Child;
use App\Models\Family;
use App\Models\FamilyParent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends LoggedController
{
	public function __construct()
	{
		parent::__construct();
	}

//    public function getIndex()
//    {
//    }

	public function getRegisterStep1()
	{
		$user = \Auth::user();
		if ($this->user->completed == 0)
			return view('register', ['user' => $user]);
		else if ($this->user->completed == 1)
			return redirect('/register-step2');
		else if ($this->user->completed == 2)
			return redirect('/register-step3');
		else if ($this->user->completed == 3)
			return redirect('/user');
	}

	public function postRegisterStep1(Request $request)
	{
		$rules = [];
		$rules_messages = [];
		if ($request->pets) {
			$rules['pets_info'] = 'required';
			$rules_messages['pets_info.required'] = trans('lang.pets_required');
		}
		$this->validate($request, [
			'phone' => 'required',
			'address' => 'required',
			'address_number' => 'required',
			'state' => 'required',
			'city' => 'required',
			'zip_code' => 'required',
			'pets' => 'required',
			] + $rules,
			[
				'phone.required' => trans('lang.phone_required'),
				'address.required' => trans('lang.address_required'),
				'address_number.required' => trans('lang.address_number_required'),
				'state.required' => trans('lang.state_required'),
				'city.required' => trans('lang.city_required'),
				'zip_code.required' => trans('lang.zip_code_required'),
				'pets.required' => trans('lang.pets_required'),
			] + $rules_messages);

		$family = Family::where('user_id', $this->user->id)->first();
		Family::find($family->id)->update($request->input());

		User::where('id', $this->user->id)->update(['completed' => 1]);

		return redirect('/register-step2');
	}

	public function getRegisterStep2()
	{
		$user = \Auth::user();
		if ($this->user->completed == 0)
			return redirect('/register-step1');
		else if ($this->user->completed == 1)
			return view('register_S2', ['user' => $user]);
		else if ($this->user->completed == 2)
			return redirect('/register-step3');
		else if ($this->user->completed == 3)
			return redirect('/user');
	}

	public function postRegisterStep2(Request $request)
	{
		$legal_years = date('d.m.Y', strtotime('-18 years, -1 day'));

		$this->validate($request, [
			'parent_1_name' => 'required',
			'parent_1_surname' => 'required',
			'jmbg' => 'required',
			'dob' => 'required|before:' . $legal_years,
			'parent_1_email' => 'required|email',
			'parent_1_phone' => 'required',
			'parent_1_sex' => 'required'
		], [
			'parent_1_name.required' => trans('lang.parent_name_required'),
			'parent_1_surname.required' => trans('lang.parent_surname_required'),
			'jmbg.required' => trans('lang.parent_oib_required'),
			'dob.required' => trans('lang.parent_dob_required'),
			'dob.before' => trans('lang.parent_of_age'),
			'parent_1_email.required' => trans('lang.email_required'),
			'parent_1_email.email' => trans('lang.email_required'),
			'parent_1_phone.required' => trans('lang.phone_required'),
			'parent_1_sex.required' => trans('lang.gender_required')
		]);

		$family = Family::where('user_id', $this->user->id)->first();

		$parent1 = new FamilyParent();
		$parent1->family_id = $family->id;
		$parent1->parent_type = 1;
		$parent1->name = $request->parent_1_name;
		$parent1->surname = $request->parent_1_surname;
		$parent1->dob = $request->dob;
		$parent1->jmbg = $request->jmbg;
		$parent1->email = $request->parent_1_email;
		$parent1->phone = $request->parent_1_phone;
		$parent1->sex = $request->parent_1_sex;
		$parent1->save();


		User::where('id', $this->user->id)->update(['completed' => 2]);

		return redirect('/register-step3');
	}

	public function getRegisterStep3()
	{
		$user = \Auth::user();
		if ($this->user->completed == 0)
			return redirect('/register-step1');
		else if ($this->user->completed == 1)
			return redirect('/register-step2');
		else if ($this->user->completed == 2)
			return view('register_S3', ['user' => $user]);
		else if ($this->user->completed == 3)
			return redirect('/user');
	}

	public function postRegisterStep3(Request $request)
	{
		$this->validate($request, [
			'emergency_contact_name' => 'required',
			'emergency_contact_surname' => 'required',
			'emergency_contact_phone' => 'required',
			'emergency_contact_relation' => 'required',
		], [
			'emergency_contact_name.required' => trans('lang.emergency_name_required'),
			'emergency_contact_surname.required' => trans('lang.emergency_surname_required'),
			'emergency_contact_phone.required' => trans('lang.emergency_phone_required'),
			'emergency_contact_relation.required' => trans('lang.emergency_relation_required'),
		]);

		$family = Family::where('user_id', $this->user->id)->first();

		$emergencyPerson = new FamilyParent();
		$emergencyPerson->family_id = $family->id;
		$emergencyPerson->parent_type = 2;
		$emergencyPerson->name = $request->emergency_contact_name;
		$emergencyPerson->surname = $request->emergency_contact_surname;
		$emergencyPerson->phone = $request->emergency_contact_phone;
		$emergencyPerson->relation = $request->emergency_contact_relation;
		$emergencyPerson->save();

		User::where('id', $this->user->id)->update(['completed' => 3]);

		return redirect('/register-step4');
	}

	public function getRegisterStep4()
	{
		$family = Family::where('user_id', $this->user->id)->first();

		$child = Child::where('family_id', $family->id)->first();

		if ($this->user->completed == 0)
			return redirect('/register-step1');
		else if ($this->user->completed == 1)
			return redirect('/register-step2');
		else if ($this->user->completed == 2)
			return redirect('/register-step3');
		else if ($this->user->completed == 3)
			return view('register_S4', ['child' => $child]);
		else if ($this->user->completed == 4)
			return redirect('/user');
	}

	public function postRegisterStep4(Request $request)
	{
		$legal_years = date('d.m.Y', strtotime('-1 day'));

		$this->validate($request, [
			'name' => 'required',
			'surname' => 'required',
			'sex' => 'required',
			'dob' => 'required|before:' . $legal_years,
//            'children[child1][allergies]' => 'required',
//            'children[child1][special_note]'   => 'required',
		], [
			'name.required' => trans('lang.child_name_required'),
			'surname.required' => trans('lang.child_surname_required'),
			'sex.required' => trans('lang.child_gender_required'),
			'dob.required' => trans('lang.child_dob_required'),
			'dob.before' => trans('lang.child_dob_valid')
		]);
//dd($request->all());
		$family = Family::where('user_id', $this->user->id)->first();

//        foreach ($request['children'] as $childin) {
		$child = new Child();
		$child->family_id = $family->id;
		$child->name = $request->name;
		$child->surname = $request->surname;
		$child->dob = date('Y-m-d H:i:s', strtotime($request->dob));
		$child->sex = $request->sex;
		$child->allergies = (array_key_exists('allergies', $request->all())) ? $request->allergies : 0;
		$child->special_note = (array_key_exists('special_note', $request->all())) ? $request->special_note : 'N/A';
		$child->save();
//        }

		User::where('id', $this->user->id)->update(['completed' => 4]);

		$mail = 'info@nanny.hr';
		$emailData = ['subject' => 'Nova registracija na Nanny.hr', 'to' => $mail];

		EmailHelper::sendEmail('emails.notify_admin_of_registration', ['name' => $family->family_name, 'phone' => $family->phone, 'address' => $family->address . '' . $family->address_number, 'city' => $family->city], $emailData);

        $welcomingEmail = $this->user->email;
        $welcomingEmailData = ['subject' => trans('lang.welcome_nanny'), 'to' => $welcomingEmail];
        EmailHelper::sendEmail('emails.notify_user_of_registration', [], $welcomingEmailData);



		return redirect('/user')->with('message', trans('lang.registration_successful'));


	}
}