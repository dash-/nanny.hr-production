<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\URL;
use Validator;
use App\Models\Child;
use App\Models\Family;
use App\Models\FamilyParent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use JeroenDesloovere\Geolocation\Geolocation;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Helpers\EmailHelper;

class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$request = Request::capture();
		parent::__construct($request);
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return User
	 */
	protected function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}

	/**
	 * Registration HTML page
	 *
	 * @return \Illuminate\View\View
	 */
	public function getLogin()
	{

		$user = new User();
		return view('login', [
			'user' => $user,
			'page_title' => 'Prijava'
		]);
	}

	public function getRegister()
	{

		$user = new User();
		return view('register_S0', [
			'user' => $user,
			'page_title' => 'Registracija'
		]);
	}

	public function postRegister(Request $request)
	{

		$this->validate($request, [
			'family' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|password',
			'terms' => 'required',
		]);
//        if(User::where('email',$request->email)->first()) {
//            return redirect()->back()->withErrors(['email' => 'User with that email already exists']);
//        }

		$user = new User();
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->role = 2;
		$user->token = bcrypt($request->email . '' . $request->password);
		$user->save();

		//TODO: ispraviti u view da su imena inptua ista kao kolone pa uraditi npr Family::create($request->input())
		$family = new Family();
		$family->user_id = $user->id;
		$family->family_name = $request->family;
//            $family->phone = $request->phone;
//            $family->pets = $request->pets;
//            $family->address = $request->address;
//            $family->city = $request->address_city;
//            $family->state = $request->address_state;
//            $family->zip_code = $request->address_zip;
		$family->save();

//            return redirect('/register-step2/'.$family->id);
//            return redirect('/#login')->with('message', 'Uspješno ste registrovani');
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
			return redirect('/user')->with('message', trans('lang.login_successful'));
	}

//    public function getRegisterStep2 ($id) {
//        $user = new User();
//        return view('register_S2', [
//            'user' => $user,
//        ]);
//    }
//
//    public function postRegisterStep2 (Request $request, $id) {
//
//        $this->validate($request, [
//            'parent_1_name'    => 'required',
//            'parent_1_surname'     => 'required',
//            'parent_1_email'  => 'required',
//            'parent_1_phone'     => 'required',
//            'parent_1_sex' => 'required',
//            'emergency_contact_name'   => 'required',
//            'emergency_contact_surname'   => 'required',
//            'emergency_contact_phone'   => 'required',
//            'emergency_contact_relation'   => 'required',
//        ]);
//
//        $parent1 = new FamilyParent();
//        $parent1->family_id = $id;
//        $parent1->parent_type = 1;
//        $parent1->name = $request->parent_1_name;
//        $parent1->surname = $request->parent_1_surname;
//        $parent1->email = $request->parent_1_email;
//        $parent1->phone = $request->parent_1_phone;
//        $parent1->sex = $request->parent_1_sex;
//
//        $parent2 = new FamilyParent();
//        $parent2->family_id = $id;
//        $parent2->parent_type = 1;
//        $parent2->name = $request->parent_2_name;
//        $parent2->surname = $request->parent_2_surname;
//        $parent2->email = $request->parent_2_email;
//        $parent2->phone = $request->parent_2_phone;
//        $parent2->sex = $request->parent_2_sex;
//
//        $emergencyPerson = new FamilyParent();
//        $emergencyPerson->family_id = $id;
//        $emergencyPerson->parent_type = 2;
//        $emergencyPerson->name = $request->emergency_contact_name;
//        $emergencyPerson->surname = $request->emergency_contact_surname;
//        $emergencyPerson->phone = $request->emergency_contact_phone;
//        $emergencyPerson->relation = $request->emergency_contact_relation;
//
//        $parent1->save();
//        $parent2->save();
//        $emergencyPerson->save();
//
//        return redirect('/register-step3/'. $id);
//    }
//
//    public function getRegisterStep3 ($id) {
//        $user = new User();
//        return view('register_S3', [
//            'user' => $user,
//        ]);
//    }
//
//    public function postRegisterStep3 (Request $request, $id) {
////        $this->validate($request, [
////            'children[child1][name]'    => 'required',
////            'children[child1][surname]'     => 'required',
////            'children[child1][dob]'  => 'required',
////            'children[child1][sex]'     => 'required',
////            'children[child1][allergies]' => 'required',
////            'children[child1][special_note]'   => 'required',
////        ]);
//        foreach($request['children'] as $childin) {
//            $child = new Child();
//            $child->family_id = $id;
//            $child->name = $childin['name'];
//            $child->surname = $childin['surname'];
//            $child->dob = $childin['dob'];
//            $child->sex = $childin['sex'];
//            $child->allergies = $childin['allergies'];
//            $child->special_note = $childin['special_note'];
//            $child->save();
//        }
//
//        return redirect('/');
//
//    }
//     public function getLogin() {
////         $user = new User();
////
//         return view('login');
//     }

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email',
			'password' => 'required'
		], [
			'email.email' => trans('lang.email_required'),
			'email.required' => trans('lang.email_required'),
			'password.required' => trans('lang.password_required'),
		]);


		$loginCredentials = ['email' => $request->email, 'password' => $request->password];
		$user = User::where('email', $request->email)->first();

		if ($user && $user->deleted_flag == 1) {
			return redirect('/login')->with('errors', trans('lang.account_disabled'));
		}

		if (Auth::attempt($loginCredentials, true)) {
			$redirect_me_after = $request->session()->pull('redirect_me_after', '');
			if ($redirect_me_after)
				return redirect($redirect_me_after)->with('message', trans('lang.login_successful'));

			return redirect('/user')->with('message', trans('lang.login_successful'));
		}

		return redirect('/login')
			->withInput($request->all())
			->withErrors([
				'email' => trans('lang.wrong_credentials'),
			]);
	}

	public function getLogout()
	{
		Auth::logout();
		return redirect('/');
	}

	public function getForgotPassword()
	{
		return view('forgot_password', [
			'page_title' => 'Zaboravljena zaporka'
		]);
	}

	public function postForgotPassword(Request $request)
	{

		if (User::where('email', $request->email)->first() && !Auth::check()) {

			$user = User::where('email', $request->email)->first();

			$updateTokenExpiration = User::find($user->id);
			$updateTokenExpiration->expire = date('Y-m-d H:i:s');
			$updateTokenExpiration->save();

			$emailData = ['subject' => 'Nova zaporka za Nanny.hr / Link to reset your password for Nanny.hr', 'to' => $request->email];
			EmailHelper::queueEmail('emails.password_reset', ['reset_link' => URL::to('/reset-password?token=' . $user->token)], $emailData);

			return redirect()->back()->with('message', trans('lang.email_change_password'));
		} else {
			return view('forgot_password');
		}
	}

	public function getResetPassword(Request $request)
	{
		if (User::where('token', $request['token'])->first()) {
			$user = User::where('token', $request['token'])->first();
			//validation of password is 1 day
			if ((strtotime(date('Y-m-d H:i:s')) - strtotime($user->expire)) / (60 * 60 * 24) <= 1)
				return view('reset_password', ['id' => $user->id, 'navigation_black' => TRUE]);
			else
				return redirect('/#login')->with('message', trans('lang.token_expired'));
		}
	}

	public function postResetPassword(Request $request)
	{

		$user = User::find($request->id);
		$user->password = bcrypt($request->password);
		$user->token = bcrypt($user->email . '' . $request->password);
		$user->save();

		return redirect('/')->with('message', trans('lang.password_changed'));
	}
}
