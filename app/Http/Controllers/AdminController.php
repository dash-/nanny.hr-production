<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 27.1.2016
 * Time: 12:00
 */

namespace App\Http\Controllers;

use App\Helpers\ParserHelper;
use App\Models\Availability;
use App\Models\Content;
use App\Models\CoverImage;
use App\Models\Holiday;
use App\Models\News;
use App\Models\Package;
use App\Models\Price;
use App\Models\Reservation;
use App\Models\ReservationAvailability;
use App\Models\ReservationHistory;
use App\Models\Token;
use App\Models\TransportReservations;
use App\Models\UsedPackage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{


	public function __construct()
	{
		$this->middleware('admin');
		if (!Auth::check())
			return redirect('/login');
		if (Auth::check() && Auth::user()->role != 1) {
			abort(403);
		}
	}


	public function getDodajPozadine()
	{
		return view('addCover', ['navigation_black' => true
		]);
	}

	public function postDodajPozadine(Request $request)
	{

		if ($request->file('pozadina')) {

			$cover_image = new CoverImage();
			$img = $request->file('pozadina');

			$filename = $request->pozadina->getClientOriginalName();
			$cover_image->image = $filename;

			$save_path = public_path() . '/images/covers/';
			Image::make($img)->save($save_path . $filename);

			$cover_image->in_use = 0;
			$cover_image->text_hr = $request->text_hr;
			$cover_image->text_en = $request->text_en;
			$cover_image->save();
		}

		return redirect()->back();
	}

	public function getPozadine()
	{
		$covers = CoverImage::paginate(20);
		return view('pozadine', ['covers' => $covers, 'navigation_black' => true
		]);
	}

	public function postPozadine(Request $request)
	{
		$covers = $request->covers_chosen;

		if ($covers) {

			$currentCovers = CoverImage::where('in_use', 1)->get();

			foreach ($currentCovers as $current) {
				CoverImage::find($current->id)->update(['in_use' => 0]);
			}

			foreach ($covers as $key => $cover) {
				CoverImage::find($key)->update([
					'in_use' => 1,
					'text_en' => isset($request->text_en[$key]) ? $request->text_en[$key] : null,
					'text_hr' => isset($request->text_hr[$key]) ? $request->text_hr[$key] : null,
				]);
			}
		}
		return redirect('/user');

	}

	public function getIzbrisiPozadinu($id)
	{
		CoverImage::find($id)->delete();
		return redirect()->back();
	}

	public function getPopustTokeni()
	{
		$tokens = Token::paginate(50);
		return view('createToken', ['tokens' => $tokens, 'navigation_black' => true
		]);
	}

	public function postPopustTokeni(Request $request)
	{

		$this->validate($request, [
			'token' => 'required',
			'discount' => 'required',
			'expiration' => 'required'
		]);

		$token = Token::where('token', $request->token)->first();

		if ($token != null)
			return redirect()->back()->with('message', 'Token je vec postojeći, molimo pokušajte ponovo');

		Token::create($request->all() + ['used' => 0]);

		return redirect()->back()->with('message', 'Token uspješno napravljen');
	}

	public function getIzbrisiToken($id)
	{
		Token::find($id)->delete();
		return redirect()->back();
	}

	public function getKorisnici(Request $request)
	{
		$request->session()->flush();
		$users = User::join('families', 'users.id', '=', 'families.user_id')->where('role', '!=', 1)->where('deleted_flag', false)->paginate(20);
		return view('users', ['users' => $users, 'navigation_black' => true
		]);
	}

	public function getRezervacijeKorisnika($id)
	{
		$reservations = Reservation::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(10);
		return view('userReservations', ['reservations' => $reservations, 'navigation_black' => true
		]);
	}

	public function getNadolazeciPraznici()
	{

		$holidays = Holiday::paginate(20);

		return view('holidays', ['holidays' => $holidays, 'navigation_black' => true
		]);
	}

	public function getDodajPraznik()
	{
		return view('addHoliday', ['navigation_black' => true]);
	}

	public function postDodajPraznik(Request $request)
	{

		$this->validate($request, [
			'holiday' => 'required',
			'date' => 'required'
		]);

		Holiday::create($request->except('date') + ['date' => date('Y-m-d H:i:s', strtotime($request->date))]);
		return redirect('/admin/nadolazeci-praznici');
	}

	public function getIzbrisiPraznik($id)
	{
		Holiday::find($id)->delete();
		return redirect()->back();
	}

	public function getOpciUvjeti()
	{
		$content = Content::whereSlug('opci-uvjeti')->first();
		if (!$content)
			abort(404);

		return view('editContent', ['content' => $content, 'navigation_black' => true]);
	}

	public function postOpciUvjeti(Request $request)
	{
		$this->validate($request, [
			'title' => 'required',
			'text' => 'required',
		]);
		$content = Content::whereSlug('opci-uvjeti')->first();
		$content->title = $request->title;
		$content->text = $request->text;
		$content->save();
		return redirect()->back()->with('message', 'Izmjene spasene');
	}

	public function getCjenik()
	{
		$prices = Price::get();
		return view('editCjenik', ['prices' => $prices, 'navigation_black' => true]);


	}

	public function postCjenik(Request $request)
	{
		$price = $request->input('price');
		$agency_price = $request->input('agency_price');

		foreach ($price as $key => $value) {
			$p = Price::find($key);
			$p->price = $value;
			$p->agency_price = $agency_price[$key];
			$p->save();
		};

		return redirect()->back()->with('message', 'Izmjene spasene');


	}
	public function getCjenikTekst()
	{
		$content = Content::whereSlug('cjenik')->where('language', 'hr')->first();
		if (!$content)
			abort(404);


		return view('editContent', ['content' => $content, 'navigation_black' => true]);
	}

    public function getCjenikTekstEng()
    {
        $content = Content::whereSlug('cjenik')->where('language', 'en')->first();
        if (!$content)
            abort(404);


        return view('editContentEng', ['content' => $content, 'navigation_black' => true]);
    }

	public function postCjenikTekst(Request $request)
	{
		$this->validate($request, [
			'title' => 'required',
			'text' => 'required',
		]);
		$content = Content::whereSlug('cjenik')->where('language', 'hr')->first();
		$content->title = $request->title;
		$content->text = $request->text;
		$content->save();
		return redirect()->back()->with('message', 'Izmjene spasene');
	}

    public function postCjenikTekstEng(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required',
        ]);
        $content = Content::whereSlug('cjenik')->where('language', 'en')->first();
        $content->title = $request->title;
        $content->text = $request->text;
        $content->save();
        return redirect()->back()->with('message', 'Izmjene spasene');
    }

    public function getRezervacije()
    {
        $reservations = Reservation::select('reservations.id', 'reservations.user_id', 'availability.nanny_id', 'reservation_availabilities.nanny', 'reservations.payment_type', 'reservations.total_price', 'reservations.status')
            ->join('reservation_availabilities', 'reservations.id','=', 'reservation_availabilities.reservation_id')
            ->join('availability', 'reservation_availabilities.availability_id', '=', 'availability.id')
            ->join('users', 'reservations.user_id', '=', 'users.id')
            ->where('users.deleted_flag', false)
            ->whereNull('availability.deleted_at')
            ->orderBy('reservations.created_at', 'desc')
            ->paginate(20);

        return view('reservations', ['reservations' => $reservations, 'navigation_black' => true ]);
    }

	public function getDirectPaymentSuccess($id)
	{
		Reservation::find($id)->wherePaymentType("Direktno")->update(['status' => 1]);
		return redirect()->back();
	}

    public function getProfilKorisnika(Request $request, $id) {
        $request->session()->put('impersonate_user_id', $id);
        return view('userProfile', ['id' => $id, 'navigation_black' => true]);
    }

    public function getChangeBabyMonitorAvailability(){
        $user = User::find(1);
	    if (!$user)
		    abort(404);
        if($user->baby_monitor_availability == true)
            $user->baby_monitor_availability = false;
        else
            $user->baby_monitor_availability = true;

        $user->save();
        return redirect()->back();
    }

    public function getCancelReservation($id) {

        $reservation = Reservation::find($id);
	    if (!$reservation)
		    abort(404);
        $reservation->status = 2;

        $reservation_availability = ReservationAvailability::where('reservation_id', $id)->first();
        $availability = Availability::find($reservation_availability->availability_id);
        $availability->used_hours = 0;
        $availability->isAvailable = true;

        $availability->save();
        $reservation->save();

        return redirect()->back();
    }

    public function getEditReservation($id) {
        $reservation = Reservation::find($id);
	    if (!$reservation)
		    abort(404);
        $history = ReservationHistory::where('reservation_id', $id)->get();

        $first_one = ReservationHistory::where('reservation_id', $id)->where('edited_by', '')->first();

        return view('editReservation', ['first_one' => $first_one, 'history'=> $history, 'reservation' => $reservation, 'id' => $id, 'navigation_black' => true, 'model' =>  new Request()]);
    }

    public function postEditReservation(Request $request) {

        $from = date('Y-m-d H:i:s',strtotime($request->from));
        $to = date('Y-m-d H:i:s',strtotime($request->to));

        if($from > $to)
            return redirect()->back()->with('errors', 'Vrijeme nije moguce izabrati');

        $total_info = [];
        $key = date('Y-m-d H:i:s',strtotime($request->from)).' - '. date('Y-m-d H:i:s', strtotime($request->to));

        $reservation = Reservation::find($request->id);

        $total_info[$key]['availability'][$reservation->reservation_availability->availability->id] = $reservation->reservation_availability->availability ->toArray();

        $total_info[$key]['info']['children']['child0'] = $reservation->reservation_availability->children;
        $total_info[$key]['info']['other_children'] = '';
        $total_info[$key]['info']['address_other'] = $reservation->reservation_availability->address;
        $total_info[$key]['info']['special_note'] = '';

        $total_info[$key]['price'] = $request->total_price;
        $total_info[$key]['chosen_nannies'][0] = $reservation->reservation_availability->availability->nanny;

        $request->session()->put('total', $reservation->total_price);
        $request->session()->put('total_info', $total_info);

        $request->session()->put('reservation_id', $request->id);
        $request->session()->put('extended_price', $request->total_price);
        $request->session()->put('extended_hours_from', $reservation->reservation_availability->end);
        $request->session()->put('extended_hours_to', date('H:i', strtotime($request->to)));
        $request->session()->put('edited_by', 'Administrator');

        return redirect('/payment');
    }

    public function getReservationHistoryPayed($id) {
        $reservation_history = ReservationHistory::find($id);
        $reservation_history->status = 1;
        $reservation_history->save();

        return redirect()->back();
    }

    public function getParse() {
        $file = public_path().'/csv/translations.csv';
        $parsed = ParserHelper::csvToArray($file);
        var_dump($parsed ? 'Done!' : "Failed!");die;
    }

    public function getObrisiKorisnika($id) {
        User::where('id', $id)->update(['deleted_flag' => true]);
        return redirect()->back();
    }

    public function getUrediPakete() {
        $packages = Package::all();
        $used_packages = UsedPackage::all();
        return view('packages', ['packages' => $packages, 'used_packages' => $used_packages,'navigation_black' => true]);
    }

    public function getDodajPaket() {
        return view('addPackage', ['navigation_black' => true]);
    }

    public function postDodajPaket(Request $request) {

        $this->validate($request, [
            'package_name' => 'required',
            'usage' => 'required',
            'price' => 'required',
        ]);

        Package::create($request->all());

        return redirect('/admin/uredi-pakete');
    }

    public function getObrisiPaket($id) {
        Package::where('id', $id)->delete();
        return redirect('/admin/uredi-pakete');
    }

    public function getUrediPaket($id) {

        $package = Package::find($id);
        return view('editPackage', ['package' => $package, 'id' => $id, 'navigation_black' => true]);
    }

    public function postUrediPaket(Request $request) {

        $update = Package::find($request->id);
        $update->package_name = $request->package_name;
        $update->usage = $request->usage;
        $update->price = $request->price;
        $update->save();

        return redirect('/admin/uredi-pakete');
    }

    public function getDodijeliPaket()
    {
        $users = User::all();
        $packages = Package::all();
        $users_array = [];
        $packages_array = [];

        foreach ($users as $user) {
            $users_array[$user->id] = $user->full_name() . ' - ' . $user->email;
        }

        foreach($packages as $package) {
            $packages_array[$package->id] = $package->package_name;
        }

        return view('assignPackage', ['users' => $users_array, 'packages' => $packages_array, 'navigation_black' => true]);
    }

    public function postDodijeliPaket(Request $request) {

        $existing_active_package = UsedPackage::where('user_id', $request->user_id)->get();

        if($existing_active_package) {
            foreach($existing_active_package as $exists) {
                if($exists->active == true)
                    return redirect()->back()->with('errors', 'Korisnik vec ima jedan aktivan paket');
            }
        }

        $package = Package::find($request->package_id);

        $used_package = new UsedPackage();
        $used_package->user_id = $request->user_id;
        $used_package->package_id = $request->package_id;
        $used_package->no_of_uses = $package->usage;
        $used_package->active = false;
        $used_package->save();

        return redirect('/admin/uredi-pakete');
    }

    public function getObrisiDodijeljeniPaket($id) {
        UsedPackage::where('id', $id)->delete();
        return redirect()->back();
    }

    public function getAktivirajPaket($id) {
        UsedPackage::where('id', $id)->update(['active' => true]);
        return redirect()->back();
    }

    public function getPrevozi() {
        $transports = TransportReservations::orderBy('created_at', 'desc')->paginate(20);
        return view('prevoziKorisnika', ['navigation_black' => true,  'transports' => $transports]);
    }

    public function getTransportDirectPaymentSuccess($id) {
        TransportReservations::find($id)->where('payment_type', 'Direktno')->update(['status' => 1]);
        return redirect()->back();
    }

    public function getTransportCancelReservation($id) {
        $reservation = TransportReservations::find($id);

        if (!$reservation)
            abort(404);
        $reservation->status = 2;
        $reservation->save();

        return redirect()->back();
    }

	public function getNovostiLista()
	{
		$novosti = News::orderBy('created_at', 'desc')->paginate(20);

		return view('adminNovostiLista', ['navigation_black' => true, 'novosti' => $novosti]);

	}

	public function getNovostObrisi($id = null)
	{
		$novost = News::find($id);
		$novost->delete();

		return redirect()->back()->with("message", "Novost izbrisana");
	}

	public function getNovost($id = null)
	{
		if ($id)
			$novost = News::find($id);
		if (!$id || !$novost)
			$novost = new News();

		return view('adminNovost', ['navigation_black' => true, 'novost' => $novost]);

	}

	public function postNovost(Request $request, $id = null)
	{

		$this->validate($request, [
			'title_hr' => 'required|unique:news,title_hr,' . $id,
//			'title_en' => 'required|unique:news,title_en,' . $id,
			'summary_hr' => 'required',
//			'summary_en' => 'required',
			'content_hr' => 'required',
//			'content_en' => 'required',
			'image' => $id ? '' : 'required',
		], [
			'image.required' => 'Slika je obavezna',
			'title_hr.required' => 'Hrvatski naziv je obavezan',
			'title_en.required' => 'Engleski naziv je obavezan',
			'title_hr.unique' => 'Hrvatski naziv već postoji, molimo obaberite drugi',
			'title_en.unique' => 'Engleski naziv već postoji, molimo obaberite drugi',
			'summary_hr.required' => 'Hrvatski kratki opis je obavezan',
			'summary_en.required' => 'Engleski kratki opis je obavezan',
			'content_hr.required' => 'Hrvatski tekst je obavezan',
			'content_en.required' => 'Engleski tekst je obavezan',
		]);

		if ($id)
			$novost = News::find($id);
		if (!$id || !$novost)
			$novost = new News();

		$novost->title_hr = $request->title_hr;
		$novost->title_en = $request->title_en;
		$novost->slug_hr = str_slug($request->title_hr);
		$novost->slug_en = str_slug($request->title_en);
		$novost->summary_hr = $request->summary_hr;
		$novost->summary_en = $request->summary_en;
		$novost->content_hr = $request->content_hr;
		$novost->content_en = $request->content_en;
		$novost->save();

		if ($request->file('image')) {

			$img = $request->file('image');

			$filename = rand(1, 31) . '-' . $request->image->getClientOriginalName();
			$novost->image = $filename;

			$save_path = public_path() . '/images/novosti/';
			Image::make($img)->resize(800, null, function ($constraint) {
				$constraint->aspectRatio();
			})->save($save_path . $filename);

			$novost->save();
		}
		return redirect('admin/novost/' . $novost->id)->with("message", "Novost spašena");

	}

}