<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 16.11.2015
 * Time: 12:03
 */

namespace App\Http\Controllers;

use App\Models\Availability;
use App\Models\Nanny;
use App\Models\NannyImage;
use Illuminate\Http\Request;
use App\Helpers\GeneralHelper;
use Intervention\Image\Facades\Image;
use Symfony\Component\Debug\Debug;

class NanniesController extends Controller
{

	public function __construct()
	{
		$this->middleware('admin');
	}

	public function getIndex()
	{

		$nannies = Nanny::join('nannies_images', 'nannies.id', '=', 'nannies_images.nanny_id')->get();

		return view('nannies', array(
			'nannies' => $nannies,
			'navigation_black' => true

		));
	}

	public function getAddNanny()
	{
		return view('addNanny', ['navigation_black' => true
		]);
	}

	public function postAddNanny(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'surname' => 'required',
			'dob' => 'required',
		]);

		$nanny = Nanny::create($request->except('dob') + ['dob' => date('Y-m-d H:i:s', strtotime($request->dob))]);
		NannyImage::create(['nanny_id' => $nanny->id]);
		return redirect('/nannies');
	}

	public function getProfile($id)
	{
		$nanny = Nanny::find($id);
		$img = NannyImage::where('nanny_id', $id)->first();
		return view('nannyProfile', array(
			'nanny' => $nanny,
			'img' => $img,
			'navigation_black' => true

		));
	}

	public function getEdit($id)
	{
		$nanny = Nanny::find($id);
		return view('editNanny', array(
			'nanny' => $nanny,
			'navigation_black' => true

		));
	}

	public function postEdit(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required',
			'surname' => 'required',
			'dob' => 'required',
		]);

		Nanny::find($id)->update($request->except('dob') + ['dob' => date('Y-m-d H:i:s', strtotime($request->dob))]);
		return redirect('/nannies');
	}

	public function getDelete($id)
	{
		Nanny::find($id)->delete();
		return redirect('/nannies');
	}

	public function getDostupnostDadilje($id)
	{
		$availability = Availability::where('nanny_id', $id)->orderBy('created_at', 'desc')->paginate(20);
		return view('nannyAvailability', [
			'availability' => $availability,
			'id' => $id,
			'navigation_black' => true

		]);
	}

	public function getDodajDostupnostDadilji($id)
	{
		return view('addNannyAvailability', [
			'availability' => new Availability(),
			'id' => $id,
			'navigation_black' => true
		]);
	}

	public function postDodajDostupnostDadilji(Request $request, $id)
	{
		$this->validate($request, [
			'from' => 'required',
			'to' => 'required'
		]);
		$availability = new Availability();
		$availability->nanny_id = $id;
		$availability->day_of_week = GeneralHelper::DaniUSedmici(date('l', strtotime($request['from'])));
		$availability->from = $request->from;
		$availability->to = $request->to;
		$availability->isAvailable = 1;
		$availability->number_of_hours = (strtotime($request['to']) - strtotime($request['from'])) / 60 / 60;
		$availability->save();

		return redirect('/nannies/dostupnost-dadilje/' . $id);
	}

	public function getObrisiDostupnost($id)
	{
		Availability::find($id)->delete();
		return redirect()->back();
	}

	public function getIzmijeniDostupnost($id)
	{

		$availability = Availability::find($id);

		if ($availability->isAvailable == true)
			$availability->isAvailable = false;
		else
			$availability->isAvailable = true;


		$availability->save();

		return redirect()->back();
	}

	public function getIzmijeniInformacijeDostupnosti($id)
	{
		$availability = Availability::find($id);
		return view('addNannyAvailability', [
			'id' => $availability->nanny_id,
			'navigation_black' => true,
			'availability' => $availability
		]);
	}

	public function postIzmijeniInformacijeDostupnosti(Request $request, $id)
	{
		$this->validate($request, [
			'from' => 'required',
			'to' => 'required'
		]);
		$availability = Availability::find($id);
		if (!$availability)
			abort(404);
		$availability->from = $request->from;
		$availability->to = $request->to;
		$availability->save();

		return redirect('/nannies/dostupnost-dadilje/' . $availability->nanny_id);
	}


	public function getPromijeniSliku($id)
	{
		$nanny = Nanny::find($id);
//        $nanny = Nanny::where('nannies.id', $id)->join('nannies_images', 'nannies.id', '=', 'nannies_images.nanny_id')->get();
		return view('editNannyImage', ['nanny' => $nanny, 'navigation_black' => true
		]);
	}

	public function postPromijeniSliku(Request $request, $id)
	{
		if ($request->file('pozadina')) {

			$nannyImageID = NannyImage::where('nanny_id', $id)->first();

			$nannyImage = NannyImage::find($nannyImageID->id);

			$image = $request->file('pozadina');

			$filename = $request->pozadina->getClientOriginalName();
			$nannyImage->image = $filename;

			$save_path = public_path() . '/images/nannies/';
			Image::make($image)->resize(200, null, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			})->save($save_path . $filename);

			$nannyImage->save();
		}


		return redirect()->back();
	}
}