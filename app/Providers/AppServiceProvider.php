<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Password validation
        Validator::extend(
            'password',
            function ($attribute, $value, $parameters) {
                $r1 = '/[A-Z]/';  //Uppercase
                $r2 = '/[a-z]/';  //lowercase
                $r3 = '/[0-9]/';  //numbers
//                $r4 = '/[!@#$%^&*()\-_=+{};:,<.>]/';  // 'special char'

                if (preg_match_all($r1, $value, $o) < 1)
                    return false;

                if (preg_match_all($r2, $value, $o) < 1)
                    return false;

                if (preg_match_all($r3, $value, $o) < 1)
                    return false;

                //Length 6
                if (strlen($value) < 6)
                    return false;

                return true;
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app;
    }
}
