<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 23.10.2015
 * Time: 15:01
 */
namespace App\Helpers;


use Illuminate\Support\Facades\Mail;

class EmailHelper {

    public static function sendEmail($view, $data, $emailData) {

        Mail::send($view, $data, function($message) use ($emailData)
        {
            $message->from('info@nanny.hr', 'Nanny.hr')->subject(isset($emailData['subject']) ? $emailData['subject'] : "Nanny.hr");
            $message->to($emailData['to']);
        });
    }

    public static function queueEmail($view, $data, $emailData) {

        Mail::queue($view, $data, function($message) use ($emailData)
        {
            $message->from('info@nanny.hr', 'Nanny.hr')->subject(isset($emailData['subject']) ? $emailData['subject'] : "Nanny.hr");
            $message->to($emailData['to']);
        });
    }

//    public static function quickContactEmail($data, $emailData) {
//
//        $emailData = ['subject' => 'Brzi kontakt od '. $request['name'] , 'to' => 'adnan.musabasic@gmail.com'];
//
//        Mail::send('emails.quick_contact', $data, function($message) use ($emailData)
//        {
//            $message->from('noreply@nanny.dash.ba', 'Nanny.hr')->subject($emailData['subject']);
//            $message->to($emailData['to']);
//        });
//    }
}