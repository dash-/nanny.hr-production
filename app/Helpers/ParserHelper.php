<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 18.4.2016
 * Time: 16:33
 */
namespace App\Helpers;


use Illuminate\Support\Facades\URL;

class ParserHelper {

    public static function csvToArray($filename='', $delimiter=',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = FALSE;
        $parse_data = FALSE;
        $item = array();

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while ($row = fgetcsv($handle, 14096, $delimiter)) {

                if(!$header) {
                    $header = array_flip($row);
                } elseif ($header AND !$parse_data) {
                    $parse_data = $row;
                    unset($parse_data[0]);
//                    unset($parse_data[1]);
                } else {
                    foreach ($parse_data as $key => $parsed) {
                        $item[$parsed][$row[0]] = str_replace('[TODO]', '', $row[$key]);
                    }
                }
            }
            fclose($handle);
        }

        foreach ($item as $key => $data) {
            $file = base_path(). '/resources/lang/'.$key.'/lang.php';
            file_put_contents($file, '<?php return ' . var_export($data, true) . ';');
        }

        return true;

    }
}