<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 7.1.2016
 * Time: 13:03
 */
namespace App\Helpers;

use App\Models\CoverImage;
use App\Models\Token;

class GeneralHelper {

    public static function DaniUSedmici($day) {

        switch($day) {
            case 'Monday':
                return 'Ponedjeljak';
                break;
            case 'Tuesday':
                return 'Utorak';
                break;
            case 'Wednesday':
                return 'Srijeda';
                break;
            case 'Thursday':
                return 'Četvrtak';
                break;
            case 'Friday':
                return 'Petak';
                break;
            case 'Saturday':
                return 'Subota';
                break;
            case 'Sunday':
                return 'Nedjelja';
                break;
        }

    }

    public static function weekendDay($day) {
        if($day == 'Saturday' || $day == 'Sunday')
            return "1";
        else
            return "0";
    }

    public static function numberOfChildren($children, $other_children) {

        $num_of_children = 0;

        if($children == 0 && $other_children != 0) {
            $num_of_children = $other_children;
        }
        else if($children != 0 && $other_children == 0) {
            $num_of_children = $children;
        }
        else if ($children != 0 && $other_children != 0) {
            $num_of_children = $children + $other_children;
        }
        else if($children == 0 && $other_children == 0) {
            $num_of_children = -1;
        }

        return $num_of_children;
    }

    public static function displayDate($date) {
        $newDate = date('d-m-Y H:i', strtotime($date));
        return $newDate;
    }

    public static function displayDateTransport($date) {
        $newDate = date('d.m.Y H:i', strtotime($date));
        return $newDate;
    }

    public static function liveCover() {
        $images = CoverImage::where('in_use', 1)->get();
        return $images;
    }

    public static function dayMonthYearDate($date){
        $newDate = date('d.m.Y', strtotime($date));
        return $newDate;
    }

    public static function displayLongDate($string) {
        $part_one = date('d.M.Y H:i', strtotime(substr($string, 0, 19)));
        $part_two = date('d.M.Y H:i', strtotime(substr($string, 23, 41)));

        return $part_one.' - '.$part_two;
    }

    public static function reservationDuration ($duration) {

        if($duration <= 2.5)
            return  "1";
        else if ($duration > 2.5 && $duration <= 5.5)
            return  "2";
        else if($duration >= 6)
            return  "3";

    }

    public static function monitorAvailability($availability) {
        if($availability == true)
            return '#56BB83';
        else
            return '#EF7CAF';
    }

    public static function dateTimeConcatenate($date, $time) {
        $concatenated_format = date('d.m.Y H:i:s', strtotime($date.' '.$time));
        return $concatenated_format;
    }
}