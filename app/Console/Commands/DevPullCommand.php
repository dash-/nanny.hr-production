<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DevPullCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Running: git pull");
        echo shell_exec("git pull");
        echo "\n";
        $this->info("Running: composer update");
        echo shell_exec("composer update");
        echo "\n";
        $this->info("Running: migrate");
        echo Artisan::call("migrate");
        $this->info("Running: seeding");
        Artisan::call("db:seed");
    }
}
