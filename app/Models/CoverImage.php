<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 27.1.2016
 * Time: 11:54
 */

namespace App\Models;


class CoverImage extends Root {
    protected $table = 'cover_images';


    public function text()
    {
        $language = 'hr';

        if (session('locale'))
            $language = session('locale');

        if ($language == 'hr')
            return $this->text_hr;
        elseif ($language == 'en')
            return $this->text_en;

        return null;
    }
}