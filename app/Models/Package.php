<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 26.4.2016
 * Time: 11:31
 */
namespace App\Models;

class Package extends Root  {

    protected $table = 'packages';

    public function uses() {
        return $this->hasMany('\App\Models\UsedPackage', 'package_id');
    }
}