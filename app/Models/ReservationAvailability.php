<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 19.1.2016
 * Time: 14:23
 */

namespace App\Models;

class ReservationAvailability extends Root {

    protected $table = 'reservation_availabilities';

    public function reservation() {
        return $this->belongsTo('\App\Models\Reservation', 'reservation_id');
    }

    public function availability() {
        return $this->belongsTo('\App\Models\Availability', 'availability_id');
    }
}