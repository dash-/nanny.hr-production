<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 16.11.2015
 * Time: 12:34
 */

namespace App\Models;

class Availability extends Root {

    protected $table = 'availability';

    public function nanny() {
	    return $this->belongsTo('\App\Models\Nanny', 'nanny_id');
    }

    public function nanny_full_name() {

	    return $this->nanny->name.' '.$this->nanny->surname;
    }


}