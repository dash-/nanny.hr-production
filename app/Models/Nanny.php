<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 16.11.2015
 * Time: 12:07
 */

namespace App\Models;

class Nanny extends Root {

    protected $table = 'nannies';

    public function availability() {
        return $this->hasMany('Availability', 'nanny_id');
    }
}