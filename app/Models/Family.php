<?php

namespace App\Models;

use JeroenDesloovere\Geolocation\Geolocation;

class Family extends Root {

    protected $table = 'families';

    protected static function boot()
    {
        parent::boot();
        static::updating(function ($family) {

            $geolocation = new Geolocation();
            $location = $geolocation->getCoordinates(
                $street = $family->address,
                $city = $family->address_city,
                $zip = $family->address_zip,
                $country = $family->address_state);

            if($location) {
                $family->lat = $location['latitude'];
                $family->lng = $location['longitude'];
            }
        });
    }

    public function user() {
        return $this->belongsTo('user', 'user_id');
    }

    public function parent () {
        return $this->hasMany('parents', 'family_id');
    }

}