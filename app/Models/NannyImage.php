<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 20.1.2016
 * Time: 15:18
 */

namespace App\Models;

class NannyImage extends Root  {
    protected $table = 'nannies_images';

    public function nanny() {
        return $this->hasOne('Nanny', 'nanny_id');
    }
}