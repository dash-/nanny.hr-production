<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 11.1.2016
 * Time: 16:37
 */

namespace App\Models;

class Price extends Root {

    /*
     * $duration:
     *
     * 1 => short
     * 2 => medium
     * 3 => long
     * */

    protected $table = 'prices';

    protected $casts = [
        'id' => 'integer',
        'children' => 'integer',
        'day' => 'integer',
        'night' => 'integer',
        'long' => 'integer',
        'weekend' => 'integer',
        'duration' => 'integer',
        'agency_price' => 'float',
        'price' => 'float',
    ];
}