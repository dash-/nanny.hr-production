<?php

namespace App\Models;

class Role extends Root {
    protected $table = 'roles';

    public function user() {
        return $this->belongsTo('user', 'user_id');
    }
}