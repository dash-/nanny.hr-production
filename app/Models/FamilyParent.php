<?php

namespace App\Models;

class FamilyParent extends Root {
    protected $table = 'parents';

    public function children() {
        return $this->belongsTo('Family', 'family_id');
    }

    public function type() {
        $type = [
            1 => 'Roditelj',
            2 => 'Kontakt za hitne slučajeve',
        ];

        return $type[$this->parent_type];
    }
}