<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 8.4.2016
 * Time: 11:08
 */
namespace App\Models;

class ReservationHistory extends Root {

    protected $table = 'reservations_history';

    public function reservation(){
        return $this->belongsTo('\App\Models\Reservation', 'reservation_id');
    }

    public function status() {
        $status = [
            0 => 'Ne',
            1 => 'Da',
            2 => 'Otkazano'
        ];

        return $status[$this->status];
    }

}