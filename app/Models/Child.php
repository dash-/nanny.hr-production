<?php

namespace App\Models;

class Child extends Root
{
	protected $table = 'children';

	public function family()
	{
		return $this->belongsTo('families', 'family_id');
	}

	public function allergies()
	{

		$allergies = [
			0 => 'Ne',
			1 => 'Da'
		];
		return $allergies[$this->allergies];
	}

	public function date_of_birth()
	{
		return date("d.m.Y", strtotime($this->dob));
	}
}