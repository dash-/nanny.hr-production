<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 11.5.2016
 * Time: 17:25
 */

namespace App\Models;

class TransportReservations extends Root {

    protected $table = 'transport_reservations';


    public function user() {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }

    public function status() {
        $status = [
            0 => 'Ne',
            1 => 'Da',
            2 => 'Otkazano'
        ];

        return $status[$this->status];
    }
}