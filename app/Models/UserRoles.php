<?php

namespace App\Models;

class UserRoles extends Root {
    protected $table = 'user_roles';

    public function user() {
        return $this->belongsTo('user', 'user_id');
    }

    public function role() {
        return $this->belongsTo('role', 'role_id');
    }
}