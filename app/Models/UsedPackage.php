<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 26.4.2016
 * Time: 14:12
 */

namespace App\Models;

class UsedPackage extends Root {

     protected $table = 'used_packages';


    public function package() {
        return $this->belongsTo('\App\Models\Package', 'package_id');
    }

    public function package_d()
    {
        return $this->belongsTo('\App\Models\Package', 'package_id')->withTrashed();
    }

    public function user() {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}