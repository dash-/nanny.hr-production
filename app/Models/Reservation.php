<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 16.11.2015
 * Time: 12:32
 */
namespace App\Models;

class Reservation extends Root
{
	protected $table = 'reservations';

    /*
     * Status rezervacije (plaćeno?):
     * 0 => 'Ne'
     * 1 => 'Da'
     * 2 => 'Otkazano'
     */

	public function user()
	{
		return $this->belongsTo('\App\Models\User', 'user_id');
	}

	public function reservation_availability()
	{
		return $this->hasOne('\App\Models\ReservationAvailability', 'reservation_id');
	}

    public function reservation_history() {
        return $this->hasMany('\App\Models\ReservationHistory', 'reservation_id');
    }

    public function status() {
        $status = [
            0 => 'Ne',
            1 => 'Da',
            2 => 'Otkazano'
        ];

        return $status[$this->status];
    }

    public function sum_payed()
    {
        $history = $this->reservation_history;
        $paid = $this->status();

        if ($history->count() != 0) {

            if ($this->status == 2)
                $paid = 'Otkazano';
            else {
                $paid = 'Da';
                foreach ($history as $his) {
                    if ($his->status == 0)
                        $paid = 'Ne';
                }
            }
        }
        return $paid;
    }
}