<?php
/**
 * Created by PhpStorm.
 * User: ado
 * Date: 9.2.2016
 * Time: 14:10
 */
namespace App\Contracts;

use Illuminate\Support\Facades\URL;

class PayWayHelper {


    /* variable $type in construct depends from which controller it is sent (payment or mpayment) to handle success or failed methods properly
        R => Rezervacije čuvalica
        T => Narucivanje transporta
    */

    public $secret_key;
    public $method;
    public $pgw_shop_id;
    public $pgw_order_id;
    public $pgw_amount;
    public $pgw_authorization_type;
    public $pgw_language;
    public $pgw_return_method;
    public $pgw_success_url;
    public $pgw_failure_url;
    public $pgw_order_info;
    public $pgw_signature;


    public function __construct($total, $info, $type){
        $this->method = "authorize-form";
        $this->secret_key = env('PAYWAY_SECRET_KEY', 't1$1iZLYTJ+8198R5tEoV5{7Je3IU');
        $this->pgw_shop_id = env('PAYWAY_SHOP_ID', 10001310);
        $this->pgw_authorization_type = 0;
        $this->pgw_language = 'hr';
        $this->pgw_return_method = 'GET';
        $this->pgw_success_url = ($type == 'R') ? URL::to("payment/success") : URL::to("mpayment/success");
        $this->pgw_failure_url = ($type == 'R') ? URL::to("payment/failed") : URL::to("mpayment/failed");
        $this->pgw_order_info = $info;
        $this->pgw_amount = $total * 100;
    }

    public function generateHash(){

        $hash_string = $this->method.$this->secret_key;

        $hash_string .= $this->pgw_shop_id.$this->secret_key;
        $hash_string .= $this->pgw_order_id.$this->secret_key;
        $hash_string .= $this->pgw_amount.$this->secret_key;
        $hash_string .= $this->pgw_authorization_type.$this->secret_key;
        $hash_string .= $this->pgw_language.$this->secret_key;
        $hash_string .= $this->pgw_return_method.$this->secret_key;
        $hash_string .= $this->pgw_success_url.$this->secret_key;
        $hash_string .= $this->pgw_failure_url.$this->secret_key;
        $hash_string .= $this->pgw_order_info.$this->secret_key;


        return hash('sha512', $hash_string);
    }
}