$(document).ready(function () {
    var inputfrom = document.getElementById('order-transport-from');
    var inputto = document.getElementById('order-transport-to');

    var autocomplete = new google.maps.places.Autocomplete(inputfrom);
    var autocomplete2 = new google.maps.places.Autocomplete(inputto);

    $('#calculateDistance').on('click', function () {
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [$('#order-transport-from').val()],
                destinations: [$('#order-transport-to').val()],
                travelMode: google.maps.TravelMode.DRIVING
            }, callback);
    });

    function callback(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
            var distance = response.rows[0].elements[0].distance.value;
            var destinationAddresses = response.destinationAddresses[0];
            var originAddresses = response.originAddresses[0];
            var dateTime = $('.transport-datetime').val();

                $.ajax({
                    type: 'POST',
                    url: '/transport/transport',
                    data: {
                        distance: distance,
                        destination: destinationAddresses,
                        origin: originAddresses,
                        date: dateTime
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            location.href = response.url;
                        }
                        else if(response.status == 'failed' && response.lang == 'hr'){
                            $( "#error-message" ).append( "<p>Izabrali ste vrijeme u proslosti ili vrijeme koje nije validno</p>" );
                        }
                        else if(response.status == 'failed' && response.lang == 'en'){
                            $( "#error-message" ).append( "<p>You have chose an invalid time or date</p>" );
                        }
                        else {
                            $( "#error-message" ).append( "<p></p>" );
                        }
                    }
                });
        }
    }


});