$(document).ready(function() {
    var inputfrom = document.getElementById('transport-from');
    var inputto = document.getElementById('transport-to');

    var autocomplete = new google.maps.places.Autocomplete(inputfrom);
    var autocomplete2 = new google.maps.places.Autocomplete(inputto);

    $('#calculateDistance').on('click', function () {

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [$('#transport-from').val()],
                destinations: [$('#transport-to').val()],
                travelMode: google.maps.TravelMode.DRIVING
            }, callback);

    });

    function callback(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
            var distance = response.rows[0].elements[0].distance.value;
            var destinationAddresses = response.destinationAddresses[0];
            var originAddresses = response.originAddresses[0];
            $.ajax({
                type: 'POST',
                url: '/availability/prevoz',
                data: {distance:distance, destination: destinationAddresses, origin:originAddresses},
                success: function (response) {
                    if (response.status == 'ok') {
                        location.href = response.url;
                    }
                }
            });
        }
    }


});