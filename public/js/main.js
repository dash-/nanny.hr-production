$(function () {

    //var i = 2;
    //
    //$('.add-child').on('click', function() {
    //    $('#children > tbody').append('<tr><td> <span>Dijete'+ i +'</span><p><label for="child_name">Ime </label><input type="text" name="children[child' + i +'][name]"/></p><p><label for="child_surname">Prezime </label><input type="text" name="children[child' + i +'][surname]"/></p><p><label for="child_dob">Datum rodjenja </label><input type="text" name="children[child' + i +'][dob]"/></p><p><label for="special_note">Specijalna napomena? </label><input type="text" name="children[child' + i +'][special_note]"/></p><p><label for="child_allergies">Alergije </label><input type="checkbox" name="children[child' + i +'][allergies]"/></p><p><b>Sex </b> <label for="special_note_m">Musko </label><input type="radio" name="children[child' + i +'][sex]" value="Male"/><label for="special_note_f">Zensko</label><input type="radio" name="children[child' + i +'][sex]" value="Female"/></p></td></tr>');
    //    i++;
    //});
    if ($(".radio-fields").length > 0) {
        var radio = $(".radio-fields input[type='radio']:checked");
        if (radio) {
            var elem = radio.closest(".izbor");
            radioFieldClicked(elem)
        }
    }
    function radioFieldClicked(elem) {
        $(".radio-fields .izbor").removeProp("checked");
        $(".radio-fields .izbor").removeClass('selected');
        elem.addClass('selected');
        elem.find("input[type='radio']").prop("checked", true);
    }

    if ($(".radio-fields-multiple").length > 0) {
        var radio = $(".radio-fields-multiple input[type='radio']:checked");
        if (radio) {
            var elem = radio.closest(".izbor");
            radioFieldMultipleClicked(elem)
        }
    }
    function radioFieldMultipleClicked(elem) {
        elem.closest('.radio-fields-holder').find(".radio-fields-multiple .izbor").removeProp("checked");
        elem.closest('.radio-fields-holder').find(".radio-fields-multiple .izbor").removeClass('selected');
        elem.addClass('selected');
        elem.find("input[type='radio']").prop("checked", true);
    }

    $(".radio-fields .izbor").click(function () {
        radioFieldClicked($(this));
    })
    $(".radio-fields-multiple .izbor").click(function () {
        radioFieldMultipleClicked($(this));
    })
    $('#carousel-example-generic').on('slid.bs.carousel', function () {
        $holder = $("ol li.active");
        $holder.removeClass('active');
        var idx = $('div.active').index('div.item');
        $('ol.carousel-indicators li[data-slide-to="' + idx + '"]').addClass('active');
    });

    $('ol.carousel-indicators  li').on("click", function () {
        $('ol.carousel-indicators li.active').removeClass("active");
        $(this).addClass("active");
    });

    $("#dob").datetimepicker({
        timepicker: false,
        //format: 'd-m-Y',
        format: 'd.m.Y',
        maxDate: new Date()
    });
    $("#datepicker").datetimepicker({
        timepicker: false,
        //format: 'd-m-Y',
        format: 'd.m.Y'
    });

    $("#datepicker2").datetimepicker({
        timepicker: false,
        //format: 'd-m-Y',
        format: 'd.m.Y'

    });

    $("#datetimepicker").datetimepicker({
        allowTimes: [
            '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
            '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
        ]
    });
    $("#datetimepicker2").datetimepicker({
        allowTimes: [
            '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
            '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
        ]
    });


    $('#from').datetimepicker({
        format: 'd.m.Y H:i',
        allowTimes: [
            '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
            '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
        ]
    });

    $('#to').datetimepicker({
        format: 'd.m.Y H:i',
        allowTimes: [
            '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30',
            '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
        ]
    });

    $('.login-register').click(function () {
        $('.slider1').animate({width: 'toggle'}, 350);
    });

    if (document.location.hash == "#login") {
        $('.slider1').show();
    }

    $('.onama-tabs').each(function () {

        var $active, $content, $links = $(this).find('a');

        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');

        $content = $($active[0].hash);

        $links.not($active).each(function () {
            $(this.hash).hide();
        });

        $(this).on('click', 'a', function (e) {
            $active.removeClass('active selected-tab');

            $content.hide();

            $active = $(this);
            $content = $(this.hash);

            $active.addClass('active selected-tab');
            $content.show();

            e.preventDefault();
        });
    });
    //
    //$('.home-boxes').on('click', function () {
    //    $('.home-title').fadeOut('slow');
    //    $('.home-boxes').fadeOut('slow');
    //    $('.boxes-info').fadeIn(800);
    //});
    //
    //$('.boxes-info').on('click', function () {
    //    $('.boxes-info').fadeOut('slow');
    //    $('.home-title').fadeIn(800);
    //    $('.home-boxes').fadeIn(800);
    //});

    //$('.home-boxes .box-blue').on('click', function() {
    //    $('.home-boxes .box-blue').fadeOut('slow');
    //    $('.home-boxes-info .box-blue').fadeIn(800);
    //    $('.home-boxes-info-text .box-blue-text').fadeIn(800);
    //})

    $('.footer-icon').on('click', function () {
        if ($('#footer-icon').hasClass('footer-info-opened')) {
            $('.footer-icon-info').fadeOut(800);
            $('.footer-icon-info-text').fadeOut(800);
            $('.footer-icon').removeClass('footer-info-opened');
        }
        else {
            $('.footer-icon-info').fadeIn(800);
            $('.footer-icon-info-text').fadeIn(800);
            $('.footer-icon').addClass('footer-info-opened');
        }
    });

    $('.info-icon').on('click', function () {
        $('.contact-form').fadeIn(200);
    });

    $('.glyphicon.glyphicon-remove-circle').on('click', function () {
        $('.contact-form').fadeOut(200);
    });

    $(document).ready(function () {

        $('#camera').attr('checked', false);
        //$('#terms').attr('checked', false);

        $('.flash-message').delay(5000).slideUp(function () {
            $(this).remove();
        });


        if ($("#terms").is(":checked")) {
            var checked = $("#terms").is(":checked");
            $("#paypal-button").attr("disabled", !checked);


            var attr1 = $('#paypal-button').attr('disabled');
            if (typeof attr1 !== typeof undefined && attr1 !== false) {
                $('#paypal-button').css('background-color', 'grey');
            }
            else {
                $('#paypal-button').css('background-color', '#1FC3F3');
            }

            $("#payway-button").attr("disabled", !checked);

            var attr2 = $('#payway-button').attr('disabled');
            if (typeof attr2 !== typeof undefined && attr2 !== false) {
                $('#payway-button').css('background-color', 'grey');
            }
            else {
                $('#payway-button').css('background-color', '#1FC3F3');
            }

            $("#direct-payment").attr("disabled", !checked);

            var attr3 = $('#direct-payment').attr('disabled');
            if (typeof attr3 !== typeof undefined && attr3 !== false) {
                $('#direct-payment').css('background-color', 'grey');
            }
            else {
                $('#direct-payment').css('background-color', '#1FC3F3');
            }
        }


        //var attr1 = $('#paypal-button').attr('disabled');
        //
        //if(typeof attr1 !== typeof undefined && attr1 !== false) {
        //    $('#paypal-button').css('background-color', 'grey');
        //    //$('#payway-button').css('background-color', 'grey');
        //}
        //
        //var attr2 = $('#payway-button').attr('disabled');
        //
        //if(typeof attr2 !== typeof undefined && attr2 !== false) {
        //    $('#payway-button').css('background-color', 'grey');
        //
        //}

    });


    //pets field show or hide depending on radio button
    $('.input-pets').hide();

    $('.yes-pets').on('click', function () {
        $('.input-pets').show();
    })

    $('.no-pets').on('click', function () {
        $('.input-pets').hide();
    })
    //////////////

    //allergies field show or hide depending on checkbox
    $('.allergies-info').hide();

    $('#allergiez').on('click', function () {
        if ($('#allergiez').is(':checked')) {
            $('.allergies-info').show();
        }
        else {
            $('.allergies-info').hide();
        }

    })
    //////////////

    $('.generate-token').on('click', function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        //var random = Math.floor((Math.random() * 900000) + 100000);
        $('.token-input').val(text);
    });


    $('.pay-way').on('click', function () {
        var token = $('#disc-token').val();
        var customPrice = $('#custom-price').val();
        if (typeof customPrice == typeof undefined) {
            customPrice = 0;
        }

        var camera = 0;
        if (document.getElementById('camera')) {
            if (document.getElementById('camera').checked) {
                camera = 1;
            }
        }
        $.ajax({
            type: 'GET',
            url: '/payment/pay-way?token=' + token + '&camera=' + camera + '&customprice=' + customPrice,
            //data: {data: token},
            success: function (response) {
                if (response.status == 'ok') {
                    location.href = response.url;
                }
                //window.location = response.url;
            }
        });
    })

    $('.pay-way-transport').on('click', function () {
        var token = $('#disc-token').val();
        $.ajax({
            type: 'GET',
            url: '/mpayment/pay-way?token=' + token,
            //data: {data: token},
            success: function (response) {
                if (response.status == 'ok') {
                    location.href = response.url;
                }
                //window.location = response.url;
            }
        });
    })
    $('[data-toggle="tooltip"]').tooltip()


    $('.direct-payment').on('click', function () {
        var token = $('#disc-token').val();
        var customPrice = $('#custom-price').val();
        if (typeof customPrice == typeof undefined) {
            customPrice = 0;
        }
        var camera = 0;
        if (document.getElementById('camera')) {
            if (document.getElementById('camera').checked) {
                camera = 1;
            }
        }

        $.ajax({
            type: 'GET',
            url: '/payment/initiate-direct-payment?token=' + token + '&camera=' + camera + '&customprice=' + customPrice,
            //data: {data: token},
            success: function (response) {
                if (response.status == 'ok') {
                    location.href = response.url;
                }
                //window.location = response.url;
            }
        });
    })

    $('.direct-payment-transport').on('click', function () {
        var token = $('#disc-token').val();
        $.ajax({
            type: 'GET',
            url: '/mpayment/initiate-direct-payment?token=' + token,
            //data: {data: token},
            success: function (response) {
                if (response.status == 'ok') {
                    location.href = response.url;
                }
                //window.location = response.url;
            }
        });
    })

    $('#camera').on('change', function () {
        var total = $('#total-price').text();
        var parsedPrice = parseFloat(total);
        if (document.getElementById('camera').checked) {
            document.getElementById('total-price').innerHTML = parsedPrice + 15;
        }
        else {
            document.getElementById('total-price').innerHTML = parsedPrice - 15;
        }
    })


    if ($('#top-link-block').length > 0) {
        $("#top-link-block a").click(function (e) {
            e.preventDefault();
            var top = $(window).height() + $(window).scrollTop()
            if (($(window).scrollTop() + $(window).height()) == $(document).height())
                top = 0;
            $('html,body').animate({scrollTop: top}, 'slow');
            return false;
        })
    }
    $(".btn-zatvoriti").click(function () {
        $(".alert").slideUp();
    })
    $('.mobile-navigation-holder .icon-bars').click(function () {
        $(".mobile-navigation").slideToggle();
    })
});






